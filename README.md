# IGCL Example Code
I completed my PhD in Aerospace Engineering at the University of Minnesota at the end of 2017, primarily working on the Interactive Guidance and Control Lab.
This repository contains a selection of code that I wrote for various projects. 
While some of this code is based on existing code written by others in the lab, the files shared here are those for which I believe I contributed all or nearly all of the code.
These folders may not contain all code files needed to run an example.

## First Person Sim
This code implements ROS-based simulation of a unicycle vehicle navigating around wall-type obstacles. 
The simulation uses an imported 3D world model, rendered using OpenSceneGraph. 
The user controls the vehicle with a USB RealFlight controller. 
The system tracks and records user gaze location on the screen, and projected into the 3D environment using a Tobii eye tracking devices.
More details of the system are located in this paper: https://ieeexplore.ieee.org/abstract/document/7379309

## Platform Tracking
This project investigated uncertain decision-making in the application of an autonomous rotorcraft landing on a moving platform. 
When humans perform a task like this, they learn an intuitive sense of the tradeoff between safety and performance, and learn when to begin a final approach to landing that will ensure adequate safety.
The objective of this project was to use an EKF to predict future landing position uncertainty based on disturbance magnitude, and automatically begin the final approach to the platform when sufficient accuracy is predicted.
This system was implemented in the lab, using a moving model train with an attached landing platform, and an autonomously controlled mCX RC helicopter.

## Subgoal Guidance
This project was a prototype for a subgoal-based dynamic planning system, where subgoals represent dynamic waypoint states. 
Subgoals are based on models of how humans understand and interact with their environment during a dynamic, constrained motion guidance task (https://doi.org/10.1109/TSMC.2013.2262043).
This code implements a prototype for guiding the mCX rotorcraft through a series of subgoal states to a goal position, in response to a pre-entered environment constraint configuration.
Additional details of the subgoal approach are here: (https://doi.org/10.1109/CDC.2015.7402389).