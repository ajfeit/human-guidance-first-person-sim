// ROS
#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Point32.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/Bool.h"
#include "igcl_data_collection/SimStatus.h"

#include <tf/tf.h>

// open scene graph library
#include <osg/PositionAttitudeTransform>
#include <osg/Group>
#include <osg/Geode>
#include <osg/ShapeDrawable>
#include <osg/Node>
#include <osgViewer/Viewer>
#include <osg/Vec3d>
#include <osg/Uniform>
#include <osgDB/ReadFile>
#include <osgGA/TrackballManipulator>
#include <osg/Geometry>
#include <osgText/Text>
#include <osg/MatrixTransform>
#include <osgSim/LineOfSight>
#include <string>
#include "ScreenCapture.h"
// #include "config/Config.h"
# include <cmath>

#define  RAD2DEG 180/3.1415926536
#define  DEG2RAD 3.1415926536/180
#define  XFOV_DEFAULT 30.0
#define  GX_RES_DEFAULT 1280.0
#define  GY_RES_DEFAULT 720.0

// Global variables for updating messages!!
static double x = 0.0;
static double y = 0.0;
static double z = 0.0;
static double yaw = 0.0;
static double pitch = 0.0;
static double roll = 0.0;
//static int gx, gy;
osg::Vec3f::value_type gx, gy;
bool recording, atgoal;
unsigned int xres, yres;
double gxscale, gyscale, gx_res, gy_res, gx_min, gy_min;
float gpsi, gthe;
double xfov, yfov;
double traj_time;
float tanxfov, tanyfov; 
float ar_view;
float mx, my;
double n_ic;
//osg::Vec3f eg; //gaze unit vector
osg::Matrixd vehicleAttMat;
osgSim::LineOfSight::Intersections gazeInt;
osg::Node::NodeMask gazeNodeMask;
int gazeViewType = 2;
int gazeSource = 2;
tf::Vector3 eg;
tf::Matrix3x3 vRot;

class myKeyboardEventHandler : public osgGA::GUIEventHandler
    {
    public:
       virtual bool handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter&);
       virtual void accept(osgGA::GUIEventHandlerVisitor& v)   { v.visit(*this); };
    };

bool myKeyboardEventHandler::handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& aa)
{
	if (ea.getEventType() == osgGA::GUIEventAdapter::MOVE) {
		mx = ea.getX();
		my = ea.getY();
		//ROS_INFO("Mouse: [%f], [%f]", mx, my);
	}
	return false;
}

void PoseCallback(const geometry_msgs::Pose::ConstPtr& pose_msg )
{
    tf::Quaternion q;
    double roll_rad, pitch_rad, yaw_rad;
    tf::quaternionMsgToTF(pose_msg->orientation, q);

    x = pose_msg->position.x;
    y = pose_msg->position.y;
    z = -(pose_msg->position.z);

    // helicopter transform in original reference frame
    tf::Transform heli_dynamic_model;
    heli_dynamic_model.setOrigin( tf::Vector3(x, y, z) );
    heli_dynamic_model.setRotation( q );

    // transform from original reference frame to sim reference frame
    //tf::Vector3 offset = tf::Vector3(100000.0, 55000.0, 700.0); //Moffett
    //tf::Vector3 offset = tf::Vector3(42.0, 73.0, 0.0);  //Akerman
    //tf::Vector3 offset = tf::Vector3(21900.0, 25780.0, 10.0);  //gym scene
    tf::Vector3 offset = tf::Vector3(0.0, 0.0, 1.5);

    tf::Transform tranform_dynamic_model_to_sim;
    //tranform_dynamic_model_to_sim.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
    tranform_dynamic_model_to_sim.setOrigin( offset );
    tranform_dynamic_model_to_sim.setRotation( tf::createQuaternionFromRPY(0, 0, 1.57079) ); //M_PI


    tf::Transform heli_sim = tranform_dynamic_model_to_sim * heli_dynamic_model;

    // coordinates for calibration sim
    //x = heli_sim.getOrigin().getX()+0.25;
    //y = heli_sim.getOrigin().getY()-2;
    //z = heli_sim.getOrigin().getZ()+0.25;

    // coordinates for akerman
    //x = heli_sim.getOrigin().getX() + 42.0;
    //y = heli_sim.getOrigin().getY() + 73.0;
    //z = heli_sim.getOrigin().getZ();

    // coordinates for Moffett
    x = heli_sim.getOrigin().getX();
    y = heli_sim.getOrigin().getY();
    z = heli_sim.getOrigin().getZ();

    vRot = tf::Matrix3x3(heli_sim.getRotation());
    vRot.getRPY(roll_rad, pitch_rad, yaw_rad);
    roll = roll_rad; //* RAD2DEG;
    pitch = pitch_rad; //* RAD2DEG;
    yaw = yaw_rad; // * RAD2DEG;
}

void statCallback(const igcl_data_collection::SimStatus::ConstPtr& stat_msg )
{
	recording = stat_msg->recording.data;
	atgoal = stat_msg->atgoal.data;
}

void gazeCallback(const geometry_msgs::Point32::ConstPtr& gaze_msg)
{
	gx = gaze_msg->x;
	gy = gy_res - gaze_msg->y;
}


int main(int argc,char** argv)
{
    // ROS
    ROS_INFO("Initializing ROS...");
    ros::init( argc, argv, "SimViewer");
    ros::NodeHandle n;

    ROS_INFO("Subscribed to ROS Pose messages from the JoyStick...");
    ros::Subscriber waypoint_sub = n.subscribe("/model/bladecx2/pose", 1, PoseCallback);
    //std::cout << "Done" << std::endl;

    ros::Subscriber stat_sub = n.subscribe("/rec_stat", 1, statCallback);

    //subscribe to gaze position
    //ros::Subscriber gaze_sub = n.subscribe("/etg/data", 1, gazeCallback);
    ros::Subscriber gaze_sub = n.subscribe("/etg/est", 1, gazeCallback);

    //publish position of gaze on scene
    ros::Publisher gaze_pub = n.advertise<geometry_msgs::Vector3>("/gaze/onscene", 1);

    geometry_msgs::Vector3 gaze_position_msg;

    //get screen resolution
    osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
    osg::GraphicsContext::ScreenIdentifier screenID = osg::GraphicsContext::ScreenIdentifier(0); 
    wsi->getScreenResolution(screenID, xres, yres); 
    ROS_INFO("Screen Resolution: x=[%d], y=[%d]", xres, yres);

    //set view parameters
	//fov is field of view in degrees
    n.param<double>("/sim/fov", xfov, XFOV_DEFAULT);
    ROS_INFO("FOV set to [%f]", xfov);
    ar_view = xres/yres;
    yfov = xfov/ar_view;

    //set gaze view type
    std::string gaze_type;
    n.param<std::string>("/gaze/view_type", gaze_type, "none");
    ROS_INFO("Gaze View Type: [%s]", gaze_type.c_str());
    if (gaze_type == "screen") {
	gazeViewType = 1;
    } else if (gaze_type == "scene") {
        gazeViewType = 2;
    } else { 
        gazeViewType = 0;
    }

    //set gaze source
    std::string gaze_source;
    n.param<std::string>("/gaze/source", gaze_source, "mouse");
    ROS_INFO("Gaze Source: [%s]", gaze_source.c_str());
    if (gaze_type == "mouse") {
	gazeSource = 1;
    } else if (gaze_type == "gaze") {
        gazeSource = 2;
    }

    // build scene graph
    osg::ref_ptr<osg::Group> scene = new osg::Group();

    //load terrain and connect to scene
    ROS_INFO("Loading Terrain");
    std::string scene_path;
    n.param<std::string>("/sim/scene_path", scene_path, "/home/andrew/ROS/models/course_1/course_1_with_obs.flt");
    osg::ref_ptr<osg::Node> terrain = osgDB::readNodeFile(scene_path.c_str());
    scene->addChild(terrain.get());
    terrain->setNodeMask(0x1);
 
    //connect transformation to vehicle coordinates to scene
    osg::PositionAttitudeTransform* VehicleTransform = new osg::PositionAttitudeTransform();
    scene->addChild(VehicleTransform);

   //Add Gaze marker
   ROS_INFO("Creating Gaze marker");
   osg::Geode* gazeGeode = new osg::Geode();
   osg::Projection* gazeProjection = new osg::Projection;
   osg::PositionAttitudeTransform* gazeViewMatrix = new osg::PositionAttitudeTransform();
   osg::PositionAttitudeTransform* gazeTransform = new osg::PositionAttitudeTransform();
   osg::Sphere* gazeMarker = new osg::Sphere(osg::Vec3f(0.0f,0.0f,0.0f),10.0f);
   if (gazeViewType == 1) {  //display gaze in 2D screen coordinates
     gazeProjection->setMatrix(osg::Matrix::ortho2D(0, xres, 0, yres));
     gazeViewMatrix->setPosition(osg::Vec3(200,200,0));
     gazeViewMatrix->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
     scene->addChild(gazeProjection);
     gazeProjection->addChild(gazeViewMatrix);
     gazeViewMatrix->addChild(gazeGeode);
   }
   else if (gazeViewType == 2) {  //display gaze projected onto the scene
     gazeMarker->setRadius(0.2);
     scene->addChild(gazeTransform);
     gazeTransform->addChild(gazeGeode);
   }
   
   gazeGeode->addDrawable(new osg::ShapeDrawable(gazeMarker));
   gazeGeode->setNodeMask(0x2);

   //Add HUD text
   // A geometry node for our HUD:
   ROS_INFO("Adding HUD");
   osg::Geode* HUDGeode = new osg::Geode();
   // Text instance that wil show up in the HUD:
   osgText::Text* textOne = new osgText::Text();
   // Projection node for defining view frustrum for HUD:
   osg::Projection* HUDProjectionMatrix = new osg::Projection;
   
   // Initialize the projection matrix for viewing everything we
   // will add as descendants of this node. Use screen coordinates
   // to define the horizontal and vertical extent of the projection
   // matrix. Positions described under this node will equate to
   // pixel coordinates.
   HUDProjectionMatrix->setMatrix(osg::Matrix::ortho2D(0,xres,0,yres));
     
   // For the HUD model view matrix use an identity matrix:
   osg::MatrixTransform* HUDModelViewMatrix = new osg::MatrixTransform;
   HUDModelViewMatrix->setMatrix(osg::Matrix::identity());

   // Make sure the model view matrix is not affected by any transforms
   // above it in the scene graph:
   HUDModelViewMatrix->setReferenceFrame(osg::Transform::ABSOLUTE_RF);

   // Add the HUD projection matrix as a child of the root node
   // and the HUD model view matrix as a child of the projection matrix
   // Anything under this node will be viewed using this projection matrix
   // and positioned with this model view matrix.
   scene->addChild(HUDProjectionMatrix);
   HUDProjectionMatrix->addChild(HUDModelViewMatrix);

   // Add the Geometry node to contain HUD geometry as a child of the
   // HUD model view matrix.
   HUDModelViewMatrix->addChild( HUDGeode );

   osg::Vec4Array* HUDcolors = new osg::Vec4Array;
   HUDcolors->push_back(osg::Vec4(0.8f,0.8f,0.8f,0.8f));

   osg::Vec2Array* texcoords = new osg::Vec2Array(4);
   (*texcoords)[0].set(0.0f,0.0f);
   (*texcoords)[1].set(1.0f,0.0f);
   (*texcoords)[2].set(1.0f,1.0f);
   (*texcoords)[3].set(0.0f,1.0f);

// Add the text (Text class is derived from drawable) to the geode:
       HUDGeode->addDrawable( textOne );

       // Set up the parameters for the text we'll add to the HUD:
       textOne->setCharacterSize(25);
       textOne->setFont("/usr/share/fonts/truetype/freefont/FreeMono.ttf");
       textOne->setText("Starting...");
       textOne->setAxisAlignment(osgText::Text::SCREEN);
       textOne->setPosition( osg::Vec3(0.75*xres,0.9*yres,-1.5) );
       textOne->setColor( osg::Vec4(199, 77, 15, 1) );

   //LIGHT CODE ------------------------
   ROS_INFO("Adjusting Lighting");
   osg::ref_ptr<osg::Group> lightGroup (new osg::Group);
   osg::ref_ptr<osg::StateSet> lightSS (scene->getOrCreateStateSet());
   osg::ref_ptr<osg::LightSource> lightSource1 = new osg::LightSource;
   osg::ref_ptr<osg::LightSource> lightSource2 = new osg::LightSource;

   // create a local light.

   float xCenter = 10.0; //scene->getRoot()->getXCenter();
   float yCenter = 15.0; //scene->getRoot()->getYCenter();

   osg::Vec4f lightPosition (osg::Vec4f(xCenter, yCenter,75.0,1.0f));
   osg::ref_ptr<osg::Light> myLight = new osg::Light;
   myLight->setLightNum(1);
   myLight->setPosition(lightPosition);
    myLight->setAmbient(osg::Vec4(0.8f,0.8f,0.8f,1.0f));
    myLight->setDiffuse(osg::Vec4(0.1f,0.4f,0.1f,1.0f));
    myLight->setConstantAttenuation(1.0f);
    myLight->setDirection(osg::Vec3(0.0f, 0.0f, -1.0f));
   lightSource1->setLight(myLight.get());

   lightSource1->setLocalStateSetModes(osg::StateAttribute::ON); 
   lightSource1->setStateSetModes(*lightSS,osg::StateAttribute::ON);
   //osg::StateSet* lightSS (lightGroup->getOrCreateStateSet());

   lightGroup->addChild(lightSource1.get());

   //Light markers: small spheres
   osg::ref_ptr<osg::Geode> lightMarkerGeode (new osg::Geode);
   lightMarkerGeode->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3f(xCenter,yCenter,75),10.0f)));

   scene->addChild(lightGroup.get());
   scene->addChild(lightMarkerGeode.get());

   //LIGHTCODE END----------------

    //  start the viewer
    ROS_INFO("Starting viewer");
    osgViewer::Viewer viewer;
    viewer.setSceneData(scene.get());
/*
    // attach a Trackball manipulator to allow user control of the view
    viewer.setCameraManipulator(new osgGA::TrackballManipulator);
    osg::Vec3 center(0, 0.0, 0.0);
    osg::Vec3 eye(0.0, -10.0, 10.0);
    osg::Vec3 up(0.0, 0.0, 1.0);
    viewer.getCameraManipulator()->setHomePosition(eye,center,up,false);
    viewer.getCameraManipulator()->home(0);
*/

    std::string view_size;
    n.param<std::string>("/sim/view_size", view_size, "win");
    if (view_size == "win") {
      //viewer.setUpViewInWindow( 320, 144, 640, 480 );
      viewer.setUpViewInWindow( 0, 0, 1300, 800 );
      //xres = 640;
      //yres = 480;
      osg::ref_ptr<osg::Camera> camera = new osg::Camera(); 
      //camera->setViewport(new osg::Viewport(0,0,640,480)); 
      camera->setViewport(new osg::Viewport(0,0,xres,yres));
      camera->setProjectionMatrixAsPerspective(yfov, 1.482, 0.5, 500);
      viewer.addSlave(camera.get(), osg::Matrixd(), osg::Matrixd::scale(1.0,1.0,1.0));
    } else if (view_size == "full") {
      osg::ref_ptr<osg::Camera> camera = new osg::Camera(); 
      camera->setViewport(new osg::Viewport(0,0,xres,yres)); 
      camera->setProjectionMatrixAsPerspective(yfov, 1.0/ar_view, 0.5, 500);
      viewer.addSlave(camera.get(), osg::Matrixd(), osg::Matrixd::scale(1.0,1.0,1.0));
    }

    n.param<double>("/sim/gx_res", gx_res, GX_RES_DEFAULT);
    n.param<double>("/sim/gy_res", gy_res, GY_RES_DEFAULT);
    n.param<double>("/sim/gx_min", gx_min, 0.0);
    n.param<double>("/sim/gy_min", gy_min, 0.0);
    gxscale = gx_res/xres;
    gyscale = gy_res/yres;

    // add the screen capture handler
    osg::ref_ptr< ScreenCapture > sc = new ScreenCapture(&n);
    viewer.getCamera()->setPostDrawCallback( sc.get() );

    //setup field of view parameters
    tanxfov = tan(DEG2RAD*xfov/2.0); ///((float;
    tanyfov = tan(DEG2RAD*yfov/2.0); ///((float) gy_res);

    //setup LineOfSight intersection detection
    osgSim::LineOfSight gazeLOS = osgSim::LineOfSight();

    //read mouse
    myKeyboardEventHandler* myFirstEventHandler = new myKeyboardEventHandler();
    viewer.addEventHandler(myFirstEventHandler);

    // create the window and start the required threads
    viewer.realize();

    ros::Rate loop_rate(30);
    ROS_INFO("Starting Loop");
    while(!viewer.done() && ros::ok()) // till window is closed
    {

	//update status text based on system state:
	std::string status;
	status = "Rec: ";
	//int recording = 1;
	if (recording) {
		status.append("ON   ");
	}
	else {
		status.append("OFF  ");
	}

	n.param<double>("/sim/n_ic", n_ic, 1.0);
	std::stringstream ss;
	ss << (n_ic+1);
	std::string s = ss.str();
	status.append(s.substr(0,2));

	if (atgoal) {
		//ROS_INFO("printing GOAL status");
		status.append(" GOAL: ");
		double traj_time;
    		n.param<double>("/sim/time", traj_time, 0.0);
		std::stringstream ss;
		ss << traj_time;
		std::string s = ss.str();
		status.append(s.substr(0,4));
		//ROS_INFO("done printing GOAL status");
	}

	textOne->setText(status);

        // update the vehicle's position and rotation
    	// I think the model for the blade cx2 needs to be rotated by -90deg causing the change
    	// in the roll and pitch axis in setAttitude
        //ROS_INFO("pose (%f, %f, %f)", x, y, z);
        osg::Vec3 VehiclePosit(x,y,z);
        VehicleTransform->setPosition(VehiclePosit);
        VehicleTransform->setAttitude(osg::Quat(pitch, osg::X_AXIS, -roll, osg::Y_AXIS, yaw-M_PI/2.0, osg::Z_AXIS));

	//compute gaze unit vector 
	// by Andrew for resolution 1280 x 720
	/*if (gazeSource == 2) {   //from gaze data
	  gpsi = atan(tanxfov*(gx - xres/3.0));
	  gthe = atan(tanyfov*(gy - yres/3.0));
	} else if (gazeSource == 1) {  //from mouse position
	  gpsi = atan(tanxfov*(mx - xres/2.0));
	  gthe = atan(tanyfov*(my - yres/2.0));
	}*/
	// changed by Abhi for resolution: 1920 x 1080
	/*
	if (gazeSource == 2) {
	  gpsi = atan(tanxfov*(gx/gxscale - xres/3.0)*0.285);
	  gthe = atan(tanyfov*(gy/gyscale - yres/3.0))*0.555;
	} else if (gazeSource == 1) {
	  gpsi = atan(tanxfov*(mx - xres/3.0)*0.285);
	  gthe = atan(tanyfov*(my - yres/3.0))*0.555;
	}
	*/
	if (gazeSource == 2) {
	  //gpsi = atan(tanxfov*(gx/gxscale - xres/2.0)/(xres/2.0));
	  gpsi = atan(tanxfov*((gx - gx_min - gx_res/2.0)/gxscale)/(xres/2.0));
	  //gthe = atan(tanyfov*(gy/gyscale - yres/2.0)/(yres/2.0));
	  gthe = atan(tanyfov*((gy - gy_min - gy_res/2.0)/gyscale)/(yres/2.0));
	} else if (gazeSource == 1) {
	  gpsi = atan(tanxfov*(mx - xres/3.0)*0.285);
	  gthe = atan(tanyfov*(my - yres/3.0))*0.555;
	}

	//compute LineOfSight
	//ROS_INFO("computing intersections...");
	gazeLOS.clear();
	eg = tf::Vector3(cos(gthe)*sin(gpsi), cos(gthe)*cos(gpsi), sin(gthe) );
	//ROS_INFO("gaze vector [%f], [%f], [%f]", eg.y(), -eg.x(), eg.z());
	eg = vRot*eg*50.0;
        //ROS_INFO("gaze vector [%f], [%f], [%f]", eg.x(), eg.y(), eg.z());
	gazeLOS.addLOS(VehiclePosit, VehiclePosit + osg::Vec3f(eg.y(), -eg.x(), eg.z()));
	gazeLOS.computeIntersections(scene, 0x1);
	gazeInt = gazeLOS.getIntersections(0);
	if (gazeInt.size() > 0) {
	  gaze_position_msg.x = gazeInt[0].x();
	  gaze_position_msg.y = gazeInt[0].y();
	  gaze_position_msg.z = gazeInt[0].z();
	  gaze_pub.publish(gaze_position_msg);
	}
	//ROS_INFO("gaze direction psi=[%f], the=[%f]", RAD2DEG*gpsi, RAD2DEG*gthe);
	//update gaze position marker
	//ROS_INFO("GazeViewType=[%d], GazeSource=[%d]", gazeViewType, gazeSource);
	if (gazeViewType == 1) {  // 2D gaze position
	  if (gazeSource == 2) {  //from gaze data
	    gazeViewMatrix->setPosition(osg::Vec3((gx-gx_min)/gxscale,(gy-gy_min)/gyscale,0));
	  } else if (gazeSource == 1) {  //from mouse position
 	    //ROS_INFO("Gaze: [%f], [%f]", mx, my );
	    gazeViewMatrix->setPosition(osg::Vec3(mx,my,0));
	  }
	} else if (gazeViewType == 2) {  //projected onto the scene
	  
	  //ROS_INFO("Found [%d] Intersections", gazeInt.size());
	  if (gazeInt.size() > 0) {
	    //ROS_INFO("Intersection at [%f], [%f], [%f]", gazeInt[0].x(), gazeInt[0].y(), gazeInt[0].z());
	    gazeTransform->setPosition(gazeInt[0]);
	    //gazeTransform->setPosition(osg::Vec3(2,10,0.0));
	  }
	}
        // Set camera position
        tf::Vector3 center_pos(1.0, 0.0, 0.0);
        //tf::Vector3 eye_pos(0.1, 0.0, 0.0);
        tf::Vector3 eye_pos(0.0, 0.0, 0.0);  //camera position relative to the vehicle
        tf::Vector3 up_pos(0.0, 0.0, 1.0);
 
        // rotate vectors based on heli pose
        tf::Matrix3x3 heli_rotation;
        heli_rotation.setRPY(-roll, -pitch, yaw);
        center_pos = heli_rotation * center_pos;
        eye_pos = heli_rotation * eye_pos;
        up_pos = heli_rotation * up_pos;

        osg::Vec3 center_point(center_pos.getX(), center_pos.getY(), center_pos.getZ());
        osg::Vec3 eye_point(eye_pos.getX(), eye_pos.getY(), eye_pos.getZ());
        osg::Vec3 up_point(up_pos.getX(), up_pos.getY(), up_pos.getZ());

        center_point = VehiclePosit + center_point;
        eye_point = VehiclePosit + eye_point;

	viewer.getCamera()->setViewMatrixAsLookAt(eye_point, center_point, up_point);
	viewer.getCamera()->setProjectionMatrixAsPerspective(yfov, 1.0/ar_view, 0.5, 500);

        // dispatch the new frame
        viewer.frame();

        // run ros callbacks
        ros::spinOnce();
        loop_rate.sleep();
    }

    exit(0);
}
