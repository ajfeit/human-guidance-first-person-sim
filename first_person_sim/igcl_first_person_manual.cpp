#include "ros/ros.h"
#include <std_srvs/Empty.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Empty.h>
#include <ardrone_autonomy/Navdata.h>

#define XBOX 1
#define REALFLIGHT 2
#define SONY 3
#define LOGITECH 4

#define MCX 1
#define CX 2
#define ARDRONE 3

#define CONTROL_SAMPLING_TIME 0.01

inline float max(float a, float b) { return a > b ? a : b; }
inline float min(float a, float b) { return a < b ? a : b; }

ros::Publisher cmd_vel_pub;
ros::Publisher takeoff_pub;
ros::Publisher land_pub;
ros::Publisher reset_pub;
ros::ServiceClient togglecam_client;

int x_but_val_prev = 0;
int y_but_val_prev = 0;
int a_but_val_prev = 0;
int b_but_val_prev = 0;

bool is_flying = false;
bool needs_reset = false;
geometry_msgs::Twist cmd_vel;
int control_type;
int rotorcraft_type;

int32_t cam_state=0; // 0 for forward and 1 for vertical, change to enum later

double joy_lat, joy_lon, joy_col, joy_ped;
double lat_trim = 0.0;
double lon_trim = 0.0;
double ped_trim = 0.0;
double col_trim = 0.0;

int batteryUpdateCount;

void navdataCallback(const ardrone_autonomy::Navdata::ConstPtr& msg)
{
	if (batteryUpdateCount > 200)
	{
		batteryUpdateCount = 0;
		ROS_INFO("ARDrone Battery: %f", msg->batteryPercent);
	}
	else
	{
		batteryUpdateCount++;

		if (msg->batteryPercent < 30.0)
		{
			ROS_WARN("ARDrone Battery: %f", msg->batteryPercent);
		}

	}
}

void joystickCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	//ROS_INFO("control_type: [%d]", control_type);
	if (control_type == XBOX)
	{
		std_msgs::Empty reset_msg;

		// XBOX
		const float maxHorizontalSpeed = 1; // use 0.1f for testing and 1 for the real thing
		joy_lon  = -max(min(-msg->axes[4], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_lat  = -max(min(-msg->axes[3], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_col  = max(min(msg->axes[1], 1), -1);
		joy_ped = -max(min(-msg->axes[0], 1), -1);


		if (rotorcraft_type == ARDRONE)
		{
			if ((is_flying == false) && (msg->buttons[0]==1) && (a_but_val_prev==0))
			{
				std_msgs::Empty takeoff_msg;
				takeoff_pub.publish(takeoff_msg);
				ROS_INFO("Takeoff");
				is_flying = true;

			}
			else if ((is_flying == true) && (msg->buttons[1]==1) && (b_but_val_prev==0))
			{
				std_msgs::Empty land_msg;
				land_pub.publish(land_msg);
				ROS_INFO("Land");
				is_flying = false;
			}


			if ((msg->buttons[2]==1) && (x_but_val_prev==0))
			{
				//toggle camera
				std_srvs::Empty togglecam_msg;
				if (togglecam_client.call(togglecam_msg))
				{
					ROS_INFO("Toggle camera called");
				}
				else
				{
					ROS_ERROR("Toggle camera failed");
				}
			}

			if ((msg->buttons[3]==1) && (y_but_val_prev==0))
			{
				//reset
				std_msgs::Empty reset_msg;
				reset_pub.publish(reset_msg);
				ROS_INFO("Reset");
			}

			a_but_val_prev = msg->buttons[0];
			b_but_val_prev = msg->buttons[1];
			x_but_val_prev = msg->buttons[2];
			y_but_val_prev = msg->buttons[3];
		}
	}
	else if (control_type == REALFLIGHT)
	{
		const float maxHorizontalSpeed = 1; // use 0.1f for testing and 1 for the real thing
		joy_lon  = max(min(msg->axes[1], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_lat  = max(min(msg->axes[0], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_col  = max(min(msg->axes[2]/0.685, 1), -1);
		joy_ped = max(min(msg->axes[4], 1), -1);

		if (rotorcraft_type == ARDRONE)
		{
			if ((is_flying == false) && (msg->buttons[0]==1) && (a_but_val_prev==0))
			{
				std_msgs::Empty takeoff_msg;
				takeoff_pub.publish(takeoff_msg);
				ROS_INFO("Takeoff");
				is_flying = true;

			}
			else if ((is_flying == true) && (msg->buttons[0]==0) && (a_but_val_prev==1))
			{
				std_msgs::Empty land_msg;
				land_pub.publish(land_msg);
				ROS_INFO("Land");
				is_flying = false;
			}


			if (((msg->buttons[1]==1) && (b_but_val_prev==0)) || ((msg->buttons[1]==0) && (b_but_val_prev==1)))
			{
				//toggle camera
				std_srvs::Empty togglecam_msg;
				if (togglecam_client.call(togglecam_msg))
				{
					ROS_INFO("Toggle camera called");
				}
				else
				{
					ROS_ERROR("Toggle camera failed");
				}
			}

			if ((msg->buttons[2]==1) && (x_but_val_prev==0))
			{
				//reset
				std_msgs::Empty reset_msg;
				reset_pub.publish(reset_msg);
				ROS_INFO("Reset");
			}

			a_but_val_prev = msg->buttons[0];
			b_but_val_prev = msg->buttons[1];
			x_but_val_prev = msg->buttons[2];
			y_but_val_prev = msg->buttons[3];
		}
		else if (rotorcraft_type == MCX)
		{

			if (msg->buttons[4]==1)
			{
				//Trim mode
				joy_ped = 0.25*joy_ped;

				if ((msg->buttons[2]==1) && (x_but_val_prev==0))
				{
					ped_trim += joy_ped;
					ROS_INFO("Set Ped Trim");
				}
			}

			if (msg->buttons[3]==1)
			{
				//Trim mode
				joy_lat = 0.25*joy_lat;
				joy_lon = 0.25*joy_lon;

				if ((msg->buttons[3]==1) && (msg->buttons[2]==1) && (x_but_val_prev==0))
				{
					lat_trim += joy_lat;
					lon_trim += joy_lon;
					ROS_INFO("Set Lat-Lon Trim");
				}
			}

			x_but_val_prev = msg->buttons[2];

			joy_lon  = max(min(joy_lon + lon_trim, 1), -1);
			joy_lat  = max(min(joy_lat + lat_trim, 1), -1);
			joy_ped = max(min(joy_ped + ped_trim, 1), -1);

			ROS_INFO("Joy ped %f, trimmed ped %f", joy_ped, joy_ped + ped_trim);
		}
	}
	else if (control_type == SONY)
	{
		const float maxHorizontalSpeed = 1; // use 0.1f for testing and 1 for the real thing
		joy_lon  = max(min(msg->axes[3], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_lat  = max(min(msg->axes[2], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_col = max(min(msg->axes[1]/0.685, 1), -1);
		joy_ped = max(min(msg->axes[0], 1), -1);
	}
	else if (control_type == LOGITECH)
	{
		const float maxHorizontalSpeed = 1; // use 0.1f for testing and 1 for the real thing
		joy_lon  = max(min(msg->axes[3], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_lat  = max(min(msg->axes[2], maxHorizontalSpeed), -maxHorizontalSpeed);
		joy_col  = max(min(msg->axes[1]/0.685, 1), -1);
		joy_ped = max(min(msg->axes[0], 1), -1);
		//ROS_INFO("joy_inp: [%f], [%f], [%f], [%f]", joy_lon, joy_lat, joy_col, joy_ped);
	}
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "igcl_manual_control");

	ros::NodeHandle n;

	batteryUpdateCount = 0;

	std::string igcl_control_type;
	n.param<std::string>("/igcl/joystick_type", igcl_control_type, "unknown");
	if (igcl_control_type == "realflight")
	{
		control_type = REALFLIGHT;
		ROS_INFO("joystick: realflight");
	}
	else if (igcl_control_type == "sony")
	{
		control_type = SONY;
		ROS_INFO("joystick: sony");
	}
	else if (igcl_control_type == "xbox")
	{
		control_type = XBOX;
		ROS_INFO("joystick: xbox");
	}
	else if (igcl_control_type == "logitech")
	{
		control_type = LOGITECH;
		ROS_INFO("joystick: logitech, [%d], [%d]", control_type, LOGITECH);
	}
	else
	{
		ROS_INFO("joystick: unknown");
	}

	ros::Duration(0.5).sleep();

	ros::Subscriber navdata_sub;

	std::string igcl_rotorcraft_type;
	n.param<std::string>("/igcl/rotorcraft_type", igcl_rotorcraft_type, "unknown");
	if (igcl_rotorcraft_type == "mcx")
	{
		rotorcraft_type = MCX;
		ROS_INFO("rotorcraft: mcx");
	}
	else if (igcl_rotorcraft_type == "cx")
	{
		rotorcraft_type = CX;
		ROS_INFO("rotorcraft: cx");
	}
	else if (igcl_rotorcraft_type == "ardrone")
	{
		rotorcraft_type = ARDRONE;
		ROS_INFO("rotorcraft: ardrone");
	}
	else
	{
		ROS_INFO("rotorcraft: unknown");
	}

	if ((rotorcraft_type == MCX) || (rotorcraft_type == CX))
	{
		cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/spektrum/cmd", 1);
	}
	else if (rotorcraft_type == ARDRONE)
	{
		cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
		takeoff_pub = n.advertise<std_msgs::Empty>("/ardrone/takeoff", 1);
		land_pub = n.advertise<std_msgs::Empty>("/ardrone/land", 1);
		reset_pub = n.advertise<std_msgs::Empty>("/ardrone/reset", 1);

		togglecam_client = n.serviceClient<std_srvs::Empty>("ardrone/togglecam");

		navdata_sub = n.subscribe("/ardrone/navdata", 1, navdataCallback);
	}

	ros::Subscriber joystick_sub = n.subscribe("/joy", 1, joystickCallback);

	ros::Rate loop_rate(1.0/CONTROL_SAMPLING_TIME);

	ROS_INFO("Start main loop");
	while (ros::ok())
	{
		ros::spinOnce();

		geometry_msgs::Twist cmd_msg;
		cmd_msg.linear.x  = joy_lon;
		cmd_msg.linear.y  = joy_lat;
		cmd_msg.linear.z  = joy_col;
		cmd_msg.angular.x = 0;
		cmd_msg.angular.y = 0;
		cmd_msg.angular.z = joy_ped;
		cmd_vel_pub.publish(cmd_msg);

		//sleep until next control update
		loop_rate.sleep();
	}

	return 0;
}

