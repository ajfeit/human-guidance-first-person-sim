#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Vector3.h"
#include "igcl_data_collection/SimState.h"
#include "igcl_data_collection/SimStatus.h"
#include "nav_msgs/Path.h"
#include "sensor_msgs/Joy.h"
#include "std_msgs/Bool.h"
#include "tf/tf.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <cstdio>
#include <vector>
#include <sstream>
#include <math.h>

using namespace std;

std::vector<igcl_data_collection::SimState> path_record;
std::vector<geometry_msgs::Vector3> gaze_record;
geometry_msgs::Vector3 gaze_position;
igcl_data_collection::SimState data_point;
geometry_msgs::Vector3 veloc;
double vx, vy, vz, ulat, ulon, ucol, uped, gx, gy, gz;
double sim_goal_xmin, sim_goal_xmax, sim_goal_ymin, sim_goal_ymax;
int recording = 0;
int publish_path = 0;
std::ofstream datafile;
int data_length;
int n_ic = 0;
int n_ic_at = 0;
std::string igcl_control_type;
bool atgoal = false;
bool started = false;
bool started_last = false;
ros::Time start_time, stop_time;
bool b1, b2, b1_last, b2_last, ic_change, b3, b3_last;
std::vector<float> x_init, y_init, z_init; 

bool isAtgoal(float x, float y, float vx, float vy)
{
	//ROS_INFO("x [%f], y [%f], vx [%f], vy [%f]", x, y, vx, vy); 
	//return ((x > 32) && (y > -2) && (y < 0));
	//return ((x > 35) && (y > 4) && (y<10)); // abhi
	//return ((x > sim_goal_xmin) && (y > sim_goal_ymin) && (y < sim_goal_ymax)); // abhi1
	return ((x > sim_goal_xmin) && (x < sim_goal_xmax) && (y > sim_goal_ymin) && (y < sim_goal_ymax)); // maze2016
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

void gazeCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	gaze_position.x = msg->x;
	gaze_position.y = msg->y;
	gaze_position.z = msg->z;
}

void poseCallback(const geometry_msgs::Pose::ConstPtr& msg)
{
	if (recording) {
		//ROS_INFO("logging data");
		data_point.pose.position = msg->position;
		data_point.pose.orientation = msg->orientation;
		data_point.velocity.linear = veloc;
		data_point.control.linear.x = ulon;
		data_point.control.linear.y = ulat;
		data_point.control.linear.z = ucol;
		data_point.control.angular.z = uped;
		data_point.header.stamp = ros::Time::now();
		data_point.header.frame_id = "/world";
		path_record.push_back(data_point);
		gaze_record.push_back(gaze_position);
		data_length++;

		//check if the goal has been reached
		if (isAtgoal(data_point.pose.position.x, data_point.pose.position.y, data_point.velocity.linear.x, data_point.velocity.linear.y)) {
			atgoal = true;
			if (started) {
				stop_time = ros::Time::now();
				started = false;
			}
		}
		if (atgoal) {
			ROS_INFO("At Goal");
		}

	}
}

void velCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	//velocities in global frame
	veloc.x = msg->x;
	veloc.y = msg->y;
	veloc.z = msg->z;
	if (started && !started_last) { started_last = true; }
	if (((fabs(veloc.x) > 0.1) || (fabs(veloc.y) > 0.1)) && !started_last) {
		started = true;
		start_time = ros::Time::now();
	}
}

void joystickCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	if (igcl_control_type == "logitech") {
		int button = msg->buttons[0];
		//ROS_INFO("button 1: [%d]", button);
		if (button && !recording) {
			recording = 1;
			data_length = 0;
			ROS_INFO("Record ON");
			//path_record = nav_msgs::Path();
			path_record.clear();
			gaze_record.clear();

		} else if (button && recording) {
			recording = 0;
			//atgoal = false;
			ROS_INFO("Record OFF");
			publish_path = 1;
		}
	} else if (igcl_control_type == "realflight") {

		int button = msg->buttons[0];
		if (button && !recording) {
			recording = 1;
			data_length = 0;
			ROS_INFO("Record ON");
			//path_record = nav_msgs::Path();
			path_record.clear();
			gaze_record.clear();
		} else if (!button && recording) {
			recording = 0;
			atgoal = false;
			ROS_INFO("Record OFF");
			publish_path = 1;
		}
		b1 = (msg->buttons[3]==1);
		b2 = (msg->buttons[4]==1);
		//ROS_INFO("[%B], [%B]", b1, b2);
		if (b1 && !b1_last) {
			n_ic_at++;
			if (n_ic_at >= n_ic) { n_ic_at = 0; }
			ic_change = true;
			//ROS_INFO("b1");
		}
		if (b2 && !b2_last) {
			n_ic_at--;
			if (n_ic_at < 0) { n_ic_at = (n_ic - 1); }
			ic_change = true;
			//ROS_INFO("b2");
		}
		b1_last = b1;
		b2_last = b2;
		b3 = msg->buttons[1];
		if (b3 && !b3_last) {
			started = false;
			started_last = false;
			atgoal = false;
		}
		b3_last = b3;
	}
}

void cmdCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
	ulat = msg->linear.y;
	ulon = msg->linear.x;
	ucol = msg->linear.z;
	uped = msg->angular.z;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "igcl_record_trajectory");
	ros::NodeHandle n;

	//get joystick type
	n.param<std::string>("/igcl/joystick_type", igcl_control_type, "realflight");

	//get goal position

	// map abhi1
	/*n.param<double>("/sim/goal_xmin", sim_goal_xmin, 37);
	n.param<double>("/sim/goal_ymin", sim_goal_ymin, -2);
	n.param<double>("/sim/goal_ymax", sim_goal_ymax, 3.5);
	*/

	// map abhi2
	/*n.param<double>("/sim/goal_xmin", sim_goal_xmin, 76);
	n.param<double>("/sim/goal_ymin", sim_goal_ymin, -37);
	n.param<double>("/sim/goal_ymax", sim_goal_ymax, -32);
	*/

	// map new maze 2016
	n.param<double>("/sim/goal_xmin", sim_goal_xmin, 117);
        n.param<double>("/sim/goal_xmax", sim_goal_xmax, 120);
	n.param<double>("/sim/goal_ymin", sim_goal_ymin, -103);
	n.param<double>("/sim/goal_ymax", sim_goal_ymax, -99.5);

	//get initial position configuration file
	ROS_INFO("Loading initial conditions");
	std::string ic_config_file;
	n.param<std::string>("/sim/ic_config_file", ic_config_file, "/home/user/models/course_1/ic_config_abhi.txt");
	string line;
  	ifstream ic_cfg (ic_config_file.c_str());
	std::vector<string> data_vec;
  	if (ic_cfg.is_open()) {
    		while ( getline (ic_cfg,line) ) {
      			ROS_INFO("%s",line.c_str());
			data_vec = split(line, ' ');
			x_init.push_back(atof(data_vec[0].c_str()));
			y_init.push_back(atof(data_vec[1].c_str()));
			z_init.push_back(atof(data_vec[2].c_str()));
			ROS_INFO("IC point [%d], x=[%f], y=[%f], z=[%f]", n_ic, x_init[n_ic], y_init[n_ic], z_init[n_ic]);
			n_ic++;   //number of ic points
    		}
		ROS_INFO("[%d] IC points loaded", n_ic);
    		ic_cfg.close();
  	}
	else ROS_INFO("Unable to open file [%s]",ic_config_file.c_str());

	if (n_ic > 0) {
		n_ic_at = 0;
		n.setParam("/sim/x_init", x_init[n_ic_at]);
		n.setParam("/sim/y_init", y_init[n_ic_at]);
		n.setParam("/sim/n_ic", n_ic_at);
		ROS_INFO("IC now at x=[%f], y=[%f]",x_init[n_ic_at], y_init[n_ic_at]); 
	} 

	//To Do:
	// - start/stop recording with button push from controller
	// - write data to text file
	// - after recording stops, publish path for visualization

	//subscribe to bladecx pose from dynamic model
	ros::Subscriber pose_sub = n.subscribe("/model/bladecx2/pose", 1, poseCallback);

	//subscribe to velocity
	//<geometry_msgs::Vector3>("/model/bladecx2/velocity"
	ros::Subscriber vel_sub = n.subscribe("/model/bladecx2/velocity", 1, velCallback);
	
	//subscribe to joy for button pushes to start/stop recording
	ros::Subscriber joystick_sub = n.subscribe("/joy", 1, joystickCallback);

	//subscribe to control inputs
	ros::Subscriber ctrl_sub = n.subscribe("/spektrum/cmd", 1, cmdCallback);

	//subscribe to gaze position
	ros::Subscriber gaze_sub = n.subscribe("/gaze/onscene", 1, gazeCallback);

	//ros::Publisher path_pub = n.advertise<igcl_data_collection::SimState>("/trajectory", 1);
	ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("/pose", 1);
	ros::Publisher stat_pub = n.advertise<igcl_data_collection::SimStatus>("/rec_stat", 1);

	//open a file

	//write header with configuration data

	//enter loop
	ROS_INFO("Record Trajectory:  Ready");
	ros::Rate loop_rate(50);
	while (ros::ok())
	{
		ros::spinOnce();

		//publish status
		//std_msgs::Bool status_msg = std_msgs::Bool();
		igcl_data_collection::SimStatus status_msg;
		status_msg.recording.data = (recording == 1);
		status_msg.atgoal.data = atgoal;
		stat_pub.publish(status_msg);

		//check for goal reached
		if (atgoal) {
			ros::Duration tt = stop_time - start_time;
			//if (started) { ROS_INFO("started"); }
			//ROS_INFO("start time: %f", start_time.toSec());
			//ROS_INFO("stop time: %f", stop_time.toSec());
			//ROS_INFO("traj time: %f", tt.toSec());
			n.setParam("/sim/time", tt.toSec());
		}

		//check for IC change
		if (ic_change) {
			n.setParam("/sim/x_init", x_init[n_ic_at]);
			n.setParam("/sim/y_init", y_init[n_ic_at]);
			n.setParam("/sim/z_init", z_init[n_ic_at]);
			n.setParam("/sim/n_ic", n_ic_at);
			ROS_INFO("IC now at x=[%f], y=[%f], z=[%f]",x_init[n_ic_at], y_init[n_ic_at], z_init[n_ic_at]);
			ic_change = false; 	
		}

		//publish the trajectory if it is ready
		if (publish_path) {
			//path_record.header.stamp = ros::Time::now();
			//path_record.header.frame_id = "/world";
			//path_pub.publish(path_record);
			//open data file
			std::time_t now = std::time(NULL);
			std::tm * ptm = std::localtime(&now);
			char buffer[32];
			// Format: Mo, 15.06.2009 20:20:00
			std::strftime(buffer, 32, "%a_%d_%m_%Y_%H_%M_%S", ptm); 
			string filename = "traj_data_";
			filename.append(buffer);
			filename.append(".txt");
			datafile.open(filename.c_str());
			ROS_INFO("file opened");
		        if (datafile.fail())
			  perror(filename.c_str());
			datafile << "Trajectory Data Start Location: " << n_ic_at << "\n";
			datafile << "time, x, y, z, phi, the, psi, xv, yv, zv, ulat, ulon, ucol, uped, gx, gy, gz\n";
			float xpos, ypos, zpos, psi, phi, the, xvel, yvel, zvel, lat, lon, col, ped;
			ROS_INFO("Recording [%d] lines", data_length);
			for (int i=0;i<data_length;i++){ 
				xpos = path_record[i].pose.position.x;
				ypos = path_record[i].pose.position.y;
				zpos = path_record[i].pose.position.z;
				psi = tf::getYaw(path_record[i].pose.orientation);
				phi = 0.0; //tf::getRoll(path_record[i].pose.orientation);
				the = 0.0; //tf::getPitch(path_record[i].pose.orientation);
				xvel = path_record[i].velocity.linear.x;
				yvel = path_record[i].velocity.linear.y;
				zvel = path_record[i].velocity.linear.z;
				lat = path_record[i].control.linear.y;
				lon = path_record[i].control.linear.x;
				col = path_record[i].control.linear.z;
				ped = path_record[i].control.angular.z;
				gx = gaze_record[i].x;
				gy = gaze_record[i].y;
				gz = gaze_record[i].z;
				datafile << path_record[i].header.stamp << ", " << xpos << ", " << ypos << ", " << zpos << ", " << phi << ", " << the << ", " << psi << ", " << xvel << ", " << yvel << ", " << zvel << ", " << lat << ", " << lon << ", " << col << ", " << ped << ", " << gx << ", " << gy << ", " << gz << endl;
				
				 
			}
			//close file
			datafile.close();
			publish_path = 0;
		}

		//publish PoseStamped
		pose_pub.publish(data_point.pose);
		//sleep until next control update
		loop_rate.sleep();
	}

	return 0;
}
