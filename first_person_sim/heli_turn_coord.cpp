//Include headers for OpenCV 
#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
#include <iomanip>
#include <queue>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <math.h>
#include <tr1/random>
#include <sstream>

//Includes all the headers necessary to use the most common public pieces of the ROS system.
#include <ros/ros.h>
#include <geometry_msgs/TransformStamped.h>
#include <opencv2/core/core.hpp>
#include <geometry_msgs/Twist.h>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <sensor_msgs/Joy.h>

using namespace cv;
using namespace std;

#define RAD2DEG (180.0/3.1416)
#define DEG2RAD (3.1416/180.0)

#define GRAVITY_MS2 9.80665

#define VICON_POSITION_NOISE 0 //0.0001
#define VICON_ORIENTATION_NOISE 0 //0.0005
#define IMU_ANGULAR_RATE_NOISE 0 //0.01
#define IMU_ACCEL_NOISE 0 //0.1
#define ULAT_NOISE 0 //0.05
#define ULON_NOISE 0 //0.05
#define UCOL_NOISE 0 //0.02
#define UPED_NOISE 0 //0.01

#define ULAT_BIAS 0 //0.03
#define ULON_BIAS 0 //0.03
#define UCOL_BIAS 0 //0.00
#define UPED_BIAS 0 //0.00

class Model {

	//typedef std::tr1::ranlux64_base_01            ENG;    // subtract_with_carry_01
	typedef std::tr1::mt19937                     ENG;    // Mersenne Twister
	typedef std::tr1::normal_distribution<double> DIST;   // Normal Distribution
	typedef std::tr1::variate_generator<ENG,DIST> GEN;    // Variate generator

	double dt;

	double phi;
	double the;
	double	psi;
	double	p;
	double	q;
	double	r;
	double	r_dot;
	double	a;
	double	b;
	double	c;
	double	d;
	double	u;
	double	v;
	double	w;
	double  x;
	double  y;
	double  z;
	double  ax;
	double  ay;
	double  az;
	double xdot, ydot, zdot;


	cv::Mat R_N2B;
	cv::Mat R_B2N;


	double lat, lon, ped, col;
	double x_ini, y_ini, z_ini;
	int is_flying;
	double u_max, u_min;
	std::vector<double> x_init_list;
	std::vector<double> y_init_list;
	double u_acc_gain, u_trn_gain;
	//double u_lon_max = 0.6;

	// setup generator for noise
	//ENG  eng;
	//DIST dist(0,1);
	//GEN  gen(eng,dist);

  public:
	Model(int argc, char **argv)
	{

		R_N2B = cv::Mat::eye(3, 3, CV_64F);
		R_B2N = cv::Mat::eye(3, 3, CV_64F);
		x_ini = 0;
		y_ini = 0;
		z_ini = 0.5;
		u_max = 1;
		u_min = 0;
		dt = 0.01;

		//ros::init(argc, argv, "bladecx2");

		//ros::NodeHandle nh;


		/* States
			0	phi
			1	the
			2	psi
			3	p
			4	q
			5	r
			6	r_dot
			7	a
			8	b
			9	c
			10	d
			11	u
			12	v
			13	w
			14  ax
			15  ay
			16  az
			17  x
			18  y
			19  z
*/
		// publish vicon messages
		ros::Publisher vicon_pub = nh.advertise<geometry_msgs::TransformStamped>("/vicon/cx/cx", 1);
		// publish imu messages
		ros::Publisher imu_pub = nh.advertise<sensor_msgs::Imu>("/cx/imu", 1);
		// publish pose messages
		ros::Publisher pose_pub = nh.advertise<geometry_msgs::Pose>("/model/bladecx2/pose", 1);
		// publish velocity direction messages
		ros::Publisher velocity_direction_pub = nh.advertise<geometry_msgs::Vector3>("/model/bladecx2/velocity_direction", 1);
		// publish velocity  messages
		ros::Publisher velocity_pub = nh.advertise<geometry_msgs::Vector3>("/model/bladecx2/velocity", 1);
		// read control commands
		ros::Subscriber sub = nh.subscribe("/spektrum/cmd", 1, &Model::cmdCallback, this);
		//subscribe to joy for button pushes to start/stop recording
		ros::Subscriber joystick_sub = nh.subscribe("/joy", 1, &Model::joystickCallback, this);

		nh.param<double>("/sim/x_init", x_ini, 0.0);
		nh.param<double>("/sim/y_init", y_ini, 0.0);
		nh.param<double>("/sim/z_init", z_ini, 0.5);
		nh.param<double>("/sim/v_max", u_max, 1000);

		// setup model
		bladecx2_initialize(x_ini, y_ini, z_ini);

		// setup generator for noise
	    //ENG  eng;
	    //DIST dist(0,1);
	    //GEN  gen(eng,dist);

	    // setup main loop
		ROS_INFO("bladecx2 model running");
		ros::Rate loop_rate(100);

		while (ros::ok())
		{

			cv::Mat u_k = cv::Mat::zeros(4,1,CV_64F);
			u_k.at<double>(0,0) = lat;
			u_k.at<double>(1,0)	= lon;
			u_k.at<double>(2,0) = ped;
			u_k.at<double>(3,0) = col;

			bladecx2_propagate(u_k);

			/*
			 * The vicon message object.
			 */
			geometry_msgs::TransformStamped msg_vicon;
			msg_vicon.header.stamp = ros::Time::now();
			//double translation_noise = 0.05; // m
			//double rotation_noise = 0.005; // rad
			msg_vicon.transform.translation.x = x; // + gen()*VICON_POSITION_NOISE;
			msg_vicon.transform.translation.y = y; // + gen()*VICON_POSITION_NOISE;
			msg_vicon.transform.translation.z = -(z); // + gen()*VICON_POSITION_NOISE);
			double roll = phi; // + gen()*VICON_ORIENTATION_NOISE;
			double pitch = the; // + gen()*VICON_ORIENTATION_NOISE;
			double yaw = (psi); // + gen()*VICON_ORIENTATION_NOISE);
			//ROS_INFO("dyn mod roll = %f, pitch = %f, yaw = %f", roll*RAD2DEG, pitch*RAD2DEG, yaw*RAD2DEG);
			tf::Quaternion ardrone_quat = tf::createQuaternionFromRPY(roll, pitch, yaw);
			msg_vicon.transform.rotation.x = ardrone_quat.x();
			msg_vicon.transform.rotation.y = ardrone_quat.y();
			msg_vicon.transform.rotation.z = ardrone_quat.z();
			msg_vicon.transform.rotation.w = ardrone_quat.w();

			/*
			 * publish
			 */
			vicon_pub.publish(msg_vicon);

			/*
			 * The imu message object.
			 */
			sensor_msgs::Imu msg_imu;
			//double angular_rate_noise = 0.001; // rad/s
			//double accel_noise = 0.1; // m/s2
			msg_imu.angular_velocity.x = (p); // + gen()*IMU_ANGULAR_RATE_NOISE);
			msg_imu.angular_velocity.y = (q); // + gen()*IMU_ANGULAR_RATE_NOISE);
			msg_imu.angular_velocity.z = (r); // + gen()*IMU_ANGULAR_RATE_NOISE);
			msg_imu.linear_acceleration.x = ax; // + gen()*IMU_ACCEL_NOISE;
			msg_imu.linear_acceleration.y = ay; // + gen()*IMU_ACCEL_NOISE;
			msg_imu.linear_acceleration.z = az; // + gen()*IMU_ACCEL_NOISE;

			/*
			 * publish
			 */
			imu_pub.publish(msg_imu);


			/*
			 * The pose message object.
			 */
			geometry_msgs::Pose pose_msg;
			pose_msg.position.x = x;
			pose_msg.position.y = y;
			pose_msg.position.z = z;
			tf::Quaternion pose_quat = tf::createQuaternionFromRPY(phi, the,  psi);
			pose_msg.orientation.x = pose_quat.x();
			pose_msg.orientation.y = pose_quat.y();
			pose_msg.orientation.z = pose_quat.z();
			pose_msg.orientation.w = pose_quat.w();

			/*
			 * publish
			 */
			pose_pub.publish(pose_msg);

			/*
			 * The velocity direction message object.
			 */
			geometry_msgs::Vector3 vel_dir_msg;
			double vel_mag = sqrt(u*u + v*v + w*w);
			if (vel_mag > 0.0000000001)
			{
				vel_dir_msg.x = u/vel_mag;
				vel_dir_msg.y = v/vel_mag;
				vel_dir_msg.z = w/vel_mag;
			}
			else
			{
				vel_dir_msg.x = 0;
				vel_dir_msg.y = 0;
				vel_dir_msg.z = 0;
			}

			/*
			 * publish
			 */
			velocity_direction_pub.publish(vel_dir_msg);

			/*
			 * The velocity direction message object.
			 */
			geometry_msgs::Vector3 vel_msg;
			vel_msg.x = xdot;
			vel_msg.y = ydot;
			vel_msg.z = zdot;

			/*
			 * publish
			 */
			velocity_pub.publish(vel_msg);

			ros::spinOnce();

			loop_rate.sleep();
		}
		
	}

	// Simple function to print out a matrix
	void matPrintToConsole( std::ostream &outStream, std::string stringLabel, cv::Mat &mat )
	{		
	    outStream << stringLabel << std::endl;
	    for (int iRow = 0; iRow < mat.rows; iRow++)
	    {
		for (int iCol = 0; iCol < mat.cols; iCol++)
		{
		    outStream << std::setw(12) << mat.at<double>(iRow,iCol) << " ";
		}
		outStream << std::endl;
	    }
	}


/**************************************************************************************************
 *					ROS callbacks
 **************************************************************************************************/

	void bladecx2_initialize( double x_ini, double y_ini, double z_ini )
	{
		phi = 0;
		the = 0;
		psi = 0;
		p = 0;
		q = 0;
		r = 0;
		r_dot = 0;
		a = 0;
		b = 0;
		c = 0;
		d = 0;
		u = 0;
		v = 0;
		w = 0;
		ax = 0;
		ay = 0;
		az = 0;
		x = x_ini;
		y = y_ini;
		//z = -0.25;
		z=z_ini;

		lat = 0;
		lon = 0;
		ped = 0;
		col = 0;//0.53;
		
		nh.param<double>("/sim/u_acc_gain", u_acc_gain, 10.0);
		nh.param<double>("/sim/u_trn_gain", u_trn_gain, 1.0);


	}

	void cmdCallback(const geometry_msgs::Twist::ConstPtr& msg)
	{
	  //ROS_INFO("cx_cmd lat: %.3f, lon: %.3f, col: %.3f, ped: %.3f", msg->lat, msg->lon, msg->col, msg->ped);
	  lat=msg->linear.y;
	  lon=msg->linear.x;
	  col=msg->linear.z;
	  ped=msg->angular.z;
	}

	void joystickCallback(const sensor_msgs::Joy::ConstPtr& msg)
	{
		int button = msg->buttons[1];
		//ROS_INFO("button 1: [%d]", button);
		if (button) {
			nh.param<double>("/sim/x_init", x_ini, 0.0);
			nh.param<double>("/sim/y_init", y_ini, 0.0);
			nh.param<double>("/sim/z_init", z_ini, 0.5);
			ROS_INFO("ICing to [%f], [%f], [%f]", x_ini, y_ini, z_ini);
			bladecx2_initialize(x_ini, y_ini, z_ini);
		}
	}

	void bladecx2_propagate(cv::Mat& u_k)
	{
	/* Input
		0	u_lat
		1	u_lon
		2	u_ped
		3	u_col
	*/

		double u_lat = u_k.at<double>(0,0); // + gen()*ULAT_NOISE + ULAT_BIAS;
		double u_lon = u_k.at<double>(1,0); // + gen()*ULON_NOISE + ULON_BIAS;
		double u_ped = u_k.at<double>(2,0); // + gen()*UPED_NOISE;
		double u_col = u_k.at<double>(3,0); // + gen()*UCOL_NOISE;

		double u_prev = u;
		double v_prev = v;
		double w_prev = w;

		// phi rad
		phi = phi + dt*p;
		// the rad
		the = the + dt*(q*cos(phi) - r*sin(phi));
		// psi rad
		psi = psi + dt*r; //dt*(r*cos(phi)*cos(the) + q*sin(phi)*cos(the));
		//ROS_INFO("(phi, the, psi) = (%f, %f, %f)", RAD2DEG*phi, RAD2DEG*the, psi);

		// p rad/s.
		p = 0; //p + dt*(Lb*b + Ld*d);
		// q rad/s
		q = 0; //q + dt*(Ma*a + Mc*c);
		// r deg/s
		if (u < 2.0) {
			r = 0.5*(u_lat + u_lat*u_lat*u_lat);
		}
		else if (u >= 2.0) {
			float yacc = u_trn_gain*(u_lat + u_lat*u_lat*u_lat);
			r = yacc/u;
		}
		// r_dot deg/s^2
		//r_dot = r_dot + dt*(Rrdot*r_dot + Rr*r + Rped*u_ped);

		// u
		double u_lon_max = 0.6;
		u = std::max(std::min(u + u_acc_gain*dt*(std::max(u_lon/u_lon_max,0.0) - (1.0/u_max)*u), u_max), u_min);
		//u = 		
		
		// v
		v = 0; //v + dt*(Yv*v + Yphi*phi);
		// w
		w = 0;
		//w = w + -1.0*dt*(u_col + 1.0*w); //w + dt*(Zw*w + Zcol*u_col);
		//ROS_INFO("(u, v, w) = (%f, %f, %f)", u, v, w);

		// ax
		ax = (u - u_prev)/dt;
		// ay
		ay = (v - v_prev)/dt;
		// az
		az = (w - w_prev)/dt;

		// Find rotation into world frame
		double the_r = the;
		double phi_r = phi;
		double psi_r = psi;
		R_B2N.at<double>(0,0) = cos(the_r) * cos(psi_r);
		R_B2N.at<double>(0,1) = cos(the_r) * sin(psi_r);
		R_B2N.at<double>(0,2) = -sin(the_r);
		R_B2N.at<double>(1,0) = sin(phi_r) * sin(the_r) * cos(psi_r) - cos(phi_r) * sin(psi_r);
		R_B2N.at<double>(1,1) = cos(phi_r) * cos(psi_r) + sin(phi_r) * sin(the_r) * sin(psi_r);
		R_B2N.at<double>(1,2) = sin(phi_r) * cos(the_r);
		R_B2N.at<double>(2,0) = sin(phi_r) * sin(psi_r) + cos(phi_r) * sin(the_r) * cos(psi_r);
		R_B2N.at<double>(2,1) = cos(phi_r) * sin(the_r) * sin(psi_r) - sin(phi_r) * sin(psi_r) ;
		R_B2N.at<double>(2,2) = cos(phi_r) * cos(the_r);
		R_N2B = R_B2N.t();

		cv::Mat acc_N = cv::Mat::zeros(3,1,CV_64F);
		acc_N.at<double>(0,0) = 0;
		acc_N.at<double>(1,0) = 0;
		acc_N.at<double>(2,0) = -GRAVITY_MS2;
		cv::Mat acc_B = R_B2N * acc_N;
		ax = ax + acc_B.at<double>(0,0);
		ay = ay + acc_B.at<double>(1,0);
		az = az + acc_B.at<double>(2,0);

		cv::Mat vel_B = cv::Mat::zeros(3,1,CV_64F);
		vel_B.at<double>(0,0) = u;
		vel_B.at<double>(1,0) = v;
		vel_B.at<double>(2,0) = w;

		cv::Mat vel_N = R_N2B * vel_B;
		// x
		xdot = vel_N.at<double>(0,0);
		x = x + dt*vel_N.at<double>(0,0);
		// y
		ydot = vel_N.at<double>(1,0);
		y = y + dt*vel_N.at<double>(1,0);
		// z
		zdot = vel_N.at<double>(2,0);
		z = z + dt*vel_N.at<double>(2,0);
		ROS_INFO("(x, y, z) = (%f, %f, %f)", x, y , z);
	}

  protected:
	ros::NodeHandle nh;

};

/**************************************************************************************************
 *					Main
 **************************************************************************************************/


int main(int argc, char **argv)
{
	ros::init(argc, argv, "bladecx2");
	Model heli_mod(argc, argv);

	return 0;
}
