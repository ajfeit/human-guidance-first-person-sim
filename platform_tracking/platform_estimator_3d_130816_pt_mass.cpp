#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float32.h"
#include "nav_msgs/Path.h"
#include "turtlesim/Pose.h"
//#include "geometry_msgs/PoseWithCovariance.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Vector3.h"
#include "opencv/cv.h"
#include "stdio.h"
#include "math.h"
#include <visualization_msgs/Marker.h>
#include "tf/tf.h"

//measurements
float xposr;
float yposr;
float xcov, ycov, cov;
float dt;
float dtpred;

cv::Mat Fk,Hk, P0, Q, R, Skk, Skkm, Skmkm, Omega, K, Fkpred, Spos, Spred, P, Pdot, Pheli, Ptot;
cv::Mat zk, xhatkk, xhatkkm, xhatkmkm, xpred, xpred_last, temp;
cv::Mat zkest, innov, zpred;
cv::Mat Qhat, Rhat;
cv::Mat u, Bk, Kc;
int n;  //# of inputs
int m;  //# of states
int l;  //# of outputs

int cpos = 0;
int clen = 25;
int start_est = 0;
cv::Mat* C; 
cv::Mat Ck;
float sigma_heli_x, sigma_heli_y, sigma_heli_z, confidence, epsilon;
double proll, ppitch, pyaw, pyaw_last, pyaw_alpha;

int control_on = 0;
int newMeasurement = 0;
int heli_start = 0;

tf::Quaternion q, q_heli, q_platform;
tf::Matrix3x3 R_P_H, R_H_G, R_P_G;  //rotation from_to, i.e. R_H_P is rotation from heli to platform
tf::Vector3 r_P_H_H, r_P_H_G, r_H_G_G, r_P_G_G;  //position from_to_frame,  i.e. r_H_P_P is vector from heli to platform in platform frame

void sensorCallback(const geometry_msgs::Pose::ConstPtr& msg)
{
	if(heli_start == 1)
	{
		//relative orientation of platform with respect to heli
		tf::quaternionMsgToTF(msg->orientation, q);

		//convert quaternion to rotation matrix
		//orientation of platform with respect to heli
		R_P_H = tf::Matrix3x3 (q);
		R_H_G = tf::Matrix3x3 (q_heli);  //orientation of heli in global frame

		//orientation of platform in global frame = orientation of heli in global frame + platform relative to heli
		R_P_G = R_P_H.transpose()*R_H_G.transpose();
		R_P_G.getRotation(q_platform);

		//extract heading
		R_P_G.getRPY(proll, ppitch, pyaw);

		//relative position of platform in heli frame, (converted from mm to m if we are using the camera)
		r_P_H_H = tf::Vector3 (msg->position.x, msg->position.y, msg->position.z);

		r_P_H_G = R_H_G * r_P_H_H;  //relative position of platform with respect to heli in global frame

		r_P_G_G = r_H_G_G + r_P_H_G;  //global position of platform in global reference frame

		//put relative position measurement into zk vector
		zk = (cv::Mat_<float>(l, 1) << r_P_G_G.x(), r_P_G_G.y(), r_P_G_G.z() );
	
		//then flag that a new measurement has arrived
		newMeasurement = 1;
		//ROS_INFO("new measurement:  %f  %f  %f", zk.at<float>(0,0), zk.at<float>(1,0), zk.at<float>(2,0));
		//ROS_INFO("new measurement:  %f  %f  %f", r_P_H_G.x());
	}

}

void heliPoseCallback(const geometry_msgs::Pose::ConstPtr& msg)
{
	tf::quaternionMsgToTF(msg->orientation, q_heli);
	r_H_G_G = tf::Vector3 (msg->position.x, msg->position.y, msg->position.z);
	heli_start = 1;
 	//ROS_INFO("w: %f",q_heli.getW());
}

void helicontrolCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	u.at<float>(0,0) = msg->x;
	u.at<float>(1,0) = msg->y;
	u.at<float>(2,0) = msg->z;
	control_on = 1;

}

void helicontrolposcovCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	sigma_heli_x = msg->x;
	sigma_heli_y = msg->y;
	sigma_heli_z = msg->z;

}

void helicontrolvelcovCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	

}

cv::Mat stateTrans(cv::Mat xkm, float dtt)
{
	//nonlinear state transition	
	cv::Mat xk = cv::Mat::zeros(m, 1, CV_32F);
	//float v = xkm.at<float>(3,0);
	//float theta = xkm.at<float>(2,0);
	//xk.at<float>(0,0) = v*cos(theta);
	//xk.at<float>(1,0) = v*sin(theta);
	//xk.at<float>(2,0) = xkm.at<float>(5,0);
	//xk.at<float>(3,0) = xkm.at<float>(4,0);
	//xk.at<float>(4,0) = 0;
	//xk.at<float>(5,0) = xkm.at<float>(6,0);
	//xk.at<float>(6,0) = 0;

	xk = Fk*xkm;
	
	//xk = (Fk + Bk*Kc*control_on)*xkm;
	return xk;
}

cv::Mat computeJacobian(cv::Mat xhat)
{
	cv::Mat Fklin;
	//float v = xhat.at<float>(3,0);
	//float theta = xhat.at<float>(2,0);

	//ROS_INFO("about to store rows");
	//Fklin = (cv::Mat_<float>(m,m) << 0,0,-v*sin(theta),cos(theta),0,0,0,  0,0,v*cos(theta),sin(theta),0,0,0,  0,0,0,0,0,1,0,  0,0,0,0,1,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,1,  0,0,0,0,0,0,0 );
	//ROS_INFO("rows set");
	//Fklin = Fklin*dt + cv::Mat::eye(m, m, CV_32F);
	
	Fk.copyTo(Fklin);	
	return Fklin;
}

void initFilter()
{
	//first try point-mass model	
	n = 3;
	m = 9;
	l = 3;	
	dt = 0.02;
	dtpred = 0.04;	

	//constant velocity model, 4x4	
	//Fk = (cv::Mat_<float>(m,m) << 0, 1, 0, 0,  0, 0, 0, 0,  0, 0, 0, 1,  0, 0, 0, 0);
	//constant acceleration model, 6x6	
	
	Fk = (cv::Mat_<float>(m,m) << 0,0,0,0,0,0,0,0,0,  1,0,0,0,0,0,0,0,0,  0,1,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,  0,0,0,1,0,0,0,0,0,  0,0,0,0,1,0,0,0,0, 0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,1,0,0, 0,0,0,0,0,0,0,1,0 );
	
	cv::Mat Fdiag = (cv::Mat_<float>(m, 1) << 1, 1, 1 , 1, 1, 1, 1, 1, 1 );
	Fk = cv::Mat::diag(Fdiag) + dt*Fk;
	Fkpred = cv::Mat::eye(m, m, CV_32F) + dtpred*Fk;
	
	//constant acceleration mode
	Hk = (cv::Mat_<float>(l, m) << 0,0,1,0,0,0,0,0,0,  0,0,0,0,0,1,0,0,0, 0,0,0,0,0,0,0,0,1   );
	//nonlinear Dubins vehicle model
	//states:  x, y, theta, v, a, w, alpha
	//Hk = (cv::Mat_<float>(l, m) << 1,0,0,0,0,0,0,  0,1,0,0,0,0,0  );	

	//input matrix for helicopter estimation
	//this is the negative of the actual heli B matrix because
	//we are modelling the relative position of the platform in response to
	//heli control
	Bk = (cv::Mat_<float>(m, n) << -1,0,0, 0,0,0, 0,0,0, 0,-1,0, 0,0,0, 0,0,0, 0,0,-1, 0,0,0, 0,0,0  );
	u = (cv::Mat_<float>(n, 1) << 0,0,0 );
	cv::Mat xhat0 = (cv::Mat_<float>(m, 1) << 0,0,0,0,0,0,0,0,0 );

	//initialize P0, Fk, Q, R, xhat0, etc
	P0 = cv::Mat::eye(m, m, CV_32F)*1;
	P0.copyTo(P);
	Pdot = cv::Mat::zeros(m, m, CV_32F);
	cv::Mat qdiag = (cv::Mat_<float>(m, 1) << 0.0001, 0.0001, 0.0001 , 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001 );
	Q = cv::Mat::diag(qdiag);

	//Kc = (cv::Mat_<float>(2, 6, CV_32F) << 0,1.7,1,0,0,0, 0,0,0,0,1.7,1 );
	

	//cv::Mat rdiag = cv::Mat_<float>(l, 1) << 5, 5 );
	R = cv::Mat::eye(l, l, CV_32F)*0.1;
	P0.copyTo(Skmkm);
	P0.copyTo(Skk);
	xhat0.copyTo(xhatkmkm);
	xhat0.copyTo(xhatkk);

	sigma_heli_x = 0;
	sigma_heli_y = 0;
	sigma_heli_z = 0;
	epsilon = 0.15;  //+/- 10 cm tolerance

	//time constant for yaw low-pass filter (2.0 second time constant)
	pyaw_alpha = 0.02/(0.2 + 0.02);
	
	std::cout << "Q = \n";
	std::cout << Q;	
	std::cout << "\n R = \n";
	std::cout << R;
	std::cout << "\n Fk = \n";
	std::cout << Fk;
	std::cout << "\n Hk = \n";
	std::cout << Hk;
}

double lowpassYaw( double pyaw_new )
{
	pyaw_last = (pyaw_alpha*pyaw_new + (1 - pyaw_alpha)*pyaw_last);
	return pyaw_last;
}

void predictFilter()
{
	//predict new platform state
	//xhatkkm = Fk*xhatkmkm;
	xhatkkm = stateTrans(xhatkmkm, dt);

	//predict new state covariance
	Skkm = Fk*Skmkm*Fk.t() + Q;

}

void updateFilter()
{
	//update covariance
	Omega = Hk*Skkm*Hk.t() + Rhat;
	K = Skkm*Hk.t()*Omega.inv();
	Skk = (cv::Mat::eye(m,m, CV_32F) - K*Hk)*Skkm;

	//update state estimate
	innov = zk - Hk*xhatkkm;
	xhatkk = xhatkkm + K*innov;

}

void delayUpdatefilter()
{
	//if no measurement is available, just use previous values
	Skkm.copyTo(Skk);
	xhatkkm.copyTo(xhatkk);
	
}

void delayFilter()
{
	//advance one time step, previous values are now current values
	xhatkk.copyTo(xhatkmkm);
	Skk.copyTo(Skmkm);

}

nav_msgs::Path pathPrediction(int length)
{
	//generate a nav_msgs::Path message that contains
	//a trajectory of n poses into the future
	//along with the covariance of the path at each time
	nav_msgs::Path predpath;
	nav_msgs::Path predleftrange;
	nav_msgs::Path predrightrange;  
  
  	predpath.header.frame_id = "/my_frame";
  	predpath.poses.resize(length);

	xhatkk.copyTo(xpred);
	xhatkk.copyTo(xpred_last);
	zpred = Hk * xhatkk;

	//can a new version of Skk be computed from an ARE based on Qhat and Rhat?
	P.copyTo(Spred);
	//Spos_pred = Hk*Skk*Hk.t();

	predpath.poses[0].pose.position.x = zpred.at<float>(0,0);
    predpath.poses[0].pose.position.y = zpred.at<float>(1,0);
	predpath.poses[0].pose.position.z = zpred.at<float>(2,0);

	for(int i = 1; i < length; i++)
  	{
	  //xpred = Fk * xpred;
	  xpred = stateTrans(xpred, dtpred);
	  
	  Fkpred = computeJacobian(xpred);
	  Spred = Fkpred*Spred*Fkpred.t() + Qhat;
	  
	  zpred = Hk * xpred;
    	    
	  predpath.poses[i].pose.position.x = zpred.at<float>(0,0);
      predpath.poses[i].pose.position.y = zpred.at<float>(1,0);
	  predpath.poses[i].pose.position.z = 0.11;	  
	  
  	} 

	//iterate to converge to time-varying state covariance matrix, P:
	P = Fk*(P - K*Hk*P)*Fk.t() + Qhat;
	P = max(P, Q*0.001);

	Spos = Hk*Spred*Hk.t();	

	return predpath;

}

void estimateCovariance()
{
	temp = innov*innov.t();
	temp.copyTo(C[cpos]);
	cpos++;
	Ck = cv::Mat::zeros(n, n, CV_32F);
	if (cpos >= clen) 
	{ 
		cpos = 0;
	}
	if (start_est >= clen)
	{ 
		
		for (int i = 0; i < clen; i++)
		{
			Ck = Ck + C[i];
		}
		Ck = Ck / (float) clen;		
	
		//std::cout << Ck;		
		
		Qhat = K*Ck*K.t();
		Rhat = Ck + Hk*Skk*Hk.t();
		//std::cout << Rhat;
	}
	else
	{
		start_est++;
		Q.copyTo(Qhat);
		R.copyTo(Rhat); 
	}

}

void computeConfidence()
{
	Pheli = (cv::Mat_<float>(3,3, CV_32F) << sigma_heli_x,0,0, 0,sigma_heli_y,0, 0,0,sigma_heli_z );
	Ptot = Pheli + Spos;
	float confx = epsilon / (sqrt(Ptot.at<float>(0,0)) * sqrt(2.0) );
	float confy = epsilon / (sqrt(Ptot.at<float>(1,1)) * sqrt(2.0) );
	float confz = epsilon / (sqrt(Ptot.at<float>(2,2)) * sqrt(2.0) );
	confidence = std::min(std::min(erf(confx), erf(confy)), erf(confz));
}

int main(int argc, char **argv)
{
  ROS_INFO("started!");
  initFilter();
  ROS_INFO("initialized");
  
  ros::init(argc, argv, "platform_estimator");
  ros::NodeHandle n;

  //subscribe to the platform sensor position measurements
  //ros::Subscriber sub = n.subscribe("/platform_sensor/pose", 10, sensorCallback);

  //subscribe to the platform sensor position measurements - from Vicon
  ros::Subscriber sub0 = n.subscribe("/target/pose", 1, sensorCallback);

  //subscribe to the absolute orientation of the helicopter
  ros::Subscriber sub4 = n.subscribe("/heli/pose", 1, heliPoseCallback);

  //subscribe to the helicopter control inputs
  ros::Subscriber sub1 = n.subscribe("/heli/control", 1, helicontrolCallback);

  //subscribe to the helictoper control error covariance
  ros::Subscriber sub2 = n.subscribe("/heli/controlerrorposcov", 1, helicontrolposcovCallback);
  ros::Subscriber sub3 = n.subscribe("/heli/controlerrorvelcov", 1, helicontrolvelcovCallback);

  //publish the estimated position, velocity, heading, acceleration
  ros::Publisher platform_est = n.advertise<geometry_msgs::Pose>("platform_estimate/pose", 10);

  //publish the predicted path when a new measurement arrives
  ros::Publisher predicted_path = n.advertise<nav_msgs::Path>("platform_estimation/pred_path", 10);

  //publish the scalar position covariance
  ros::Publisher position_cov = n.advertise<geometry_msgs::Vector3>("platform_estimate/pred_pos_cov", 10);

  //publish the velocity and acceleration estimates
  ros::Publisher veloc_est = n.advertise<geometry_msgs::Vector3>("platform_estimate/velocity", 10);
  ros::Publisher accel_est = n.advertise<geometry_msgs::Vector3>("platform_estimate/accel", 10);

  //publish the landing region confidence
  ros::Publisher conf_pub = n.advertise<std_msgs::Float32>("platform_estimate/confidence", 10);

  //estimate platform state at 20 Hz
  //to do:  make this rate a parameter that can be varied at runtime
  ros::Rate loop_rate(50);

  nav_msgs::Path predpath;
  predpath.header.frame_id = "/my_frame";
  predpath.header.stamp = ros::Time::now();

  //publish platform state and covariance	
  //geometry_msgs::PoseWithCovariance platform_pose;
  geometry_msgs::Pose platform_pose;
  geometry_msgs::Vector3 position_covariance;
  geometry_msgs::Vector3 vel_msg;
  geometry_msgs::Vector3 acc_msg;

  std_msgs::Float32 conf_msg;

  //setup Marker for error bounds:
  ros::Publisher error_pub = n.advertise<visualization_msgs::Marker>("/platform_estimation/platform_error", 1);
  uint32_t cylinder = visualization_msgs::Marker::CYLINDER;
  visualization_msgs::Marker err;
  err.header.frame_id = "/my_frame";
  err.header.stamp = ros::Time::now();
  err.ns = "platform_sim";
  err.id = 1;
  err.type = cylinder;
  err.action = visualization_msgs::Marker::ADD;

  err.scale.x = 0.1;
  err.scale.y = 0.1;
  err.scale.z = 0.1;

  err.color.r = 0.0f;
  err.color.g = 0.5f;
  err.color.b = 0.4f;
  err.color.a = 0.7f;

  err.lifetime = ros::Duration();

  err.pose.orientation.x = 0.0;
  err.pose.orientation.y = 0.0;
  err.pose.orientation.z = 0.0;
  err.pose.orientation.w = 1.0;

  //setup data for covariance estimation
  C = new cv::Mat[clen];  //array of Ck matrices
  ROS_INFO("initialized C matrix");

  while (ros::ok())
  {

	//compute Jacobian
	Fk = computeJacobian(xhatkmkm);
	//ROS_INFO("Jacobian Computed");
	
	//run prediction step
	predictFilter();
	//ROS_INFO("Prediction");

	//check to see if measurement has been received:
	if (newMeasurement == 0)
	{
		delayUpdatefilter();	
	}
	else if (newMeasurement == 1)
	{
		updateFilter();
		estimateCovariance();

		//use adapted values of Q and R
		//Qhat.copyTo(Q);
		Rhat.copyTo(R);

		//update path prediction
  		predpath = pathPrediction(100);
		predicted_path.publish(predpath);

		//compute confidence
		computeConfidence();
		conf_msg.data = confidence;
		conf_pub.publish(conf_msg);
			
		// compute error bound circle for rviz
		xcov = Spos.at<float>(0,0);
		ycov = Spos.at<float>(1,1);
		err.pose.position.x = zpred.at<float>(0,0);
    		err.pose.position.y = zpred.at<float>(1,0);
    		err.pose.position.z = 0;
		err.scale.x = sqrt(xcov)*2;
  		err.scale.y = sqrt(ycov)*2;
		error_pub.publish(err);
		// and also publish for debugging
		position_covariance.x = xcov;
		position_covariance.y = ycov;
		position_cov.publish(position_covariance);
		
		newMeasurement = 0;
		//ROS_INFO("prediction made");
	}

	//advance one time step
	delayFilter();

	zkest = Hk * xhatkk;

	platform_pose.position.x = zkest.at<float>(0,0);
	platform_pose.position.y = zkest.at<float>(1,0);
	platform_pose.position.z = zkest.at<float>(2,0);
	q_platform.setRPY(proll, ppitch, lowpassYaw(pyaw));
	tf::quaternionTFToMsg(q_platform, platform_pose.orientation);

	vel_msg.x = xhatkk.at<float>(1,0); //x velocity
	vel_msg.y = xhatkk.at<float>(4,0); //y velocity
	vel_msg.z = xhatkk.at<float>(7,0); //z velocity

	acc_msg.x = xhatkk.at<float>(0,0); //x acceleration
	acc_msg.y = xhatkk.at<float>(3,0); //y acceleration
	acc_msg.z = xhatkk.at<float>(6,0); //z acceleration

	veloc_est.publish(vel_msg);
	accel_est.publish(acc_msg);
	platform_est.publish(platform_pose);

        ros::spinOnce();

        loop_rate.sleep();

  }

  return 0;
}
