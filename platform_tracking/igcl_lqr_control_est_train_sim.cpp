#include "ros/ros.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>

#include <tf/tf.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Pose.h>
#include "controllers.h"
#include <opencv2/core/core.hpp>
#include <std_msgs/Float32.h>

#define  MM2M    0.001   //converts milimiters to meters.
#define  RAD2DEG 180/3.1415926536  //converts radians to degrees
#define  DEG2RAD 3.1415926536/180  //converts degrees to radians
#define  GRAVITY_MS2 9.80665

//#define CONTROL_SAMPLING_TIME 0.033
#define K_P_to_V 1.0
#define TIME_AHEAD 0.2
#define ALT_REF_MODE_0 0.75
#define ALT_REF_MODE_1 0.75
#define ALT_REF_MODE_2 0.15
#define VELOCITY_LIMIT 0.25
#define LANDING_RAMP 0.025
#define CONF_THRESH_MODE_2 0.50

#define XBOX 1
#define REALFLIGHT 2

#define MCX 1
#define CX 2
#define ARDRONE 3

#define LOG_ON 1

//inline float max(float a, float b) { return a > b ? a : b; }
//inline float min(float a, float b) { return a < b ? a : b; }

//sensor_msgs::Joy control_outputs_msg;
//ros::Publisher control_out_pub;

using namespace std;

ros::Publisher cmd_pub;

int control_type = XBOX;
int rotorcraft_type = MCX;

bool heli_take_off;
bool heli_land;
bool heli_land_vs_fly;
bool heli_switch_camera;
bool auto_vs_manual;
bool manual_control;
bool auto_vs_manual_val_prev;

tf::Matrix3x3 R_P_H, R_H_G;
tf::Vector3 r_P_H_G, r_H_P_G, r_H_P_H;
tf::Vector3 v_P_H_G, v_H_P_G, v_H_P_H;
tf::Quaternion q_heli, q;

double x_ref = 0.0;
double y_ref = 0.0;
double z_ref = 1.0;
double ulon_gain = 0.1;
double ulat_gain = 0.1;
double ucol_gain = 0.1;
double control_limit = 0.2;

AttitudeTrim        att_trim    =  {3.5,1,0};//{ 3.5, 2, 0 };// { 5, 4.5, 0 };// phi, the, psi  // for other white heli { -3, 0 , 0 }
ServoTrim           servo_trim  =  {0, 0, 0 , 0}; // lon, lat, thro, tail
ServoDeflection     servo_def   = { 0, 0, 0, 0 };

double x, y, z, the, phi, psi, u, v, w;
double x_target, y_target, z_target, the_target, phi_target, psi_target, u_target, v_target, w_target, yaw_error, roll_error, pitch_error;
double ax_target, ay_target, az_target;
double joy_lat, joy_lon, joy_col, joy_ped;

double ulon, ulat, uped, ucol, ucol_prev;

int masterFlag;
int localmasterFlag;

double u_ref, v_ref, w_ref, alt_ref, psi_ref, psiDot_ref, vertVel_ref, x_err, y_err, z_err;

double poseTime = 0;
double poseTime_prev = 0;
double x_prev, y_prev, z_prev;

double poseTime_target = 0;
double poseTime_prev_target = 0;
double x_prev_target, y_prev_target, z_prev_target;
double vx_target, vy_target, vz_target;

double target_pose_x, target_pose_y, target_pose_z, target_pose_roll, target_pose_pitch, target_pose_yaw;
double heli_pose_x, heli_pose_y, heli_pose_z;

cv::Mat xc; // controller state vector
cv::Mat Ac; // controller system matrix
cv::Mat Bc; // input matrix
cv::Mat Cc; // output matrix
cv::Mat Dc; // passthrough matrix

cv::Mat xvc; // vertical controller state vector
cv::Mat Avc; // controller system matrix
cv::Mat Bvc; // input matrix
cv::Mat Cvc; // output matrix
cv::Mat Dvc; // passthrough matrix
double vert_trim, w_cmd;

//PID controllers
pid         rollPID;
pid         pitchPID;
pid         uPID;
pid         vPID;
pid         xPID;
pid         yPID;

//the following are still old pid loops:
pid         yawPID;

PIDContrGains        pidContrGains;

//NotchFilter     roll_notchfilter( 12.92 , 0.2, CONTROL_SAMPLING_TIME );
//NotchFilter     pitch_notchfilter( 12.08 , 0.2, CONTROL_SAMPLING_TIME );

//covariance estimation data
cv::Mat xerr, ctemp, Ck, muk;
cv::Mat* C;
cv::Mat* mu;
int cpos = 0;
int clen = 25;
int start_est = 0;
float confidence;
int data_logged = 0;  //flag so that data is logged once per landing

void trainvelCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	//sensor provides relative velocity of platform with respect to heli in global frame
	v_P_H_G = tf::Vector3 (msg->x, msg->y, msg->z);

	//relative velocity of heli in global frame
	v_H_P_G = -v_P_H_G;

	//relative velocity of heli in heli frame
	v_H_P_H = R_H_G.transpose() * v_H_P_G;

}

void trainaccCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	ax_target = msg->x;
	ay_target = msg->y;
	az_target = msg->z;

}

void confidenceCallback(const std_msgs::Float32::ConstPtr& msg)
{
	confidence = msg->data;

}

void joystickCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	//ROS_INFO("Joystick callback");


	// RealFlight    heli_pose_x = P_T_Horg.x();
    	//heli_pose_y = P_T_Horg.y();
    	//heli_pose_z = P_T_Horg.z();
	const float maxHorizontalSpeed = 1; // use 0.1f for testing and 1 for the real thing
	joy_lon  = max(min(msg->axes[1], maxHorizontalSpeed), -maxHorizontalSpeed);
	joy_lat  = max(min(msg->axes[0], maxHorizontalSpeed), -maxHorizontalSpeed);
	joy_col  = max(min(msg->axes[2]/0.685, 1.0), -1.0);
	joy_ped = max(min(msg->axes[4]/1.0, 1.0), -1.0);

	auto_vs_manual = msg->buttons[0];
	if (auto_vs_manual == true)
	{
		manual_control = false;
	}
	else
	{
		manual_control = true;    //heli_pose_x = P_T_Horg.x();
	    //heli_pose_y = P_T_Horg.y();
	    //heli_pose_z = P_T_Horg.z();
		resetControllers();
	}

	heli_land_vs_fly = msg->buttons[1];
	if (heli_land_vs_fly == true)
	{
		heli_land = true;
	}
	else
	{
		heli_land = false;
	}
}

void trainposCallback(const geometry_msgs::Pose::ConstPtr& msg)
{
	//sensor provides relative position of platform with respect to heli in global frame
	//and global orientation of heli
	//controller needs position of heli with respect to train, in heli reference
	// and height of heli above train in train (or global) frame
	r_P_H_G = tf::Vector3 (msg->position.x, msg->position.y, msg->position.z);
	
	//relative orientation of platform with respect to heli	
	tf::quaternionMsgToTF(msg->orientation, q);
	R_P_H = tf::Matrix3x3 (q);

	//relative position of heli with respect to platform, in global reference
	r_H_P_G = -r_P_H_G;
	
	//global orientation of heli, to rotation matrix
	R_H_G = tf::Matrix3x3 (q_heli);

	//relative position of heli with respect to platform, in heli reference
	r_H_P_H = R_H_G.transpose() * r_H_P_G;

	//get relative heading of platform
	tf::Matrix3x3(q).getRPY(roll_error, pitch_error, yaw_error);
   	yaw_error = yaw_error * RAD2DEG;
	
}

void heliOrientationCallback( const geometry_msgs::Quaternion::ConstPtr& msg)
{
	tf::quaternionMsgToTF(*msg, q_heli);
}


/* resetController
 *
 * reset the controllers
 */
void resetControllers ()
{
    // reset the integrators in the PID controller
    yawPID.reset();

    //initialize state of lqg controller to zeros
    xc = cv::Mat::zeros(Ac.cols, 1, CV_64F);
    //std::cout << "xc reset = " << xc << "\n";

    //initialize state of vertical controller
    xvc = cv::Mat::zeros(Avc.cols, 1, CV_64F);
    //define vertical trim as last manual input before switching to controller mode
    vert_trim = ucol;
}

/* initialize the controllers */
void InitControllers()
{
	if (rotorcraft_type == MCX)
	{
		//initialize the PID gains, don't need to lock at this point
		pidContrGains.yaw.kP = 0.005; //0.0025 //0.006
		pidContrGains.yaw.kD = 0.0; //0
		pidContrGains.yaw.kI = 0.000; //0.005
		pidContrGains.yaw.tau_filter = 0.1;

		//Define lqg long/lat controller discrete system matrix values
		float dt = 0.01;
		Ac = (cv::Mat_<double>(4,4) << 0.8437,0,0,0, 0.009196,1,0,0, 0,0,0.8437,0, 0,0,0.009196,1);
		Bc = (cv::Mat_<double>(4,2) << 0.07357,0, 0.0003783,0, 0,0.03678, 0,0.0001891 );
		Cc = (cv::Mat_<double>(2,4) << 1.487,2.529,0,0, 0,0,0.8925,3.124 );
		Dc = (cv::Mat_<double>(2,2) << 0,0, 0,0);
	    	xc = cv::Mat::zeros(4, 1, CV_64F);
	    	std::cout << "xc init = " << xc << "\n";

		//Define vertical controller system matrix values
		//converted from continuous to discrete using c2d in Matlab 
		//with dt = 0.01
		Avc = (cv::Mat_<double>(2,2) << 0.7787, 0, 0.008848, 1 );
		Bvc = (cv::Mat_<double>(2,1) << 0.07078, 0.0003687 );
		Cvc = (cv::Mat_<double>(1,2) << 2.5, 4.25 );
		Dvc = (cv::Mat_<double>(1,1) << 0 );

	}
}

void errorCovariance()
{
  ctemp = xerr*xerr.t();
  ctemp.copyTo(C[cpos]);
  xerr.copyTo(mu[cpos]);
  cpos++;
  Ck = cv::Mat::zeros(6, 6, CV_32F);
  muk = cv::Mat::zeros(6,1, CV_32F);
  if (cpos >= clen) 
  { 
    cpos = 0;
  }
  //if we've recieved clen measurements:
  if (start_est >= clen)
  {
    //ROS_INFO("averaging:");
    //then sum elements of array:
    for (int i = 0; i < clen; i++)
    {
      Ck = Ck + C[i];
      muk = muk + mu[i];
      //ROS_INFO("loop iteration");
    }
    //then find the average:
    Ck = Ck / (float) clen;
    muk = muk / (float) clen;
    //std::cout << Ck << "\n";		
  }
  else
  {
    start_est++;
  }

}

cv::Mat runLongLatController(cv::Mat e)
{
	xc = Ac*xc + Bc*e;
	//std::cout << "xc = " << xc << "\n";

	return (Cc * xc + Dc * e);
}

cv::Mat runVertController(cv::Mat w_err)
{
	xvc = Avc*xvc + Bvc*w_err;
	return (Cvc*xvc + Dvc*w_err);
}

int main(int argc, char **argv)
{

	//initialize control error statistics matrix arrays
	C = new cv::Mat[clen];
	mu = new cv::Mat[clen];

	//Initialize ROS
	ros::init(argc, argv, "igcl_pid_control_est_train_sim");

	ROS_INFO("Initialize ROS");
	ros::NodeHandle n;

	//if data logging is on, open data file
	ROS_INFO("Opening Landing Log File...");
	ofstream logfile;
	if (LOG_ON == 1)
	{
		logfile.open("/home/user/landing_error.txt", ios::out | ios::app);
		if (logfile.is_open())
		{
			logfile << endl;
			logfile << "Starting ROS " << ros::Time::now() << endl;
			logfile << "time \t x_error \t y_error \t platform_heading \t confidence" << endl;
			ROS_INFO("File Opened");
		}
		else
		{
			ROS_INFO("Error opening landing log file");
		}
	}

	std::string igcl_control_type;
	n.param<std::string>("/igcl/joystick_type", igcl_control_type, "unknown");
	if (igcl_control_type == "realflight")
	{
		control_type = REALFLIGHT;
		ROS_INFO("joystick: realflight");
	}
	else
	{
		ROS_INFO("joystick: unknown");
	}

	ros::Duration(0.5).sleep();

	std::string igcl_rotorcraft_type;
	n.param<std::string>("/igcl/rotorcraft_type", igcl_rotorcraft_type, "unknown");
	if (igcl_rotorcraft_type == "mcx")
	{
		rotorcraft_type = MCX;
		ROS_INFO("rotorcraft: mcx");
	}
	else if (igcl_rotorcraft_type == "cx")
	{
		rotorcraft_type = CX;
		ROS_INFO("rotorcraft: cx");
	}
	else
	{
		ROS_INFO("rotorcraft: unknown");
	}

	//Initialize joystick inputs
	ROS_INFO("initialize joystick inputs");
	heli_take_off = false;
	heli_land = false;
	heli_switch_camera = false;
	auto_vs_manual = false;
	manual_control = true;
	auto_vs_manual_val_prev = false;
	joy_lon  = 0;
	joy_lat  = 0;
	joy_col  = -1;
	joy_ped = 0;
	int heli_land_mode = 0;
	alt_ref = ALT_REF_MODE_0;

	localmasterFlag = 1;

	//Initialize controllers
	InitControllers();

	//initialize the controller gains
	yawPID.init(pidContrGains.yaw.kP, pidContrGains.yaw.kD, pidContrGains.yaw.kI, CONTROL_SAMPLING_TIME, pidContrGains.yaw.tau_filter);

	ros::Subscriber joystick_sub = n.subscribe("/joy", 1, joystickCallback);
	//ros::Subscriber pose_sub = n.subscribe("/target/pose", 1, PoseCallback);

	//subscribe to estimated platform position, velocity, and accelerations:
	ros::Subscriber plat_pos_sub = n.subscribe("/platform_estimate/pose", 1, trainposCallback);
	ros::Subscriber plat_vel_sub = n.subscribe("/platform_estimate/velocity", 1, trainvelCallback);
	ros::Subscriber plat_acc_sub = n.subscribe("/platform_estimate/accel", 1, trainaccCallback);

	//subscribe to the pose of the platform in the helicopter frame
	//ros::Subscriber pose_sub = n.subscribe("/target/pose", 1, PoseCallback);

	//subscribe to the global orientation of the helicopter
	ros::Subscriber heli_att_sub = n.subscribe("/heli/orientation", 1, heliOrientationCallback);

	//subscribe to the platform estimation and control confidence:
	ros::Subscriber plat_conf = n.subscribe("/platform_estimate/confidence", 1, confidenceCallback);

	//publish control error covariance
	ros::Publisher pub3 = n.advertise<geometry_msgs::Vector3>("/heli/controlerrorposcov", 1);
  	ros::Publisher pub4 = n.advertise<geometry_msgs::Vector3>("/heli/controlerrorvelcov", 1);
	geometry_msgs::Vector3 control_cov_pos;
  	geometry_msgs::Vector3 control_cov_vel;

	if ((rotorcraft_type == MCX) || (rotorcraft_type == CX))
	{
		cmd_pub = n.advertise<geometry_msgs::Twist>("/spektrum/cmd", 1);
	}
	else if (rotorcraft_type == ARDRONE)
	{
		cmd_pub = n.advertise<geometry_msgs::Twist>("/ardrone/cmd_vel", 1);
	}

	ros::AsyncSpinner spinner(2); // Use 2 threads
	spinner.start();

	ros::Rate loop_rate(1.0/CONTROL_SAMPLING_TIME);

	ros::Time heli_approach_start_time = ros::Time::now();

	ROS_INFO("Start main loop");
	ROS_INFO("heli_land_mode = 0");
	while (ros::ok() && localmasterFlag )
	{
		if (manual_control == true) // manual mode
		{
			ulat =  joy_lat;
			ulon =  joy_lon;
			ucol =  joy_col;
			uped =  joy_ped;

			//altPID.bumplessupdate(ucol);

			//initialize controllers
			resetControllers();

			control_cov_pos.x = 0;
			control_cov_pos.y = 0;
			control_cov_pos.z = 0;

			control_cov_vel.x = 0;
			control_cov_vel.y = 0;
			control_cov_vel.z = 0;

			data_logged = 0; //reset data log flag for next time we go to controller mode

			//make sure controllers are initialized when we switch to controller mode
			resetControllers();

			//ROS_INFO("Man ulat = %f, ulon = %f, ucol = %f, uped = %f", ulat, ulon, ucol, uped);
		}
		else // controller mode
		{
			//use manual control for default
			ulat =  joy_lat;
			ulon =  joy_lon;
			ucol =  joy_col;
			uped =  joy_ped;

			//position error, in heli frame = position of heli with respect to train
			tf::Vector3 P_H_error = r_H_P_H;
			z = r_P_H_G.z();  //altitude = position of heli with respect to train, in train reference
			w = v_P_H_G.z();
			//this should be r_H_P_G.z, but that signal is negative?			
			ROS_INFO("z:  %f", z);
	
			//desired velocity from position error
			tf::Vector3 V_H_desired = P_H_error * K_P_to_V;
			//ROS_INFO("Velocity Targ (%.2f, %.2f, %.2f), Act (%.2f, %.2f, %.2f)", V_H_desired.x(), V_H_desired.y(), V_H_desired.z(), u, v, w);

			if (heli_land == false)
			{
				heli_land_mode = 0;
				alt_ref = ALT_REF_MODE_0;
				w_ref = 0;
			}
			else if (heli_land_mode == 0)
			{
				heli_land_mode = 1;
				heli_approach_start_time = ros::Time::now();
			}


			if (heli_land_mode == 1)
			{
				alt_ref = ALT_REF_MODE_1;

				ROS_INFO("mode1 (%f, %f, %f) %f", fabs(r_H_P_H.x()), fabs(r_H_P_H.y()), fabs(ALT_REF_MODE_1-z), ros::Time::now().toSec() - heli_approach_start_time.toSec());
				if (fabs(P_H_error.x()) < 0.15 && fabs(P_H_error.y()) < 0.15 && fabs(ALT_REF_MODE_1-z) < 0.15)
				{
					if ((ros::Time::now().toSec() - heli_approach_start_time.toSec()) > 3.0)
					{
						heli_land_mode = 2;
						ROS_INFO("Mode 2");
					}
				}
				else
				{
					heli_approach_start_time = ros::Time::now();
				}
				//ROS_INFO("Mode 1 Alt Ref = %f", alt_ref);
			}
			//mode 2:  slowly descend to low altitude tracking
			else if (heli_land_mode == 2)
			{
				if (alt_ref > ALT_REF_MODE_2)
				{
					if (confidence > CONF_THRESH_MODE_2)
					{
						alt_ref = alt_ref - 0.001;  // 1mm/0.01 sec = 10cm/sec descent
						w_ref = -0.1;  // = -0.1 m/s
					}
				}
				else
				{
					alt_ref = ALT_REF_MODE_2;
					w_ref = 0.0;
				}
				//ROS_INFO("Mode 2 Alt Ref = %f", alt_ref);

				ROS_INFO("mode2 (%f, %f, %f", fabs(P_H_error.x()), fabs(P_H_error.y()), fabs(ALT_REF_MODE_2-z));
				if (confidence > CONF_THRESH_MODE_2 && fabs(ALT_REF_MODE_2-z) < 0.05)
				{
					ROS_INFO("Error (%.2f, %.2f, %.2f)", P_H_error.x(), P_H_error.y(), P_H_error.z());
					heli_land_mode = 3;
					ROS_INFO("heli_land_mode = 3");
					ucol_prev = ucol;
				}
			}

			//mode 3: ramp down throttle to 0 (-1)
			if (heli_land_mode == 3)
			{
				alt_ref = 0.02;
				w_ref = -0.2;
				ROS_INFO("Height Above Platform: %f", (z - z_target));
				if (fabs(z - z_target) < 0.10)
				{
					ucol = -1;
					heli_land_mode = 4;
					if (data_logged == 0 && LOG_ON == 1)
					{
						data_logged = 1;
						ROS_INFO("Landed at:  x[%f]  y[%f]", P_H_error.x(), P_H_error.y());
						logfile << ros::Time::now() << "\t" << P_H_error.x() << "\t" << P_H_error.y() << "\t" << psi_target << "\t" << confidence << endl;
					}
				}
			}

			//heading control
			//double psi_error = psi_target - psi;
			double psi_error = yaw_error;
			if (psi_error > 180.0)
				psi_error = psi_error - 360.0;
			if (psi_error < -180.0)
				psi_error = psi_error + 360.0;

			servo_def.deltaPed = yawPID.process(psi_error, 0);
			uped = servo_def.deltaPed;
			//ROS_INFO("u_yaw = %.2f, yaw = %.2f, y_err = %.2f, ped = %.2f", psi_target, psi, psi_error, uped);

			// set references
			u_ref = min(max(V_H_desired.x(), -VELOCITY_LIMIT), VELOCITY_LIMIT);
			v_ref = min(max(V_H_desired.y(), -VELOCITY_LIMIT), VELOCITY_LIMIT);

			//lateral and longitudinal lqg controller
			cv::Mat ctrl_input = (cv::Mat_<double>(2,1) << v_ref - v, u_ref - u );
			//ROS_INFO("u_ref = %f, v_ref = %f, u = %f, v = %f", u_ref, v_ref, u, v);

			//std::cout << "lqg_in = " << lqg_input << "\n";
			cv::Mat ctrl_output = runLongLatController(ctrl_input);
			//std::cout << "lqg_out = " << lqg_output << "\n";

			ulat = ctrl_output.at<double>(0,0);
			ulon = ctrl_output.at<double>(0,1);


			//vertical control
			w_cmd = max(min(((alt_ref - z)*0.25 + w_ref), 0.75), -0.75);
			double w_e = w_cmd - w;
			cv::Mat w_err = (cv::Mat_<double>(1,1) << w_e );
			cv::Mat vert_control = runVertController(w_err);
			if (heli_land_mode == 4)
			{
				ucol = -1;
			}
			else
			{
				
				ucol = vert_control.at<double>(0,0);
			}

			//limit control signals
			ulon = min(max(ulon, -1.0), 1.0);
			ulat = min(max(ulat, -1.0), 1.0);
			ucol = min(max(ucol, -1.0), 1.0);
			uped = min(max(uped, -1.0), 1.0);

			//copy control error into matrix:
			xerr = (cv::Mat_<float>(6,1) << P_H_error.x(), 0.0, P_H_error.y(), 0.0, z);

			//update control error covariance calculation
			errorCovariance();

			//publish control error covariance data
			control_cov_pos.x = Ck.at<float>(0,0);
    			control_cov_pos.y = Ck.at<float>(2,2);
    
    			control_cov_vel.x = Ck.at<float>(1,1);
    			control_cov_vel.y = Ck.at<float>(3,3);
		}

		pub3.publish(control_cov_pos);
		pub4.publish(control_cov_vel);

		geometry_msgs::Twist cmd_msg;
		cmd_msg.linear.x  = ulon;
		cmd_msg.linear.y  = ulat;
		cmd_msg.linear.z  = ucol;
		cmd_msg.angular.x = 0;
		cmd_msg.angular.y = 0;
		cmd_msg.angular.z = uped;
		cmd_pub.publish(cmd_msg);

		//sleep until next control update
		loop_rate.sleep();
	}

	//close log file
	if (LOG_ON == 1)
	{
		logfile.close();
	}
	// publish the controller_info_msg:
	ROS_INFO("Finish");

	return 0;
}
