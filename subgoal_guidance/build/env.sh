#!/bin/sh


if [ $# -eq 0 ] ; then
    /bin/echo "Entering build environment at /home/user/ros_igcl_packages/subgoal_guidance/build"
    . /home/user/ros_igcl_packages/subgoal_guidance/build/setup.sh
    $SHELL -i
    /bin/echo "Exiting build environment at /home/user/ros_igcl_packages/subgoal_guidance/build"
else
    . /home/user/ros_igcl_packages/subgoal_guidance/build/setup.sh
    exec "$@"
fi


