FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/subgoal_guidance/msg"
  "../src/subgoal_guidance/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_cpp"
  "../msg_gen/cpp/include/subgoal_guidance/State2D.h"
  "../msg_gen/cpp/include/subgoal_guidance/Control2D.h"
  "../msg_gen/cpp/include/subgoal_guidance/ControlSequence.h"
  "../msg_gen/cpp/include/subgoal_guidance/State2DArray.h"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
