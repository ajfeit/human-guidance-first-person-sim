FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/subgoal_guidance/msg"
  "../src/subgoal_guidance/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_gensrv_lisp"
  "../srv_gen/lisp/GetSubgoals.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_GetSubgoals.lisp"
  "../srv_gen/lisp/ComputeControlSequence.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_ComputeControlSequence.lisp"
  "../srv_gen/lisp/ComputeOptimalVelocity.lisp"
  "../srv_gen/lisp/_package.lisp"
  "../srv_gen/lisp/_package_ComputeOptimalVelocity.lisp"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_gensrv_lisp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
