FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/subgoal_guidance/msg"
  "../src/subgoal_guidance/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_gensrv_cpp"
  "../srv_gen/cpp/include/subgoal_guidance/GetSubgoals.h"
  "../srv_gen/cpp/include/subgoal_guidance/ComputeControlSequence.h"
  "../srv_gen/cpp/include/subgoal_guidance/ComputeOptimalVelocity.h"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_gensrv_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
