FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/subgoal_guidance/msg"
  "../src/subgoal_guidance/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_gensrv_py"
  "../src/subgoal_guidance/srv/__init__.py"
  "../src/subgoal_guidance/srv/_GetSubgoals.py"
  "../src/subgoal_guidance/srv/_ComputeControlSequence.py"
  "../src/subgoal_guidance/srv/_ComputeOptimalVelocity.py"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_gensrv_py.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
