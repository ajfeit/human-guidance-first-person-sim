FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/subgoal_guidance/msg"
  "../src/subgoal_guidance/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_py"
  "../src/subgoal_guidance/msg/__init__.py"
  "../src/subgoal_guidance/msg/_State2D.py"
  "../src/subgoal_guidance/msg/_Control2D.py"
  "../src/subgoal_guidance/msg/_ControlSequence.py"
  "../src/subgoal_guidance/msg/_State2DArray.py"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_py.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
