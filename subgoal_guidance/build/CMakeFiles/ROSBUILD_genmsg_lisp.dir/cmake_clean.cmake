FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/subgoal_guidance/msg"
  "../src/subgoal_guidance/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_lisp"
  "../msg_gen/lisp/State2D.lisp"
  "../msg_gen/lisp/_package.lisp"
  "../msg_gen/lisp/_package_State2D.lisp"
  "../msg_gen/lisp/Control2D.lisp"
  "../msg_gen/lisp/_package.lisp"
  "../msg_gen/lisp/_package_Control2D.lisp"
  "../msg_gen/lisp/ControlSequence.lisp"
  "../msg_gen/lisp/_package.lisp"
  "../msg_gen/lisp/_package_ControlSequence.lisp"
  "../msg_gen/lisp/State2DArray.lisp"
  "../msg_gen/lisp/_package.lisp"
  "../msg_gen/lisp/_package_State2DArray.lisp"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_lisp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
