#!/usr/bin/env python
import rospy
from subgoal_guidance.msg import *
from subgoal_guidance.srv import *
from geometry_msgs.msg import Vector3
#from visualization_msgs.msg import Marker
#from visualization_msgs.msg import MarkerArray
from numpy import *
#import matplotlib.pyplot as plt
from sympy import symbols, solve, Matrix, diff, Subs, Point, Line
import math

if __name__ == '__main__':
	xstart = Vector3()
	xstart.x = 0
	xstart.y = 0
	xgoal = State2D()
	xgoal.x = 1
	xgoal.y = 1
	xgoal.vx = 0.0
	xgoal.vy = 0.5
	rospy.wait_for_service('compute_optimal_velocity')
	try:
		compute_optimal_velocity = rospy.ServiceProxy('compute_optimal_velocity', ComputeOptimalVelocity)
		resp = compute_optimal_velocity(xgoal, xstart)
		rospy.loginfo("Complete")
		print ("xstart: " + str(resp.startstate.x) + " , " + str(resp.startstate.vx) + " , " + str(resp.startstate.y) + " , " + str(resp.startstate.vy))
		print ("xgoal: " + str(xgoal.x) + " , " + str(xgoal.vx) + " , " + str(xgoal.y) + " , " + str(xgoal.vy))
		print ("J opt: " + str(resp.jopt))
	except rospy.ServiceException, e:
		print "Service Call Failed: %s" %e
