#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest('subgoal_guidance')
from subgoal_guidance.msg import *
from subgoal_guidance.srv import *
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Vector3

class Sequence(object):
	def __init__(self):
		rospy.init_node('get_subgoals_server', anonymous=True)
		self.srv = rospy.Service('get_subgoals', GetSubgoals, self.handleGetSubgoals)
		self.markerpub = rospy.Publisher('subgoals/markers/', MarkerArray)
		#self.filename = filename
		rospy.wait_for_service('compute_optimal_velocity')
		self.compute_optimal_velocity = rospy.ServiceProxy('compute_optimal_velocity', ComputeOptimalVelocity)
		rospy.loginfo("Ready to Get Subgoals")
		self.run()

	def run(self):
		rospy.spin()
		
	def handleGetSubgoals(self, req):
		#open subgoal data file
		rospy.loginfo("Opening " + str(req.filename))
		sglist = State2DArray()
		f = open(req.filename,'r')
		n = 0
		for line in f:
			linelist = line.split(',', 3)
			if (n == 0 and len(linelist) == 4):
				sg = State2D()
				sg.x = float(linelist[0])
				sg.vx = float(linelist[1])
				sg.y = float(linelist[2])
				sg.vy = float(linelist[3])
				sglist.states.append(sg)
				n = n + 1
			else:
				sg = State2D()
				sgp = Vector3()
				sgp.x = float(linelist[0])
				sgp.y = float(linelist[1])
				print "Subgoal: " + str(sgp.x) + ", " + str(sgp.y)
				try:
					resp = self.compute_optimal_velocity(sglist.states[n-1], sgp)
					sglist.states.append(resp.startstate)
					n = n + 1
				except rospy.ServiceException, e:
					print "Service Call Failed: %s" %e
		rospy.loginfo("Loaded subgoals")
		self.displaySubgoals(sglist.states[::-1])
		sglist.states = sglist.states[::-1]
		return GetSubgoalsResponse(sglist)

	def displaySubgoals(self, sglist):
		markers = MarkerArray()
		i = 1
		for sg in sglist:
			marker = self.createMarker(i)
			marker.pose.position.x = sg.x
			marker.pose.position.y = sg.y
			markers.markers.append(marker)
			i = i + 1
		self.markerpub.publish(markers)

	def createMarker(self, number):
		_id=0
		_type = Marker.CUBE

		tmarker = Marker(type=_type, action=Marker.ADD)
		##marker.header.frame_id = point.header.frame_id
		tmarker.header.frame_id = 'my_frame'
		tmarker.header.stamp = rospy.Time.now()
		tmarker.pose.position.x = 0.0
		tmarker.pose.position.y = 0.0
		tmarker.pose.position.z = 0.0

		tmarker.ns = 'subgoal/' + str(number);
		tmarker.id = int(number);

		#pointxyz
		tmarker.pose.orientation.x = 0.0
		tmarker.pose.orientation.y = 0.0
		tmarker.pose.orientation.z = 0.0
		tmarker.pose.orientation.w = 1

		tmarker.scale.x = 0.1
		tmarker.scale.y = 0.1
		tmarker.scale.z = 0.1

		tmarker.color.r = 0
		tmarker.color.g = 1
		tmarker.color.b = 0
		tmarker.color.a = 1
		tmarker.id = _id
		return tmarker
		
		
if __name__ == '__main__':
	seq = Sequence()
