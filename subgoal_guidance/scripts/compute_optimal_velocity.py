#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest('subgoal_guidance')
from subgoal_guidance.msg import *
from subgoal_guidance.srv import *
#from visualization_msgs.msg import Marker
#from visualization_msgs.msg import MarkerArray
from numpy import *
#import matplotlib.pyplot as plt
from sympy import symbols, solve, Matrix, diff, Subs, Point, Line
import math

class OptimalVelocity(object):
	def __init__(self):
		rospy.init_node('compute_optimal_velocity_server', anonymous=True)
		self.srv = rospy.Service('compute_optimal_velocity', ComputeOptimalVelocity, self.handleOptimalVelocity)
		self.dt = 0.01
		self.a = matrix( [[0, 1, 0, 0], [0, 0, 0, 0], [0, 0, 0, 1], [0, 0, 0, 0]] )
		self.a = eye(4) + self.a*self.dt
		self.b = matrix( [[0, 0], [1, 0], [0, 0], [0, 1]] )
		self.b = self.b*self.dt
		rospy.loginfo("Ready to Compute Optimal Velocity")
		self.run()

	def run(self):
		rospy.spin()

	def handleOptimalVelocity(self, req):
		xpos_init = Matrix( [[req.startpos.x], [req.startpos.y]] )
		xgoal = Matrix( [[req.goalstate.x], [req.goalstate.vx], [req.goalstate.y], [req.goalstate.vy]] )
		#compute vx and vy, iterate over travel time
		RR = self.b*self.b.transpose()
		RR = RR + self.a * self.b * self.b.transpose() * self.a.transpose()
		k = 2
		self.computeCosts(RR, xpos_init, xgoal, k)
		Jlast = self.cost
		k = k + 1
		RR = RR + linalg.matrix_power(self.a,k-1) * self.b * self.b.transpose() * linalg.matrix_power(self.a.transpose(),k-1)
		self.computeCosts(RR, xpos_init, xgoal, k)
		dJ = abs(Jlast - self.cost)
		rospy.loginfo("Iterating over time...")
		while (dJ > 0.004):
			Jlast = self.cost
			k = k + 1
			RR = RR + linalg.matrix_power(self.a,k-1) * self.b * self.b.transpose() * linalg.matrix_power(self.a.transpose(),k-1)
			self.computeCosts(RR, xpos_init, xgoal, k)
			dJ = abs(Jlast - self.cost)
			#print dJ
		xstart = State2D()
		xstart.x = xpos_init[0]
		xstart.y = xpos_init[1]
		xstart.vx = self.vx
		xstart.vy = self.vy
		rospy.loginfo("Request Complete")
		return ComputeOptimalVelocityResponse(xstart, self.cost)

	def computeCosts(self, RR, p0, xg, k):
		vx, vy = symbols('vx, vy')
		x0s = Matrix( [[p0[0]], [vx], [p0[1]], [vy]] )
		dx = xg - x0s
		xf = Matrix( [[(dx[0] - x0s[1]*k*self.dt)], [dx[1]], [(dx[2] - x0s[3]*k*self.dt)], [dx[3]]] )
		#print xf
		cost = self.symbCost(RR, xf, k)
		#print cost
		x1c = diff(cost, vx)
		x2c = diff(cost, vy)
		#print(x1c)
		#print(x2c)
		velsol = solve([x1c, x2c], vx, vy)
		#self.vx = velsol[vx].subs( [ (x, self.pos[0]), (y, self.pos[1]) ] )
		#self.vy = velsol[vy].subs( [ (x, self.pos[0]), (y, self.pos[1]) ] )
		self.vx = velsol[vx]
		self.vy = velsol[vy]
		#self.cost = cost.subs([(x, self.pos[0]), (vx, self.vx), (y, self.pos[1]), (vy, self.vy)])
		self.cost = cost.subs([(vx, self.vx), (vy, self.vy)])

	def symbCost(self, RR, xf, k):
		return (xf.transpose()*linalg.inv(RR)*xf)[0] + 10.0*self.dt*k

if __name__ == '__main__':
	optvel = OptimalVelocity()
