#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest('subgoal_guidance')
from subgoal_guidance.msg import *
from subgoal_guidance.srv import *
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray

class Sequence(object):
	def __init__(self):
		rospy.init_node('get_subgoals_server', anonymous=True)
		self.srv = rospy.Service('get_subgoals', GetSubgoals, self.handleGetSubgoals)
		self.markerpub = rospy.Publisher('subgoals/markers/', MarkerArray)
		#self.filename = filename
		rospy.loginfo("Ready to Get Subgoals")
		self.run()

	def run(self):
		rospy.spin()
		
	def handleGetSubgoals(self, req):
		#open subgoal data file
		rospy.loginfo("Opening " + str(req.filename))
		sglist = State2DArray()
		f = open(req.filename,'r')
		for line in f:
			linelist = line.split(',', 3)
			sg = State2D()
			sg.x = float(linelist[0])
			sg.vx = float(linelist[1])
			sg.y = float(linelist[2])
			sg.vy = float(linelist[3])
			sglist.states.append(sg)
		rospy.loginfo("Loaded subgoals")
		self.displaySubgoals(sglist)
		return GetSubgoalsResponse(sglist)

	def displaySubgoals(self, sglist):
		markers = MarkerArray()
		i = 1
		for sg in sglist.states:
			marker = self.createMarker(i)
			marker.pose.position.x = sg.x
			marker.pose.position.y = sg.y
			markers.markers.append(marker)
			i = i + 1
		self.markerpub.publish(markers)

	def createMarker(self, number):
		_id=0
		_type = Marker.CUBE

		tmarker = Marker(type=_type, action=Marker.ADD)
		##marker.header.frame_id = point.header.frame_id
		tmarker.header.frame_id = 'my_frame'
		tmarker.header.stamp = rospy.Time.now()
		tmarker.pose.position.x = 0.0
		tmarker.pose.position.y = 0.0
		tmarker.pose.position.z = 0.0

		tmarker.ns = 'subgoal/' + str(number);
		tmarker.id = int(number);

		#pointxyz
		tmarker.pose.orientation.x = 0.0
		tmarker.pose.orientation.y = 0.0
		tmarker.pose.orientation.z = 0.0
		tmarker.pose.orientation.w = 1

		tmarker.scale.x = 0.1
		tmarker.scale.y = 0.1
		tmarker.scale.z = 0.1

		tmarker.color.r = 0
		tmarker.color.g = 1
		tmarker.color.b = 0
		tmarker.color.a = 1
		tmarker.id = _id
		return tmarker
		
		
if __name__ == '__main__':
	seq = Sequence()
