from numpy import *
import matplotlib.pyplot as plt
from sympy import symbols, solve, Matrix, diff, Subs, Point, Line
import math
from operator import attrgetter
from subgoal_guidance.msg import State2D
from geometry_msgs.msg import Vector3
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray

class Vertex(object):

	def __init__(self, position):
		self.pos = position

	def toMat(self):
		return Matrix( [[self.pos[0]], [self.vx], [self.pos[1]], [self.vy]] )

	def plotVel(self, ax):		
		ax.quiver(asscalar(self.pos[0]), asscalar(self.pos[1]), float(self.vx), float(self.vy))

	def computeAngle(self):
		vec1 = Matrix( self.connl.pos - self.pos ).transpose()
		vec2 = Matrix( self.connr.pos - self.pos ).transpose()
		vec3 = Matrix( [[self.vx],[self.vy]] )
		sign1 = vec1[0]*vec2[1] - vec1[1]*vec2[0]
		sign2 = vec1[0]*vec3[1] - vec1[1]*vec3[0]

		costh1 = vec1.dot(vec2)/(vec1.norm()*vec2.norm())
		sinth1 = -sign1/(vec1.norm()*vec2.norm())

		costh2 = vec1.dot(vec3)/(vec1.norm()*vec3.norm())
		sinth2 = -sign2/(vec1.norm()*vec3.norm())

		self.angle = math.atan2(sinth1, costh1)
		
		self.velang = math.atan2(sinth2, costh2)

	def compVecang(self, vec):
		vec1 = Matrix( self.connl.pos - self.pos ).transpose()
		vec2 = Matrix( self.connr.pos - self.pos ).transpose()
		vec3 = vec
		#sign1 = vec1[0]*vec2[1] - vec1[1]*vec2[0]
		sign2 = vec1[0]*vec3[1] - vec1[1]*vec3[0]

		#costh1 = vec1.dot(vec2)/(vec1.norm()*vec2.norm())
		#sinth1 = -sign1/(vec1.norm()*vec2.norm())

		costh2 = vec1.dot(vec3)/(vec1.norm()*vec3.norm())
		sinth2 = -sign2/(vec1.norm()*vec3.norm())

		#self.angle = math.atan2(sinth1, costh1)
		
		return math.atan2(sinth2, costh2)		

	def isSubgoal(self):
		opp_angle = self.velang + pi

		#unwrap
		if opp_angle > pi:
			opp_angle = opp_angle - pi
		elif opp_angle < -pi:
			opp_angle = opp_angle + pi
		
		if (((self.velang > self.angle) or (self.velang < 0)) and ((opp_angle < 0) or (opp_angle > self.angle))):
			return True
		else:
			return False	

	def pointsExt(self, vec):
		ang = self.compVecang(vec)
		if ((ang > self.angle) or (ang < 0)):
			return True
		else:
			return False

	def isinWay(self, obs, S, A, xgs):
		#two subgoals on obstruction
		sga = obs.subgoals[0]
		sgb = obs.subgoals[1]
		self.computeCosts(G, S, xgs)
		costself = self.cost
		#print(costself)
		#print(sga.cost)
		#print(sgb.cost)

		#if ((self.dist(xgv) < sga.dist(xgv)) & (self.dist(xgv) < sgb.dist(xgv))):
		if ((costself < sga.cost) & (costself < sgb.cost)):
			self.subgoal = xgv
			return False
		else:
			#find two additional points just inside each subgoal
			sgap = Vertex( sga.pos + 0.0001*(sgb.pos - sga.pos) )
			sgbp = Vertex( sgb.pos + 0.0001*(sga.pos - sgb.pos) )
			
			#compute costs and optimal velocities at these new points
			sgap.computeCosts(G, S, xgs)
			sgbp.computeCosts(G, S, xgs)

			#compute costs from self through each subgoal and two new points
			#cost from start to sga + cost from sga to goal
			self.computeCosts(G, S, sga.toMat())
			costa1 = self.cost + sga.cost
			#cost from start to sgap, etc
			self.computeCosts(G, S, sgap.toMat())
			costa2 = self.cost + sgap.cost
			#cost from start to sgb, etc
			self.computeCosts(G, S, sgb.toMat())
			costb1 = self.cost + sgb.cost
			#cost from start to sgbp, etc
			self.computeCosts(G, S, sgbp.toMat())
			costb2 = self.cost + sgb.cost
	
			#then test
			#if there is an optimal path going through the obstruction
			if ((costa2 < costa1) & (costb2 < costb1)):
				if (costa1 < costb1):
					self.subgoal = sga
					self.color = 'bo'
					return True
				elif (costa1 > costb1):
					self.subgoal = sgb
					self.color = 'ro'
					return True
			else:
				self.subgoal = xgv
				return False

	def dist(self, vert):
		return linalg.norm(vert.pos - self.pos)

class Obstruction(object):
	def __init__(self, verts):
		self.size = len(verts)
		self.vertices = verts

	def __init__(self, k, cov):
		self.size = k
		self.vertices = self.randomObs(k)
		self.comp_opt_vel = cov

	def compCoordlist(self):
		self.vx = []
		self.vy = []
		for v in self.vertices:
			self.vx.append(v.pos[0])
			self.vy.append(v.pos[1])

	def plotObs(self, i):
		self.compCoordlist()
		obmarklist = MarkerArray()
		for vert in self.vertices:
			obmark = createMarker(i)
			obmark.pose.position.x = vert.position[0]
			obmark.pose.position.y = vert.position[1]
			obmarklist.append(obmark)
		return obmarklist
		#ax.fill(self.vx, self.vy, 'b')

	def computeCosts(self, G, S, xg):
		xgs = Matrix(xg)
		self.costs = []
		for v in self.vertices:
			v.computeCosts(G, S, xgs)
			self.costs.append(v.cost)
		self.maxcost = max(self.costs)
		self.mincost = min(self.costs)

	def plotVel(self, ax):
		for v in self.vertices:
			v.plotVel(ax)

	def computeAngles(self):
		for v in self.vertices:
			v.computeAngle()

	def findSubgoals(self):
		self.subgoals = []
		for v in self.vertices:
			if v.isSubgoal():
				self.subgoals.append(v)
		return self.subgoals

	def plotSubgoals(self, ax):
		for sg in self.subgoals:
			ax.plot(sg.pos[0], sg.pos[1], 'go')

	def randomObs(self, k):
		#generate 4 random angles that sum to 360 degrees
		angles = random.random((k ,))*0.873 + 0.349
		c = sum(angles)/(2*pi)
		angles = angles / c
		print(angles)
	
		#determine random offset of obstruction
		#position between 1 and 7 in x and y directions
		offset = random.random((2,))*6 + 1
	
		#generate 4 random distances
		dist = random.random((k,))*0.4 + 0.8

		#compute coordinates of each vertex
		vertlist = []
		ang = 0;
		for i in range(0,k):
			vert = array( [dist[i]*cos(ang), dist[i]*sin(ang)] ) + offset
			ang = ang + angles[i]
			vertex = Vertex(vert, self.comp_opt_vel)
			vertlist.append(vertex)
			#also store left and right neighbors for each vertex
			if i > 0:
				vertlist[i].connl = vertlist[i-1]
				vertlist[i-1].connr = vertlist[i]
			if i==(k-1):
				vertlist[i].connr = vertlist[0]
				vertlist[0].connl = vertlist[i]
		return vertlist

def createMarker(self, number):
		_id=0
		_type = Marker.CUBE

		tmarker = Marker(type=_type, action=Marker.ADD)
		##marker.header.frame_id = point.header.frame_id
		tmarker.header.frame_id = 'my_frame'
		tmarker.header.stamp = rospy.Time.now()
		tmarker.pose.position.x = 0.0
		tmarker.pose.position.y = 0.0
		tmarker.pose.position.z = 0.0

		tmarker.ns = 'subgoal/' + str(number);
		tmarker.id = int(number);

		#pointxyz
		tmarker.pose.orientation.x = 0.0
		tmarker.pose.orientation.y = 0.0
		tmarker.pose.orientation.z = 0.0
		tmarker.pose.orientation.w = 1

		tmarker.scale.x = 0.1
		tmarker.scale.y = 0.1
		tmarker.scale.z = 0.1

		tmarker.color.r = 0
		tmarker.color.g = 1
		tmarker.color.b = 0
		tmarker.color.a = 1
		tmarker.id = _id
		return tmarker

if __name__ == '__main__':

	#given final goal state, and obstruction
	#compute subgoal tree
		# 1) determine which obstruction vertices are subgoals
		# 2) add these subgoals as branches of the final goal state
		# 3) <extend if there are more than one obstruction>

	#given a subgoal tree and a starting point
	#determine the list of subgoals to follow
		# 1) determine which subgoal domain the starting point is in
			#one obstruction: min_{sg} of c(x0, sg) + c(sg, xg)

	#init ros node
	rospy.init_node('get_subgoals_from_obstructions', anonymous=True)
	srv = rospy.Service('get_subgoals_from_obs', GetSubgoalsFromObs, self.handleGetSubgoalsFromObs)
	markerpub = rospy.Publisher('obstructions/markers/', MarkerArray)
	#self.filename = filename
	rospy.wait_for_service('compute_optimal_velocity')
	compute_optimal_velocity = rospy.ServiceProxy('compute_optimal_velocity', ComputeOptimalVelocity)
	rospy.loginfo("Ready to Get Subgoals")

	#generate obstructions
	n = 1  #number of obstructions
	k = 4  #vertices per obstruction
	obslist = []

	for j in range(0,n):
		#new obstruction with k random vertices
		obs = Obstruction(k, compute_optimal_velocity)
	
		#add obstruction to list of obstructions
		obslist.append(obs)

	#plot obstructions in rviz: publish marker
	i = 1
	for ob in obslist:
		markers = ob.plotObs(pub0, i)
		markerpub.publish(markers)	
		i = i + 1
	#then compute costs
	
	#define goal state
	xg = State2D()
	xg.x = 0
	xg.y = 0
	xg.vx = -0.5
	xg.vy = -0.5

	#compute costs and optimal velocity vectors at each vertex	
	for ob in obslist:
		for vert in ob.vertlist:
			resp = compute_optimal_velocity(xg, 
			ob.computeCosts(xg)
			ob.plotVel(ax)
	
	#determine which vertices should be subgoals	
	#if the velocity vector does not point to the interior of the obstruction, or
	#the negative of the velocity vector does not point to the interior of the obstruction,
	#then it is a valid subgoal (if the trajectory doesn't intersect any other subgoals

	#for each obstruction

	allsg = []
	for ob in obslist:
		#for each vertex, determine the angle that the edges make
		ob.computeAngles()
		allsg = allsg + ob.findSubgoals()
		ob.plotSubgoals(ax)

	#illustrate partition boundaries by checking a range of start states
	xran = linspace(0.0, 9.0, num=15)
	yran = linspace(0.0, 9.0, num=15)
	ob = obslist[0]
	for x in xran:
		print(x)
		for y in yran:			
			xpt = Vertex( array( [x, y] ) )
			if xpt.isinWay(ob, S, G, xgs):
				#print("is in the way")
				ax.plot(xpt.pos[0], xpt.pos[1], xpt.color)

	plt.show()
			
