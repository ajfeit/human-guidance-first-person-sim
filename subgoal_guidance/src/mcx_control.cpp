#include "ros/ros.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include <deque>

#include <opencv2/core/core.hpp>
#include <tf/tf.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float32.h>
#include "controllers.h"
#include <subgoal_guidance/State2D.h>

#define  MM2M    0.001   //converts milimiters to meters.
#define  RAD2DEG 180/3.1415926536  //converts radians to degrees
#define  DEG2RAD 3.1415926536/180  //converts degrees to radians
#define  GRAVITY_MS2 9.80665

#define CONTROL_SAMPLING_TIME 0.01
#define K_P_to_V 0.8
#define TIME_AHEAD 0.2
#define ALT_REF_MODE_0 0.50
#define ALT_REF_MODE_1 0.50
#define ALT_REF_MODE_2 0.18
#define VELOCITY_LIMIT 1.5
#define LANDING_RAMP 0.01

#define XBOX 1
#define REALFLIGHT 2

#define MCX 1
#define CX 2
#define ARDRONE 3
#define LOG_ON 1

#define CONF_THRESH_MODE_2 0.85  //confidence for safe landing

//inline float max(float a, float b) { return a > b ? a : b; }
//inline float min(float a, float b) { return a < b ? a : b; }

//sensor_msgs::Joy control_outputs_msg;
//ros::Publisher control_out_pub;
ros::Publisher cmd_pub;

int control_type = REALFLIGHT;
int rotorcraft_type = MCX;

bool heli_take_off;
bool heli_land;
bool heli_land_vs_fly;
bool heli_switch_camera;
bool auto_vs_manual;
bool manual_control;
bool auto_vs_manual_val_prev;

btQuaternion Q_H2G;
btQuaternion Q_T2G;
double x_ref = 0.0;
double y_ref = 0.0;
double z_ref = 1.0;
double ulon_gain = 0.1;
double ulat_gain = 0.1;
double ucol_gain = 0.1;
double control_limit = 0.2;

AttitudeTrim        att_trim    =  {3.5,1,0};//{ 3.5, 2, 0 };// { 5, 4.5, 0 };// phi, the, psi  // for other white heli { -3, 0 , 0 }
ServoTrim           servo_trim  =  {0, 0, 0 , 0}; // lon, lat, thro, tail
ServoDeflection     servo_def   = { 0, 0, 0, 0 };

double x, y, z, the, phi, psi, u, v, w;
double x_target, y_target, z_target, the_target, phi_target, psi_target, u_target, v_target, w_target;
double joy_lat, joy_lon, joy_col, joy_ped;

double x_target_vicon, y_target_vicon, z_target_vicon;
double vx_target_vicon, vy_target_vicon, vz_target_vicon;
double psi_target_vicon, the_target_vicon, phi_target_vicon;
double x_target_est, y_target_est, z_target_est;
double vx_target_est, vy_target_est, vz_target_est;

double ulon, ulat, uped, ucol, ucol_prev;

int masterFlag;
int localmasterFlag;

double u_ref, v_ref, w_ref, alt_ref, psi_ref, psiDot_ref, vertVel_ref, x_err, y_err, z_err;

double poseTime = 0;
double poseTime_prev = 0;
double x_prev, y_prev, z_prev;

double poseTime_target = 0;
double poseTime_prev_target = 0;
double x_prev_target, y_prev_target, z_prev_target;
double vx_target, vy_target, vz_target;
double ax_target, ay_target, az_target;

cv::Mat xc; // lat/long controller state vector
cv::Mat Ac; // controller system matrix
cv::Mat Bc; // input matrix
cv::Mat Cc; // output matrix
cv::Mat Dc; // passthrough matrix

cv::Mat xvc; // vertical controller state vector
cv::Mat Avc; // controller system matrix
cv::Mat Bvc; // input matrix
cv::Mat Cvc; // output matrix
cv::Mat Dvc; // passthrough matrix
double vert_trim, w_cmd;

//PID controllers
pid         yawPID;

PIDContrGains        pidContrGains;

using namespace std;

//covariance estimation data
cv::Mat xerr, ctemp, Ck, muk;
cv::Mat* C;
cv::Mat* mu;
int cpos = 0;
int clen = 50;
int start_est = 0;
float confidence;

int data_logged = 0;  //flag so that data is logged once per landing

void joystickCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	//ROS_INFO("Joystick callback");
	/*
	// XBOX
	heli_take_off = msg->buttons[0];
	heli_land = msg->buttons[1];
	heli_switch_camera = msg->buttons[2];
	auto_vs_manual = msg->buttons[3];

	if ((auto_vs_manual == true) && (auto_vs_manual_val_prev == false))
	{
		if (manual_control == true)
		{
			manual_control = false;
		}
		else
		{
			manual_control = true;
		}
	}

	auto_vs_manual_val_prev = auto_vs_manual;

	const float maxHorizontalSpeed = 1; // use 0.1f for testin		if (masterFlag == 0)

	joy_lon  = max(min(msg->axes[4], maxHorizontalSpeed), -maxHorizontalSpeed);
	joy_lat  = max(min(msg->axes[3], maxHorizontalSpeed), -maxHorizontalSpeed);
	joy_col  = max(min(msg->axes[1], 1), -1);
	joy_ped  = max(min(msg->axes[0], 1), -1);
	*/

	// RealFlight
	const float maxHorizontalSpeed = 1; // use 0.1f for testing and 1 for the real thing
	joy_lon  = max(min(msg->axes[1], maxHorizontalSpeed), -maxHorizontalSpeed);
	joy_lat  = max(min(msg->axes[0], maxHorizontalSpeed), -maxHorizontalSpeed);
	joy_col  = max(min(msg->axes[2]/0.685, 1.0), -1.0);
	joy_ped = max(min(msg->axes[4]/1.0, 1.0), -1.0);

	auto_vs_manual = msg->buttons[0];
	if (auto_vs_manual == true)
	{
		manual_control = false;
	}
	else
	{
		manual_control = true;
	}

	heli_land_vs_fly = msg->buttons[1];
	if (heli_land_vs_fly == true)
	{
		heli_land = true;
	}
	else
	{
		heli_land = false;
	}
}

void statecmdCallback(const subgoal_guidance::State2D::ConstPtr& msg)
{
	x_target = msg->x;
	y_target = msg->y;
	z_target = 0.75;
	vx_target = msg->vx;
	vy_target = msg->vy;
	vz_target = 0.0;
	ax_target = msg->ax;
	ay_target = msg->ay;
}

void viconMcxCallback(const geometry_msgs::TransformStamped::ConstPtr& msg)
{
	double dt, dx, dy, dz, vx, vy, vz;

	x = msg->transform.translation.x;
	y = msg->transform.translation.y;
	z = msg->transform.translation.z;

	if ((fabs(x) < 0.01) && (fabs(y) < 0.01) && (fabs(z) < 0.01))
	{
		ROS_INFO("Vicon Error");
		x = x_prev;
		y = y_prev;
		z = z_prev;
	}
	else
	{

		//ROS_INFO("Vicon x = %.2f, y = %.2f, z = %.2f", x, y, z);

		poseTime = 0.000000001 * (double) msg->header.stamp.nsec;
		if (poseTime_prev == 0)
		{
			dt = 0.01;
			dx = 0;
			dy = 0;
			dz = 0;
		}
		else
		{
			dt = poseTime - poseTime_prev;
			dx = x-x_prev;
			dy = y-y_prev;
			dz = z-z_prev;
		}

		poseTime_prev = poseTime;
		x_prev = x;
		y_prev = y;
		z_prev = z;

		vx = dx/dt;
		vy = dy/dt;
		vz = dz/dt;
		//ROS_INFO("Vicon vx = %.2f, vy = %.2f, vz = %.2f", vx, vy, vz);

		Q_H2G.setX(msg->transform.rotation.x);
		Q_H2G.setY(msg->transform.rotation.y);
		Q_H2G.setZ(msg->transform.rotation.z);
		Q_H2G.setW(msg->transform.rotation.w);

		btVector3 v_G(vx, vy, vz);
		btTransform T_H2G;
		btVector3 trans_vec(0.0, 0.0, 0.0);
		T_H2G.setOrigin(trans_vec);
		T_H2G.setRotation(Q_H2G);
		btVector3 v_H = T_H2G.inverse() * v_G;
		u = v_H.x();
		v = v_H.y();
		w = v_H.z();
		//ROS_INFO("u = %0.2f, v = %0.2f, w = %0.2f", u, v, w);

		tf::Quaternion q;
		double roll, pitch, yaw;
		tf::quaternionMsgToTF(msg->transform.rotation, q);
		tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
		phi = roll * RAD2DEG;
		the = pitch * RAD2DEG;
		psi = yaw * RAD2DEG;
	}

	//ROS_INFO("Vicon qx = %f, qy = %f, qz = %f, qw = %f", msg->orientation.x, msg->orientation.y, msg->orientation.z, msg->orientation.w);

	//double yaw = tf::getYaw(msg->orientation);
	//Q_G2H = tf::createQuaternionFromYaw(yaw);
	//ROS_INFO("yaw = %f", yaw*RAD2DEG);

	//double roll, pitch, yaw;
	//btMatrix3x3(q).getRPY(roll, pitch, yaw);
	//phi = roll;
	//the = pitch;
	//psi = yaw;
}

void resetControllers ()
{
    // reset the integrators in the PID controller
    yawPID.reset();

    //initialize state of lqg controller to zeros
    xc = cv::Mat::zeros(Ac.cols, 1, CV_64F);
    //std::cout << "xc reset = " << xc << "\n";

    //initialize state of vertical controller
    xvc = cv::Mat::zeros(Avc.cols, 1, CV_64F);
    //define vertical trim as last manual input before switching to controller mode
    vert_trim = ucol;

}

/* initialize the controllers */
void InitControllers()
{
	if (rotorcraft_type == MCX)
	{

		//initialize the PID gains, don't need to lock at this point
		pidContrGains.yaw.kP = 0.005; //0.0025 //0.006
		pidContrGains.yaw.kD = 0.0; //0
		pidContrGains.yaw.kI = 0.000; //0.005
		pidContrGains.yaw.tau_filter = 0.1;

		//Define lqg long/lat controller discrete system matrix values
		//float dt = 0.01;
		Ac = (cv::Mat_<double>(4,4) << 0.8437,0,0,0, 0.009196,1,0,0, 0,0,0.8437,0, 0,0,0.009196,1);
		Bc = (cv::Mat_<double>(4,2) << 0.07357,0, 0.0003783,0, 0,0.03678, 0,0.0001891 );
		Cc = (cv::Mat_<double>(2,4) << 1.487,2.529,0,0, 0,0,0.8925,3.124 );
		Dc = (cv::Mat_<double>(2,2) << 0,0, 0,0);
	    	xc = cv::Mat::zeros(4, 1, CV_64F);
	    	std::cout << "xc init = " << xc << "\n";

		//Define vertical controller system matrix values
		//converted from continuous to discrete using c2d in Matlab 
		//with dt = 0.01
		Avc = (cv::Mat_<double>(2,2) << 0.7787, 0, 0.008848, 1 );
		Bvc = (cv::Mat_<double>(2,1) << 0.07078, 0.0003687 );
		Cvc = (cv::Mat_<double>(1,2) << 2.5, 4.25 );
		Dvc = (cv::Mat_<double>(1,1) << 0 );

	}
	
}

cv::Mat runLongLatController(cv::Mat lqg_input)
{
	xc = Ac*xc + Bc*lqg_input;
	//std::cout << "xc = " << xc << "\n";

	return (Cc * xc + Dc * lqg_input);
}

cv::Mat runVertController(cv::Mat w_err)
{
	xvc = Avc*xvc + Bvc*w_err;
	return (Cvc*xvc + Dvc*w_err);
}


int main(int argc, char **argv)
{
	//initialize control error statistics matrix arrays
	C = new cv::Mat[clen];
	mu = new cv::Mat[clen];

	//Initialize ROS
	ros::init(argc, argv, "pid_control_est_train");

	ROS_INFO("Initialize ROS");
	ros::NodeHandle n;

	//if data logging is on, open data file
	ROS_INFO("Opening Landing Log File...");
	ofstream logfile;
	if (LOG_ON == 1)
	{
		logfile.open("/home/user/landing_error.txt", ios::out | ios::app);
		if (logfile.is_open())
		{
			logfile << endl;
			logfile << "Starting ROS " << ros::Time::now() << endl;
			logfile << "time \t x_error \t y_error \t platform_heading \t confidence" << endl;
			ROS_INFO("File Opened");
		}
		else
		{
			ROS_INFO("Error opening landing log file");
		}
	}

	std::string igcl_control_type;
	n.param<std::string>("/igcl/joystick_type", igcl_control_type, "unknown");
	if (igcl_control_type == "realflight")
	{
		control_type = REALFLIGHT;
		ROS_INFO("joystick: realflight");
	}
	else
	{
		ROS_INFO("joystick: unknown");
	}

	ros::Duration(0.5).sleep();

	std::string igcl_rotorcraft_type;
	n.param<std::string>("/igcl/rotorcraft_type", igcl_rotorcraft_type, "unknown");
	if (igcl_rotorcraft_type == "mcx")
	{
		rotorcraft_type = MCX;
		ROS_INFO("rotorcraft: mcx");
	}
	else
	{
		ROS_INFO("rotorcraft: unknown");
	}

	//Initialize joystick inputs
	ROS_INFO("initialize joystick inputs");
	heli_take_off = false;
	heli_land = false;
	heli_switch_camera = false;
	auto_vs_manual = false;
	manual_control = true;
	auto_vs_manual_val_prev = false;
	joy_lon  = 0;
	joy_lat  = 0;
	joy_col  = -1;
	joy_ped = 0;
	int heli_land_mode = 0;
	alt_ref = ALT_REF_MODE_0;

	localmasterFlag = 1;

	//Initialize controllers
	InitControllers();

	//initialize the controller gains
	yawPID.init(pidContrGains.yaw.kP, pidContrGains.yaw.kD, pidContrGains.yaw.kI, CONTROL_SAMPLING_TIME, pidContrGains.yaw.tau_filter);

	ros::Subscriber joystick_sub = n.subscribe("/joy", 1, joystickCallback);
	ros::Subscriber vicon_mcx_sub = n.subscribe("/vicon/mcx/mcx", 1, viconMcxCallback);
	
	//subscribe to estimated platform position, velocity, and accelerations:
	ros::Subscriber state_cmd = n.subscribe("/subgoal/state", 1, statecmdCallback);
	

	//control_out_pub = n.advertise<sensor_msgs::Joy>("/control_output", 1);
	//cmd_pub = n.advertise<geometry_msgs::Twist>("/spektrum/cmd", 1);
	if ((rotorcraft_type == MCX) || (rotorcraft_type == CX))
	{
		cmd_pub = n.advertise<geometry_msgs::Twist>("/spektrum/cmd", 1);
	}
	else if (rotorcraft_type == ARDRONE)
	{
		cmd_pub = n.advertise<geometry_msgs::Twist>("/ardrone/cmd_vel", 1);
	}

	ros::AsyncSpinner spinner(2); // Use 2 threads
	spinner.start();

	ros::Rate loop_rate(1.0/CONTROL_SAMPLING_TIME);

	ros::Time heli_approach_start_time = ros::Time::now();

	ROS_INFO("Start main loop: MCX Control for Subgoal Guidance");
	while (ros::ok() && localmasterFlag )
	{
		if (manual_control == true) // manual mode
		{
			ulat =  joy_lat;
			ulon =  joy_lon;
			ucol =  joy_col;
			uped =  joy_ped;

			//ROS_INFO("Man ulat = %f, ulon = %f, ucol = %f, uped = %f", ulat, ulon, ucol, uped);

			data_logged = 0; //reset data log flag for next time we go to controller mode

			resetControllers();
		}
		else // controller mode
		{
			//use manual control for default
			ulat =  joy_lat;
			ulon =  joy_lon;
			ucol =  joy_col;
			uped =  joy_ped;
			
			w_ref = 0;
			alt_ref = 0.75;
			psi_target = atan2(vy_target, vx_target)*(180/3.14159);

			// Find positional error
			btVector3 P_G_heli(x, y, z);
			//ROS_INFO("P_G_Heli (%f, %f, %f)", x, y, z);
			//btVector3 P_G_desired(x_target+TIME_AHEAD*vx_target, y_target+TIME_AHEAD*vy_target, alt_ref);
			btVector3 P_G_desired(x_target, y_target, alt_ref);
			//ROS_INFO("Target (%f, %f, %f)", P_G_desired.x(), P_G_desired.y(), P_G_desired.z());
			btVector3 P_G_error = P_G_desired - P_G_heli;
			//ROS_INFO("P_G_Error (%f, %f, %f)", P_G_error.x(), P_G_error.y(), P_G_error.z());
			//ROS_INFO("heli (%.2f, %.2f, %.2f), target (%.2f, %.2f, %.2f)", x, y, z, x_target, y_target, z_target);
			btTransform T_H2G;
			btVector3 trans_vec(0.0, 0.0, 0.0);
			T_H2G.setOrigin(trans_vec);
			T_H2G.setRotation(Q_H2G);
			btVector3 P_H_error = T_H2G.inverse() * P_G_error;
			//ROS_INFO("P_H_error (%f, %f, %f)", P_H_error.x(), P_H_error.y(), P_H_error.z());

			//btTransform T_T2G;
			//T_T2G.setOrigin(trans_vec);
			//T_T2G.setRotation(Q_T2G);
			//btVector3 P_T_error = T_T2G.inverse() * P_G_error;

			// Find desired velocity
			btVector3 V_G_target(vx_target, vy_target, vz_target);
			btVector3 V_H_target = T_H2G.inverse() * V_G_target;
			//ROS_INFO("Velocity G (%.2f, %.2f, %.2f), Velocity H (%.2f, %.2f, %.2f)", V_G_target.x(), V_G_target.y(), V_G_target.z(), V_H_target.x(), V_H_target.y(), V_H_target.z());
			btVector3 V_H_desired =  V_H_target + P_H_error * K_P_to_V;
			//ROS_INFO("Velocity Targ (%.2f, %.2f, %.2f), Act (%.2f, %.2f, %.2f)", V_H_desired.x(), V_H_desired.y(), V_H_desired.z(), u, v, w);


			//heading control
			double psi_error = psi_target - psi;
			if (psi_error > 180.0)
				psi_error = psi_error - 360.0;
			if (psi_error < -180.0)
				psi_error = psi_error + 360.0;
			uped = yawPID.process(psi_error, 0);

			u_ref = min(max(V_H_desired.x(), -VELOCITY_LIMIT), VELOCITY_LIMIT);
			v_ref = min(max(V_H_desired.y(), -VELOCITY_LIMIT), VELOCITY_LIMIT);

			//u_ref = joy_lon;
			//v_ref = joy_lat;


			cv::Mat lqg_input = (cv::Mat_<double>(2,1) << v_ref - v, u_ref - u );
			//ROS_INFO("u_ref = %f, v_ref = %f, u = %f, v = %f", u_ref, v_ref, u, v);

			//std::cout << "lqg_in = " << lqg_input << "\n";
			cv::Mat lqg_output = runLongLatController(lqg_input);
			//std::cout << "lqg_out = " << lqg_output << "\n";

			ulat = lqg_output.at<double>(0,0);
			ulon = lqg_output.at<double>(0,1);
			//ROS_INFO("ulat: %f, ulon: %f", ulat, ulon);

			//vertical control
			
			w_cmd = max(min(((alt_ref - z)*1.0 + w_ref), 0.75), -0.75);
			double w_e = w_cmd - w;
			cv::Mat w_err = (cv::Mat_<double>(1,1) << w_e );
			cv::Mat vert_control = runVertController(w_err);
			if (heli_land_mode == 4)
			{
				ucol = -1;
			}
			else
			{
				
				ucol = vert_control.at<double>(0,0);
			} 

			ulon = min(max(ulon, -1.0), 1.0);
			ulat = min(max(ulat, -1.0), 1.0);
			ucol = min(max(ucol, -1.0), 1.0);
			uped = min(max(uped, -1.0), 1.0);

					
		}

		geometry_msgs::Twist cmd_msg;
		cmd_msg.linear.x  = ulon;
		cmd_msg.linear.y  = ulat;
		cmd_msg.linear.z  = ucol;
		cmd_msg.angular.x = 0;
		cmd_msg.angular.y = 0;
		cmd_msg.angular.z = uped;
		cmd_pub.publish(cmd_msg);

		//sleep until next control update
		loop_rate.sleep();
	}


	//close log file
	if (LOG_ON == 1)
	{
		logfile.close();
	}

	// publish the controller_info_msg:
	ROS_INFO("Finish");

	return 0;
}
