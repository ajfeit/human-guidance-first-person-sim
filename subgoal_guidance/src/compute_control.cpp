#include "ros/ros.h"
//#include "geometry_msgs/Pose.h"
//#include "geometry_msgs/Twist.h"
//#include "geometry_msgs/Wrench.h"
//#include "geometry_msgs/Vector3.h"
#include "std_msgs/Float32.h"
#include "subgoal_guidance/State2D.h"
#include "subgoal_guidance/Control2D.h"
#include "subgoal_guidance/ControlSequence.h"
#include "subgoal_guidance/ComputeControlSequence.h"
#include "opencv/cv.h"
#include <cmath>


cv::Mat A, B;
cv::Mat x0, xg, RR, u, temp, dx, xf, Jmat;
double dt, t;

void initModel()
{
  dt = 0.01;
  
  A = (cv::Mat_<double>(4, 4, CV_64F) << 0,1,0,0, 0,0,0,0, 0,0,0,1, 0,0,0,0 );
  A = cv::Mat::eye(4,4, CV_64F) + dt*A;

  B = (cv::Mat_<double>(4,2, CV_64F) << 0,0, 1,0, 0,0, 0,1 );
  B = B*dt;

  x0 = cv::Mat::zeros(4,1, CV_64F);
  xg = cv::Mat::zeros(4,1, CV_64F);
  xf = cv::Mat::zeros(4,1, CV_64F);

}

bool computeControl(subgoal_guidance::ComputeControlSequence::Request  &req,
         subgoal_guidance::ComputeControlSequence::Response &res)
{
	//load service request data
	ROS_INFO("starting");
	cv::Mat R;
	x0 = (cv::Mat_<double>(4,1, CV_64F) << req.xstart.x, req.xstart.vx, req.xstart.y, req.xstart.vy);
	xg = (cv::Mat_<double>(4,1, CV_64F) << req.xgoal.x, req.xgoal.vx, req.xgoal.y, req.xgoal.vy); 
	dx = xg - x0;	
	A.copyTo(temp);
	hconcat(temp*B, B, R);
	int k = 2;
	double J, Jlast, dJ;
	//xf = [dx(1) - x(2)*n*dt; dx(2); dx(3) - x(4)*n*dt; dx(4)];
	//xf = dx - temp*x0; 
	ROS_INFO("A");
        xf.at<double>(0,0) = dx.at<double>(0,0) - x0.at<double>(1,0)*k*dt;
	xf.at<double>(1,0) = dx.at<double>(1,0);
	xf.at<double>(2,0) = dx.at<double>(2,0) - x0.at<double>(3,0)*k*dt;
	xf.at<double>(3,0) = dx.at<double>(3,0);
	ROS_INFO("B");
	Jmat = xf.t() * (R*R.t()).inv() * xf;
	J = Jmat.at<double>(0,0) + 10.0*((double) k)*dt;
	ROS_INFO("entering first loop, J = %f, R(1,1) = %f", J, R.at<double>(0,0));
	do
	{
		temp = A*temp;
		hconcat(temp*B, R, R);
		//xf = dx - temp*x0;
		xf.at<double>(0,0) = dx.at<double>(0,0) - x0.at<double>(1,0)*k*dt;
		xf.at<double>(1,0) = dx.at<double>(1,0);
		xf.at<double>(2,0) = dx.at<double>(2,0) - x0.at<double>(3,0)*k*dt;
		xf.at<double>(3,0) = dx.at<double>(3,0);

		Jlast = J;
		//ROS_INFO("R: height %i, width, %i ", R.size().height, R.size().width);
		Jmat = xf.t() * (R*R.t()).inv() * xf;
		//ROS_INFO("R (1,1): %f", R.at<double>(0,0));
		J = Jmat.at<double>(0,0) + 5.0*((double) k)*dt;
		dJ = std::abs(J - Jlast);
		//ROS_INFO("looped %i times, J, = %f, dJ = %f", k, J, dJ);
		k++;
	} while (dJ > 0.01);
	u = R.t()*((R*R.t()).inv())*xf;
	ROS_INFO("looped %i times, J, = %f, dJ = %f", k, J, dJ);
	ROS_INFO("finished first loop, J = %f, t = %f", J, k*dt);
	res.topt = k;
	res.jopt = J;
	//geometry_msgs::Vector3 ut;
	//std::vector<geometry_msgs::Vector3> utemp[k];
	subgoal_guidance::Control2D ut;
	ut.x = u.at<double>(0,0);
	ut.y = u.at<double>(1,0);
	//ROS_INFO("A");
	subgoal_guidance::ControlSequence utseq;
	utseq.controls.push_back(ut);

	//ROS_INFO("C");
	//res.uopt[0].x = u.at<double>(0,0);
	ROS_INFO("entering second loop, k = %i", k);
	ROS_INFO("size of u: %i, %i", u.size().height, u.size().width);
	for(int j = 1;j<=k;j++)
	{
		//ROS_INFO("second loop, %i of %i", j, k);
		//res.uopt[i-1].x = u.at<double>(i-1,0);
		//res.uopt[i-1].y = u.at<double>(i-1,1);
		ut.x = u.at<double>(2*j-2,0);
		ut.y = u.at<double>(2*j-1,0);
		utseq.controls.push_back(ut);
	}
	res.uopt = utseq;
	R.release();
	ROS_INFO("Complete");
	return true;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "compute_control_server");
  ros::NodeHandle n;
  initModel();

  ros::ServiceServer service = n.advertiseService("compute_control", computeControl);
  ROS_INFO("Ready to compute control sequences.");
  ros::spin();

  return 0;
}
