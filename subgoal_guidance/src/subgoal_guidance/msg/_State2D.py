"""autogenerated by genpy from subgoal_guidance/State2D.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct


class State2D(genpy.Message):
  _md5sum = "b9cb6d9e7fd0fea0b91cd24f5a98538f"
  _type = "subgoal_guidance/State2D"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """string agent
float64 x
float64 y
float64 vx
float64 vy
float64 ax
float64 ay

"""
  __slots__ = ['agent','x','y','vx','vy','ax','ay']
  _slot_types = ['string','float64','float64','float64','float64','float64','float64']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       agent,x,y,vx,vy,ax,ay

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(State2D, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.agent is None:
        self.agent = ''
      if self.x is None:
        self.x = 0.
      if self.y is None:
        self.y = 0.
      if self.vx is None:
        self.vx = 0.
      if self.vy is None:
        self.vy = 0.
      if self.ax is None:
        self.ax = 0.
      if self.ay is None:
        self.ay = 0.
    else:
      self.agent = ''
      self.x = 0.
      self.y = 0.
      self.vx = 0.
      self.vy = 0.
      self.ax = 0.
      self.ay = 0.

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self.agent
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_6d.pack(_x.x, _x.y, _x.vx, _x.vy, _x.ax, _x.ay))
    except struct.error as se: self._check_types(se)
    except TypeError as te: self._check_types(te)

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.agent = str[start:end].decode('utf-8')
      else:
        self.agent = str[start:end]
      _x = self
      start = end
      end += 48
      (_x.x, _x.y, _x.vx, _x.vy, _x.ax, _x.ay,) = _struct_6d.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self.agent
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_6d.pack(_x.x, _x.y, _x.vx, _x.vy, _x.ax, _x.ay))
    except struct.error as se: self._check_types(se)
    except TypeError as te: self._check_types(te)

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.agent = str[start:end].decode('utf-8')
      else:
        self.agent = str[start:end]
      _x = self
      start = end
      end += 48
      (_x.x, _x.y, _x.vx, _x.vy, _x.ax, _x.ay,) = _struct_6d.unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_6d = struct.Struct("<6d")
