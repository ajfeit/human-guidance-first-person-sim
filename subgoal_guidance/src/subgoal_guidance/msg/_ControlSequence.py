"""autogenerated by genpy from subgoal_guidance/ControlSequence.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import subgoal_guidance.msg

class ControlSequence(genpy.Message):
  _md5sum = "2aace39510956fa7c9170b969f4cf1b5"
  _type = "subgoal_guidance/ControlSequence"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """Control2D[] controls

================================================================================
MSG: subgoal_guidance/Control2D
float32 x
float32 y

"""
  __slots__ = ['controls']
  _slot_types = ['subgoal_guidance/Control2D[]']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       controls

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(ControlSequence, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.controls is None:
        self.controls = []
    else:
      self.controls = []

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      length = len(self.controls)
      buff.write(_struct_I.pack(length))
      for val1 in self.controls:
        _x = val1
        buff.write(_struct_2f.pack(_x.x, _x.y))
    except struct.error as se: self._check_types(se)
    except TypeError as te: self._check_types(te)

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.controls is None:
        self.controls = None
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.controls = []
      for i in range(0, length):
        val1 = subgoal_guidance.msg.Control2D()
        _x = val1
        start = end
        end += 8
        (_x.x, _x.y,) = _struct_2f.unpack(str[start:end])
        self.controls.append(val1)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      length = len(self.controls)
      buff.write(_struct_I.pack(length))
      for val1 in self.controls:
        _x = val1
        buff.write(_struct_2f.pack(_x.x, _x.y))
    except struct.error as se: self._check_types(se)
    except TypeError as te: self._check_types(te)

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.controls is None:
        self.controls = None
      end = 0
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.controls = []
      for i in range(0, length):
        val1 = subgoal_guidance.msg.Control2D()
        _x = val1
        start = end
        end += 8
        (_x.x, _x.y,) = _struct_2f.unpack(str[start:end])
        self.controls.append(val1)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_2f = struct.Struct("<2f")
