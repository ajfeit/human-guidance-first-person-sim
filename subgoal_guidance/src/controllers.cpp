/* controllers.cpp
 *
 * Controller classes and method definitions
 *
 * History:
 * -------------------------------------------------------------------------
 * 		Date	Developer	Modification
 * -------------------------------------------------------------------------
 *
 */

#include "controllers.h"


#define fill( m, a11, a12, a21, a22 )	\
	 {								\
		m[0][0]  = a11;				\
		m[0][1]  = a12;				\
		m[1][0] = a21;				\
		m[1][1] = a22;				\
	} // fills a 2 by 2 matrix


/* $Id: PID.cpp,v 2.0 2002/09/22 02:07:30 tramm Exp $
 *
 * (c) Aaron Kahn
 * (c) Trammell Hudson
 *
 * PID control logic for a SISO controller.
 *
 *
 * This is a general PID control law function.  The output is the
 * result of the controller.
 *
 *  command[0] ---->O--> LIMITER --> Kp ---------------+
 *                - |              |                   |
 *  feedback[0] ----+             INTEGRATE ---> Ki ---+---> OUTPUT
 *                                                     |
 *  command[1] ---->O--> LIMITER --> Kd ---------------+
 *                - |
 *  feedback[1] ----+
 *
 *************
 *
 *  The PID class is part of the autopilot simulation package.
 *
 *  Autopilot is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Autopilot is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Autopilot; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA1
 */

double PID::step()
{
    PID *pid = this;

    double err = LIMIT(  pid->commend_values[0] - pid->feedback_values[0],   pid->ProErrorLimits[0],    pid->ProErrorLimits[1] );

    double velerr = LIMIT( pid->commend_values[1] - pid->feedback_values[1], pid->VelErrorLimits[0],  pid->VelErrorLimits[1] );

    double result =	0.0  + pid->Kp*err  + pid->Kd*velerr   + pid->Ki * pid->int_state;

    pid->int_state = LIMIT(  pid->int_state + err*pid->int_dt,  pid->IntStateLimits[0],   pid->IntStateLimits[1]  );

    return LIMIT(  result,    pid->OutErrorLimits[0],     pid->OutErrorLimits[1] );
}

NotchFilter::NotchFilter(double wn, double zita, double Ts)
{
    reset();

    /*  notch filter in continous time:
     *  TF = (s^2+2*wn*zeita*s+wn^2)/(s^2+2*wn*s+wn^2)
     *  the following is the coeficient of notch filter in discrete domain using TUSTIN transformation
     *  Ts is the sampling time
     */

    // notch filter numerator
    this->num[0] = 4 + 4*zita*wn*Ts + wn*wn*Ts*Ts ;
    this->num[1] = -8 + 2*wn*wn*Ts*Ts ;
    this->num[2] =  4 -4*zita*wn*Ts + wn*wn*Ts*Ts;

    // notch filter denominator
    this->den[0] =  4 + 4*wn*Ts  + wn*wn*Ts*Ts ;
    this->den[1] = 2*wn*wn*Ts*Ts-8 ;
    this->den[2] = 4-4*wn*Ts + wn*wn*Ts*Ts;
}

double NotchFilter::process(double in)
{
    double out;

    out = 1/den[0]*( -den[1]*out_old1 - den[2]*out_old2 + num[0]*in + num[1]*in_old1 + num[2]*in_old2 ) ;

    in_old2 = in_old1 ;
    in_old1 = in ;
    out_old2 = out_old1 ;
    out_old1 = out;

    return out;
}

void NotchFilter::reset()
{
    in_old1 = 0;
    in_old2 = 0;
    out_old1 = 0;
    out_old2 = 0;
}

double Integrator::apply(double in)
{
	// tustin approximatation for digital integrator:
	double out;
	out = out_old + Ts/2*(in + in_old);

	out_old = out;
	in_old  = in;
	return out;
}

void Integrator::reset()
{
	in_old = 0;
	out_old = 0;
}


void LeadLag::reset()
{
	in_old = 0;
	out_old = 0;
}
double LeadLag::apply( double in)
{
	/*double bb = this->b;
	double aa = this->a;
	double dt = this->Ts;*/

	double out;


	out = (-(Ts-2*b)*out_old + (2*a+Ts)*in + (Ts-2*a)*in_old )/(2*b+Ts);


	out_old = out;
	in_old  = in;
	return out;
}

void FisrtOrderFilter::reset()
{
	out_old = 0;
	in_old = 0;
}

double FisrtOrderFilter::apply(double in)
{
	double out;
	out = out_old + Ts/tau*(-out_old + in_old);

	out_old = out;
	in_old  = in;
	return out;
}

SecondOrderFilter::SecondOrderFilter(double wn, double zita, double Ts)
{
    reset();

    /*  second order filter in continous time:
     *  TF = (wn^2)/(s^2+2*wn*zita*s+wn^2)
     *  the following is the coeficient of notch filter in discrete domain using TUSTIN transformation
     *  Ts is the sampling time
     */

    // second order filter numerator
     this->num[0] =  wn*wn*Ts*Ts ;
     this->num[1] =  2*wn*wn*Ts*Ts ;
     this->num[2] =  wn*wn*Ts*Ts;

     // second order filter denominator
     this->den[0] =  4 + 4*wn*Ts  + wn*wn*Ts*Ts ;
     this->den[1] = 2*wn*wn*Ts*Ts-8 ;
     this->den[2] = 4-4*wn*Ts + wn*wn*Ts*Ts;
}

void SecondOrderFilter::reset()
{
	in_old1 = 0;
	in_old2 = 0;
	out_old1 = 0;
	out_old2 = 0;
}

double SecondOrderFilter::process(double in)
{
    double out;

    out = 1/den[0]*( -den[1]*out_old1 - den[2]*out_old2 + num[0]*in + num[1]*in_old1 + num[2]*in_old2 ) ;

    in_old2 = in_old1 ;
    in_old1 = in ;
    out_old2 = out_old1 ;
    out_old1 = out;

    return out;
}





double pid::process(double y_ref, double y_meas)
{
	double P, I, D;
	double out, err;

	err = y_ref - y_meas;

	P = Kp* err;
	// D value based on measurement signal derivative:
	//D = D_old * tau_filter/(tau_filter+Ts) - Kd/(tau_filter+Ts)*(y_meas-y_meas_old);

	// D value based on error signal derivative:
	D =  D_old * tau_filter/(tau_filter+Ts)  + Kd/(tau_filter+Ts)*(err- err_old);

	//double err_I = LIMIT(err, -0.5, 0.5);
	//I = I_old + Ki * Ts * err_I;
	I = I_old + Ki * Ts * err;


	out = P + D + I;


	D_old = D;
	y_meas_old = y_meas;
	I_old = I;
	err_old = err;

	return out;
}

void pid::reset()
{
	I_old = 0; // Integrator old value;
	D_old = 0; // Derivative old value;
	y_meas_old = 0; // measurement old value;
	err_old = 0;
}

void pid::bumplessupdate(double I)
{
	I_old = I;
	D_old = 0;
	err_old = 0;
}

double AttitudeLock::process(double attRate_ref, double attRate, double att)
{
	double I, out;

	I = I_old + (attRate_ref * Kstick - attRate * Kgyro) * Ts;

	out = I * Kp - attRate * Kd +  attRate_ref * KstickDirect;
	//out = -att * Kp - attRate * Kd +  attRate_ref * KstickDirect;


	I_old = I;
	printf("out = %.3f\n", out);
	return out;
}
void AttitudeLock::reset()
{
	I_old = 0 ;
}


//----------H-infinity controller class functions
bool LoopShaping_HinfCtrlInnerLoop::initialize( char * HinfInnerLoopFileName , char *PreGainInnerLoopFileName  )
{
         FILE * file_in;
         FILE * file_ingain;
         double temp;
         reset(); // reset the state vector of the controller to zero.

         this->dt = CONTROL_SAMPLING_TIME; // controller sampling time

         // open the proper file for H-inf controller pre gain 2by2 matrix:
         strcpy(this->PreGainInnerLoopFileName,  PreGainInnerFILENAME  );
         file_ingain = fopen(PreGainInnerLoopFileName, "r");
           	if (!file_ingain)
				{
                  printf("Couldn't open the H-infinity inner gain File: \"%s\"\n", PreGainInnerLoopFileName);
                  return false;
                }
         for(int i=0; i<2; i++)
             for(int j=0; j<2; j++)
               {
                  fscanf(file_ingain, "%lf", &temp);
                  this->PreGain_inner[i][j] = temp;
               };

         /*fill( this->PreGain_inner , // the loop-shaping controller Pre-Grain
        		 -0.3656 ,-0.2038,
        		 0.2006 , -0.3565 );*/

         // open the proper file for H-inf controller matrices:
         strcpy(this->HinfInnerLoopFileName,  HINFInnerFILENAME  );
         file_in = fopen(HinfInnerLoopFileName, "r");
         	if (!file_in)
          	{
               printf("Couldn't open the H-infinity inner Controller File: \"%s\"\n", HinfInnerLoopFileName);
               return false;
            }
   	     //read the matrix sizes
         fscanf(file_in, "%d", &nA);
         fscanf(file_in, "%d", &nB);
         fscanf(file_in, "%d", &nC);
         // read the matrices from file:
         for(int i=0; i<nA; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->A_ctrl[i][j] = temp;
           }
         for(int i=0; i<nA; i++)
           for(int j=0; j<nB; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->B_ctrl[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->C_ctrl[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
        	 for(int j=0; j<nB; j++)
        	 {
        		 fscanf(file_in, "%lf", &temp);
        		 this->D_ctrl[i][j] = temp;
        	 }

        fclose(file_in);

        /*for(int i=0;i<20;i++)
        	printf("%0.2f \n  ",this->C_ctrl[0][i]);*/

        return true;

}

void LoopShaping_HinfCtrlInnerLoop::step()
{
 Vector<nStateCtrl_inner> Xdot;

 Xdot = (this->A_ctrl) *(this->state_ctrl) +  (this->B_ctrl) * (this->input_ctrl);
 this->state_ctrl  += Xdot * this->dt ;
 this->output_ctrl = this->C_ctrl * this->state_ctrl + this->D_ctrl * this->input_ctrl ;

};


bool LoopShaping_HinfCtrlOuterLoop::initialize( char * HinfOuterLoopFileName, char *PreGainOuterLoopFileName )
{
         FILE * file_in ;
         FILE * file_outgain;
         double temp;
         reset(); // reset the state vector of the controller to zero.

         this->dt = CONTROL_SAMPLING_TIME; // controller sampling time

         // open the proper file for H-inf controller pre gain 2by2 matrix:
         strcpy(this->PreGainOuterLoopFileName,  PreGainOuterFILENAME  );
         file_outgain = fopen(PreGainOuterLoopFileName, "r");
           	if (!file_outgain)
				{
                  printf("Couldn't open the H-infinity outer gain File: \"%s\"\n", PreGainOuterLoopFileName);
                  return false;
                }
         for(int i=0; i<2; i++)
             for(int j=0; j<2; j++)
               {
                  fscanf(file_outgain, "%lf", &temp);
                  this->PreGain_outer[i][j] = temp;
               };

         /*for(int i=0; i<2; i++)
                     for(int j=0; j<2; j++)
                      printf("%lf ", this->PreGain_outer[i][j] );*/

         /*fill( this->PreGain_outer , // the loop-shaping controller Pre-Grain
        		 -0.3656 ,-0.2038,
        		 0.2006 , -0.3565 );*/

         // open the proper file for H-inf controller matrices:
         strcpy(this->HinfOuterLoopFileName,  HINFOuterFILENAME  );
         file_in = fopen(HinfOuterLoopFileName, "r");
         	if (!file_in)
          	{
               printf("Couldn't open the H-infinity outer Controller File: \"%s\"\n", HinfOuterLoopFileName);
               return false;
            }
   	     //read the matrix sizes
         fscanf(file_in, "%d", &nA);
         fscanf(file_in, "%d", &nB);
         fscanf(file_in, "%d", &nC);
         // read the matrices from file:
         for(int i=0; i<nA; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->A_ctrl[i][j] = temp;
           }
         for(int i=0; i<nA; i++)
           for(int j=0; j<nB; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->B_ctrl[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->C_ctrl[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
        	 for(int j=0; j<nB; j++)
        	 {
        		 fscanf(file_in, "%lf", &temp);
        		 this->D_ctrl[i][j] = temp;
        	 }

        fclose(file_in);

        /*for(int i=0;i<20;i++)
        	printf("%0.2f \n  ",this->A_ctrl[i][0]);*/

        return true;

}

void LoopShaping_HinfCtrlOuterLoop::step()
{
 Vector<nStateCtrl_outer> Xdot;

 Xdot = (this->A_ctrl) *(this->state_ctrl) +  (this->B_ctrl) * (this->input_ctrl);
 this->state_ctrl  += Xdot * this->dt ;
 this->output_ctrl = this->C_ctrl * this->state_ctrl + this->D_ctrl * this->input_ctrl ;

};


//----------------------------------------
bool PreCompensator_InnerLoop::initialize(char * PreCompensatorFileName )
{
         // initialize the matrices needed for H-infinity control design
         //
         FILE * file_in ;
         double temp;
         reset(); // reset the state vector of the controller to zero.

         this->dt = CONTROL_SAMPLING_TIME;

         // open the proper file for H-inf controller matrices:
         strcpy(this->PreCompensatorFileName, PreCompInnerFILENAME );
         file_in = fopen(PreCompensatorFileName, "r");
         	if (!file_in)
          	{
               printf("Couldn't open the pre-compensator File: \"%s\"\n", PreCompensatorFileName);
               return false;
            }
   	     //read the matrix sizes
         fscanf(file_in, "%d", &nA);
         fscanf(file_in, "%d", &nB);
         fscanf(file_in, "%d", &nC);
         // read the matrices from file:
         for(int i=0; i<nA; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->A_filter[i][j] = temp;
           }
         for(int i=0; i<nA; i++)
           for(int j=0; j<nB; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->B_filter[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->C_filter[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
        	 for(int j=0; j<nB; j++)
        	 {
        		 fscanf(file_in, "%lf", &temp);
        		 this->D_filter[i][j] = temp;
        	 }

        fclose(file_in);
        /*for(int i=0;i<6;i++)
              printf("%0.2f \n  ",this->C_filter[0][i]);*/

        return true;
}

void PreCompensator_InnerLoop::step( Vector<2> in)
{
    Vector<nStateW1_inner> Xdot;

    Xdot = (this->A_filter) *(this->state_filter) +  (this->B_filter) * in ;
    this->state_filter  += Xdot * this->dt ;
    this->output_filter = this->C_filter * this->state_filter + this->D_filter * in ;

}



bool PreCompensator_OuterLoop::initialize(char * PreCompensatorFileName )
{
         // initialize the matrices needed for H-infinity control design
         //
         FILE * file_in ;
         double temp;
         reset(); // reset the state vector of the controller to zero.

         this->dt = CONTROL_SAMPLING_TIME;

         // open the proper file for H-inf controller matrices:
         strcpy(this->PreCompensatorFileName, PreCompOuterFILENAME );
         file_in = fopen(PreCompensatorFileName, "r");
         	if (!file_in)
          	{
               printf("Couldn't open the pre-compensator outer File: \"%s\"\n", PreCompensatorFileName);
               return false;
            }
   	     //read the matrix sizes
         fscanf(file_in, "%d", &nA);
         fscanf(file_in, "%d", &nB);
         fscanf(file_in, "%d", &nC);
         // read the matrices from file:
         for(int i=0; i<nA; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->A_filter[i][j] = temp;
           }
         for(int i=0; i<nA; i++)
           for(int j=0; j<nB; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->B_filter[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
           for(int j=0; j<nA; j++)
           {
                fscanf(file_in, "%lf", &temp);
                this->C_filter[i][j] = temp;
           }

         for(int i=0; i<nC; i++)
        	 for(int j=0; j<nB; j++)
        	 {
        		 fscanf(file_in, "%lf", &temp);
        		 this->D_filter[i][j] = temp;
        	}


        fclose(file_in);
        /*for(int i=0;i<6;i++)
              printf("%0.2f \n  ",  figure(30);
        subplot(211), plot(t, dlon_linsim,'b'); ylabel('lon. input %');
        axis([0 t(end) -1 1]); hold on;
        subplot(212), plot(t, dlat_linsim,'b'); ylabel('lat. input %');
        axis([0 t(end) -1 1]);
        xlabel('sec'); hold on;
              this->C_filter[0][i]);*/

        return true;
}

void PreCompensator_OuterLoop::step( Vector<2> in)
{
    Vector<nStateW1_outer> Xdot;

    Xdot = (this->A_filter) *(this->state_filter) +  (this->B_filter) * in ;
    this->state_filter  += Xdot * this->dt ;
    this->output_filter = this->C_filter * this->state_filter + this->D_filter * in;

}

double Desired_cmdRate::step(double meas, double cmd)
{

	double dydt_desired, dydt;
	double dzdt;
	double in, out;

	/*
	 * The first part computes the lowpass derivatve of the command:
	 */
	in = cmd;
	//out = 1/(2*this->tau + this->dt)*( 2*in - 2* this->in_old  - ( -2*this->tau+this->dt)* this->out_old  ); // tustin approximation.
	out = 1/(this->tau + this->dt)*( in - this->in_old  + this->tau * this->out_old  ); // zoh approximation.
	out_old = out ;
	in_old  = in ;

	this->cmd_deriv = out;

	/* Filter Structure:
		*  dy_dt_desired = Kf*dy_dt + Kb*(fc*yc - y + z)
		*  dz_dt = fi*Kb*(yc-y)
		* where y is the measurement and yc is the command
	*/

	dzdt = (this->fi)*(this->Kb)*(cmd - meas) ;
	this->state1_filter += dzdt * this->dt ;
	dydt = this->Kf* this->cmd_deriv + (this->Kb)*( (this->fc)*cmd - meas + this->state1_filter );

	this->dydt_desired_nofilter = dydt;

	this->state2_filter += ( -this->cuttoff * this->state2_filter +  this->cuttoff* dydt ) * this->dt;

	dydt_desired = this->state2_filter;
	return dydt_desired;
}

/* convert the value from [minI, maxI] to [minO, maxO] range */
double normalize(double value, double minI, double maxI, double minO, double maxO)
{
    //convert to [0, 1] range
    value += -minI;
    value /= (maxI-minI);

    //convert to [0, maxO-minO] range
    value *= (maxO-minO);

    //convert to [minO, maxO] range
    value += minO;

    //limit to [minO, maxO] range
    if (value < minO)    value = minO;
    else if(value > maxO) value = maxO;

    return value;
}

