#include "ros/ros.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/Float32.h"
#include "subgoal_guidance/State2D.h"
#include "subgoal_guidance/State2DArray.h"
#include "subgoal_guidance/ComputeControlSequence.h"
#include "subgoal_guidance/GetSubgoals.h"
#include "opencv/cv.h"
#include <visualization_msgs/Marker.h>


class SubgoalSequence {
	cv::Mat A, B;
	cv::Mat x0, xg, RR, u, temp, dx, xf, Jmat, x;
	double dt, t;
	int kopt, numsubgoals;
	float jopt;
	subgoal_guidance::ControlSequence utseq;
	subgoal_guidance::State2DArray sgseq;
	int subgoalnum, subgoalnext;

  public:
	SubgoalSequence (int argc, char **argv)
	{
		ros::init(argc, argv, "compute_control_test_client");

		ros::NodeHandle n;
  
		ros::ServiceClient client0 = n.serviceClient<subgoal_guidance::ComputeControlSequence>("compute_control");
		ros::ServiceClient client1 = n.serviceClient<subgoal_guidance::GetSubgoals>("get_subgoals");
		ros::Publisher pub0 = n.advertise<subgoal_guidance::State2D>("/subgoal/state", 1);
		ros::Publisher pub1 = n.advertise<visualization_msgs::Marker>("/subgoal/state_marker", 1); 

		ROS_INFO("Initializing...");
		initModel();

		ROS_INFO("Getting subgoal sequence");
  		subgoal_guidance::GetSubgoals srv1;
  		srv1.request.filename = "/home/user/ros_igcl_packages/subgoal_guidance/data/subgoal_data.txt";
  		if (client1.call(srv1))
  		{
			sgseq = srv1.response.subgoals;
			numsubgoals = sgseq.states.size();
			ROS_INFO("Service Call Successfull, %i subgoals", sgseq.states.size());
			subgoalnum = 0;
  			subgoalnext = subgoalnum + 1;
			runLoop( pub0, pub1, client0);
  		}
  		else
  		{
  			ROS_ERROR("Failed to call sevice get_subgoals");
  		}
		
	}

	void runLoop(ros::Publisher &pub0, ros::Publisher &pub1, ros::ServiceClient &client0)
	{
	  //run at 100hz
	  ROS_INFO("Initializing Loop");
	  ros::Rate loop_rate(100);
	  int k = 0;
	  cv::Mat usim = cv::Mat::zeros(2,1, CV_64F);
	  subgoal_guidance::Control2D utemp;
	  subgoal_guidance::State2D modelState;
	  visualization_msgs::Marker marker;

	  modelState.x = x.at<double>(0,0);
	  modelState.y = x.at<double>(2,0);
	  modelState.vx = x.at<double>(1,0);
	  modelState.vy = x.at<double>(3,0);

	  initMarker(marker);
	  ROS_INFO("Starting Loop");
	  computeControl(modelState, sgseq.states[subgoalnext], client0);
	  while (ros::ok())
	  {
	    if (k <= utseq.controls.size())
	    {
	      usim.at<double>(0,0) = utseq.controls[k].x;
	      usim.at<double>(1,0) = utseq.controls[k].y;
	      k++;
	    }
	    else
	    {
	      usim = cv::Mat::zeros(2,1, CV_64F);
	      //increment subgoals
	      subgoalnum = subgoalnum + 1;
	      subgoalnext = subgoalnum + 1;
	      if (subgoalnext > numsubgoals - 1) { subgoalnext = subgoalnext - numsubgoals; }
	      if (subgoalnum == numsubgoals) { subgoalnum = 0; }
	      computeControl(modelState, sgseq.states[subgoalnext], client0);
	      k = 0;
	    }
	    runModel(usim);
	    modelState.x = x.at<double>(0,0);
	    modelState.y = x.at<double>(2,0);
	    modelState.vx = x.at<double>(1,0);
	    modelState.vy = x.at<double>(3,0);
	    modelState.ax = usim.at<double>(0,0);
	    modelState.ay = usim.at<double>(1,0);
	    //ROS_INFO("x( %i ) = ( %f, %f )", k, modelState.x, modelState.y);

	    marker.pose.position.x = modelState.x;
	    marker.pose.position.y = modelState.y;

	    pub0.publish(modelState);
	    pub1.publish(marker);

	    ros::spinOnce();
	    loop_rate.sleep();
	    
	  }
	}

	void initModel()
	{
  	  dt = 0.01;
  	  A = (cv::Mat_<double>(4, 4, CV_64F) << 0,1,0,0, 0,0,0,0, 0,0,0,1, 0,0,0,0 );
  	  A = cv::Mat::eye(4,4, CV_64F) + dt*A;
  	  B = (cv::Mat_<double>(4,2, CV_64F) << 0,0, 1,0, 0,0, 0,1 );
  	  B = B*dt;
  	  x0 = (cv::Mat_<double>(4,1, CV_64F) << 0,0.2,0,0.0 );
  	  xg = (cv::Mat_<double>(4,1,CV_64F) << 1,0,1,0.2 );
  	  x0.copyTo(x);
	}
	void runModel( cv::Mat u)
	{
  	  x = A*x + B*u;
	}

	void initMarker( visualization_msgs::Marker &marker )
	{
  	  // Set the frame ID and timestamp.  See the TF tutorials for information on these.
  	  marker.header.frame_id = "/my_frame";
  	  marker.header.stamp = ros::Time::now();

  	  // Set the namespace and id for this marker.  This serves to create a unique ID
  	  // Any marker sent with the same namespace and id will overwrite the old one
  	  marker.ns = "subgoal_guidance";
  	  marker.id = 0;

  	  // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  	  uint32_t cube = visualization_msgs::Marker::CUBE;
  	  marker.type = cube;
  	  // Set the marker action.  Options are ADD and DELETE
  	  marker.action = visualization_msgs::Marker::ADD;

  	  // Set the scale of the marker -- 1x1x1 here means 1m on a side
  	  marker.scale.x = 0.1;
  	  marker.scale.y = 0.1;
  	  marker.scale.z = 0.1;

  	  // Set the color -- be sure to set alpha to something non-zero!
  	  marker.color.r = 1.0f;
  	  marker.color.g = 0.2f;
  	  marker.color.b = 0.2f;
  	  marker.color.a = 1.0;

  	  marker.lifetime = ros::Duration();
  
  	  geometry_msgs::Quaternion heading;
  	  heading.x = 0.0;
  	  heading.y = 0.0;
  	  heading.z = 0.0;
  	  heading.w = 1.0;

  	  marker.pose.orientation = heading;
	}

	void printSubgoal(subgoal_guidance::State2D sg)
	{
	  ROS_INFO("Subgoal: [ %f, %f, %f, %f ]", sg.x, sg.vx, sg.y, sg.vy);
	}

	void computeControl(subgoal_guidance::State2D x0, subgoal_guidance::State2D xg, ros::ServiceClient &client0)
	{
	  subgoal_guidance::ComputeControlSequence srv;
	  srv.request.xstart = x0;
	  srv.request.xgoal = xg;
	  printSubgoal(xg);
	  ROS_INFO("Calling service to compute control sequence...");
	  if (client0.call(srv))
	  {
	    kopt = srv.response.topt;
	    jopt = srv.response.jopt;
	    //subgoal_guidance::ControlSequence utseq;
	    subgoal_guidance::Control2D utemp;
	    float temp;
	    utseq = srv.response.uopt;
	    utemp = utseq.controls[0];
	    temp = utemp.x;
	    ROS_INFO("Service Call Successful, k_opt = %i, J_opt = %f",kopt, jopt);
	    ROS_INFO("size of uopt = %i", utseq.controls.size());
	    //for(int i = 0;i<kopt;i++)
	    //{
		//ROS_INFO("u( %i ) = [ %f, %f ]", i+1, utseq.controls[i].x, utseq.controls[i].y);
	    //}
	  }
	  else
	  {
	    ROS_ERROR("Failed to call service compute_control");
	  }
	  ROS_INFO("Call complete");
	}
};

int main(int argc, char **argv)
{
  SubgoalSequence seq (argc, argv);
  
  return 0;
}
