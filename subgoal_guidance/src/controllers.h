/* controllers.h
 *
 * PID Controller classes & methods declarations
 *
 * History:
 * -------------------------------------------------------------------------
 * 		Date	Developer	Modification
 * -------------------------------------------------------------------------
 *   06/02/09   venkat		modification history started
 *   08/21/09   Navid		H-infinity controller for the inner-loop added
 *   08/28/09   Navid		H-infinity controller for the outer-loop added
 */
#ifndef __CONTROLLERS_H__
#define __CONTROLLERS_H__



#include <stdio.h>
#include <string.h>

#include "matrix.h"
#include "matrix_invert.h"
#include "vector.h"

#define CONTROL_SAMPLING_TIME 0.033//0.00625 //0.01

#define LIMIT(A,B,C) (((A)<(B))?(B):(MIN(A,C))) //!< Limit operation
#define MIN(A,B)   (((A)<(B))?(A):(B)) //!< Minimum operation



// H-infinity controller input files and parameters:
// Inner Loop:
#define HINFInnerFILENAME        "./src/hinfctrldata/hinf_innerloop.txt"
#define PreCompInnerFILENAME     "./src/hinfctrldata/precomp_innerloop.txt"
#define PreGainInnerFILENAME	 "./src/hinfctrldata/pregain_innerloop.txt"
#define nStateCtrl_inner      42 //  number of states in H-inf controller inner-loop
#define nStateW1_inner        14  // number of states in the pre-compensator
// Outer Loop:
#define HINFOuterFILENAME        "./src/hinfctrldata/hinf_outerloop.txt"
#define PreCompOuterFILENAME     "./src/hinfctrldata/precomp_outerloop.txt"
#define PreGainOuterFILENAME	 "./src/hinfctrldata/pregain_outerloop.txt"
#define nStateCtrl_outer       4//42 //  number of states in H-inf controller outer-loop
#define nStateW1_outer         2//2  // number of states in the pre-compensator

#define  RAD2DEG 180/3.1415926536  //converts radians to degrees
#define  DEG2RAD 3.1415926536/180  //converts degrees to radians


typedef struct
{
    double phi;
    double the;
    double psi;

} AttitudeTrim;

typedef struct
{
    double deltaLon;
    double deltaLat;
    double deltaCol;
    double deltaPed;

} ServoTrim;

typedef struct
{
    double deltaLon;
    double deltaLat;
    double deltaCol;
    double deltaPed;

} ServoDeflection;


typedef struct
{
    double kP, kD, kI, tau_filter;
}PIDGains;

/* PID controller gains */
typedef struct
{
    PIDGains roll, pitch, yaw;
    PIDGains u, v, alt;
    PIDGains vertVel;
    double u_feedForward, v_feedForward;
    PIDGains x,y;

}PIDContrGains;


typedef struct
{
	double  Kp, Kd, Kstick, KstickDirect, Kgyro;
}AttitudeLockGains;

typedef struct
{
	AttitudeLockGains roll, pitch;
}AttitudeLockCtrlGains;



/* $Id: PID.cpp,v 2.0 2002/09/22 02:07:30 tramm Exp $
 *
 * (c) Aaron Kahn
 * (c) Trammell Hudson
 *
 * PID control logic for a SISO controller.
 *
 *************
 *
 *  The PID class is part of the autopilot simulation package.
 *
 *  Autopilot is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Autopilot is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Autopilot; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * * This is a general PID control law function.  The output is the
 * result of the controller.
 *
 *  command[0] ---->O--> LIMITER --> Kp ---------------+
 *                - |              |                   |
 *  feedback[0] ----+             INTEGRATE ---> Ki ---+---> OUTPUT
 *                                                     |
 *  command[1] ---->O--> LIMITER --> Kd ---------------+
 *                - |
 *  feedback[1] ----+
 */
class PID
{
public:
    PID(double Kp, double Kd, double Ki, double	dt) :
        Kp(Kp), Kd(Kd), Ki(Ki), int_dt(dt), int_state(0.0)
    {
        this->reset();
    }


    PID()
    {
        this->reset();
    }

    void init(double Kp, double Kd, double Ki, double	dt)
    {
    	this->Kp = Kp;
    	this->Kd = Kd;
    	this->Ki = Ki;
    	this->int_dt = dt;
    }

    ~PID() {}

    // Set the desired point
    void commend(double	position, double velocity)
    {
        this->commend_values[0] = position;        this->commend_values[1] = velocity;
    }
	// Update the feedback values
	void feedback(double position, double velocity)
    {
        this->feedback_values[0] = position;
        this->feedback_values[1] = velocity;
    }

    // Run the filter
    double step();
    void reset()
    {
        this->int_state		= 0.0;
        this->limit_int( -1000, 1000 );
        this->limit_pro( -1000, 1000 );
        this->limit_vel( -1000, 1000 );
        this->limit_out( -1000, 1000 );
    };

    // Set limits on the different terms
    void limit_int(double min, double max)
    {
        this->IntStateLimits[0]	= min;
        this->IntStateLimits[1] = max;
    }

    void limit_vel(double min, double max)
    {
        this->VelErrorLimits[0]	= min;
        this->VelErrorLimits[1] = max;
    }

    void limit_pro(double min, double max)
    {
        this->ProErrorLimits[0]	= min;
        this->ProErrorLimits[1] = max;
    }

    void limit_out(double min, double max)
    {
        this->OutErrorLimits[0] = min;
        this->OutErrorLimits[1] = max;
    }
private:
    // Gains for proportion, derivative and integral terms
    double      Kp;
    double      Kd;
    double      Ki;

    // integrator state and time step
    // dt == 0 implies no integration
    double      int_dt;
    double      int_state;

    // Limits on the error terms.  [MIN MAX]
    double      ProErrorLimits[2];
    double      VelErrorLimits[2];
    double      IntStateLimits[2];
    double      OutErrorLimits[2];

    // [position velocity] commands
    double      commend_values[2];

    // [position velocity] state feedback values
    double      feedback_values[2];
};


/*  notch filter in continous time:
    *  TF = (s^2+2*wn*zeita*s+wn^2)/(s^2+2*wn*s+wn^2)
    *  the following is the coeficient of notch filter in discrete domain using TUSTIN transformation
    *  Ts is the sampling time
*/

class pid
{
public:
	pid(double Kp, double Kd, double Ki, double	dt, double tau):
	        Kp(Kp), Kd(Kd), Ki(Ki), Ts(dt), tau_filter(tau)
	{
		this->reset();
	}

	pid()
	{
	        this->reset();
    }
    void init(double Kp, double Kd, double Ki, double	dt, double tau)
    {
	    	this->Kp = Kp;
	    	this->Kd = Kd;
	    	this->Ki = Ki;
	    	this->Ts = dt;
	    	this->tau_filter = tau;
    }

	~pid() {};

	double process(double y_ref, double y_meas);
	void bumplessupdate(double I );

	double getI() { return I_old; }
	void reset();

private:
	double      Kp;
	double      Kd;
	double      Ki;
	double      Ts; // sampling time
	double     tau_filter; // derivative low pass filter time constant;

	double I_old; // Integrator old value;
	double D_old; // Derivative old value;
	double y_meas_old ; // measurement old value;
	double err_old; // error old value;

};


class AttitudeLock
{
public:
	AttitudeLock(double Kp, double Kd, double Kstick, double KstickDirect, double Kgyro, double	Ts):
	        Kp(Kp), Kd(Kd), Kstick(Kstick), KstickDirect(KstickDirect), Kgyro(Kgyro), Ts(Ts)
	{
		this->reset();
	}

	AttitudeLock()
	{
	        this->reset();
    }
    void init(double Kp, double Kd, double Kstick, double KstickDirect, double Kgyro, double Ts)
    {
	    	this->Kp = Kp;
	    	this->Kd = Kd;
	    	this->Kstick = Kstick;
	    	this->KstickDirect = KstickDirect;
	    	this->Kgyro = Kgyro;
	    	this->Ts = Ts;
    }

	~AttitudeLock() {};

	double process(double attRate_ref, double attRate, double att);

	void reset();

private:
	double      Kp;
	double      Kd;
	double      Kstick;
	double      KstickDirect;
	double      Kgyro;
	double      Ts; // sampling time
	double		I_old; // Integrator old value;

};



class NotchFilter
{
public:
    NotchFilter(double wn, double zita, double Ts);

    ~NotchFilter() { };

    double process(double in);

    void reset();

private:
    double      in_old1;
    double      in_old2;
    double      out_old1;
    double      out_old2;

    double      num[3];
    double      den[3];
};

/* tustin approximatation for digital integrator*/
class Integrator
{
	public:
		Integrator(double dt): Ts(dt)
		{
			this->reset();

		}
		~Integrator() {};
		double apply(double in);
		void   reset();

	private:
		double in_old, out_old;
		double Ts;
};

/* tustin approximatation for digital lead-lag: (as+1)/(bs+1) */
class LeadLag
{
public:
    LeadLag(double a, double b, double Ts):
    	a(a), b(b), Ts(Ts)
    {
    	this->reset();
    }

    ~LeadLag() { };

    double apply(double in);

    void reset();

private:
    double a, b, Ts;
    double  out_old, in_old;

};


class FisrtOrderFilter
{
	public:
	FisrtOrderFilter(double tau,  double Ts):
    	tau(tau), Ts(Ts)
    {
    	this->reset();
    }
	~FisrtOrderFilter(){};
	double apply(double in);
	void  reset();

	private:
		double out_old, in_old;
		double Ts, tau;

};


class SecondOrderFilter
{
   public:
	SecondOrderFilter(double wn, double zita, double Ts);
	~SecondOrderFilter() { };

	    double process(double in);

	    void reset();

	private:
	    double      in_old1;
	    double      in_old2;
	    double      out_old1;
	    double      out_old2;

	    double      num[3];
	    double      den[3];

};




/*  class LoopShaping_HinfCtrlInnerLoop
 *  creates the state-space model of the H-infinity loop-shaping controller
 *  for the inner loop.
 */
class LoopShaping_HinfCtrlInnerLoop
{
public:

    LoopShaping_HinfCtrlInnerLoop()
    {
    	this->reset();
    };

    void reset()
    {
        this->state_ctrl.fill();
    };

    bool initialize( char * HinfInnerLoopFileName,  char *PreGainInnerLoopFileName) ;

	// Update the feedback values that goes into the inner loop controller
	void feedback(double theta, double phi)
    {
         // note that the feedbacks are in fact the controller input.
        this->input_ctrl[0] = theta;
        this->input_ctrl[1] = phi;
    }
    // Run the controller
    void step();

    void limit_out(double min, double max)
    {
        this->output_ctrl[0] = min;
        this->output_ctrl[1] = max;
    };

    double dt ; // sampling time for the controller
    Vector<nStateCtrl_inner>  state_ctrl; // controller internal state vector
    Vector<2>         	      output_ctrl; // controller output vector
    Vector<2>           	  input_ctrl; // controller input vector
    Matrix<2,2>				  PreGain_inner; // pre-Gain used in H-inf loop shaping controller


private:

    char HinfInnerLoopFileName[60];// name of the file which contains the controller matrices
    char PreGainInnerLoopFileName[60];// name of the file which contains the inner loop pre-gain

    Matrix<nStateCtrl_inner,nStateCtrl_inner> A_ctrl; // controller matrices: x_dot = A x + Bu
    Matrix<nStateCtrl_inner,2>		  		  B_ctrl; //                        y   = C x + Du
    Matrix<2,nStateCtrl_inner>				  C_ctrl;
    Matrix<2,2>								  D_ctrl;

    int            nA; // matrix A has dimension nA by nA
    int            nB; // number of columns in matrix B
    int            nC; // number of rows in matrix C


};

/* class PreCompensator_InnerLoop
 * creates the filter for the pre-compensator (W1) in the H-inifinity loop shaping scheme.
 *
*/

class PreCompensator_InnerLoop
{
public:

    PreCompensator_InnerLoop()
    {
    	this->reset();
    };

    void reset()
    {
        this->state_filter.fill();
    };

    bool initialize( char * PreCompensatorFileName ) ;

   // Run the filter
    void step( Vector<2> in);

    double dt ; // sampling time for the filter
    Vector<nStateW1_inner>      state_filter; // filter internal state vector
    Vector<2>                   output_filter; // filter output vector

private:

    char PreCompensatorFileName[60];// name of the file which contains the controller matrices
    Matrix<nStateW1_inner,nStateW1_inner>	A_filter; // controller matrices: x_dot = A x + Bu
    Matrix<nStateW1_inner,2>			    B_filter; //                        y   = C x + Du
    Matrix<2,nStateW1_inner>		     	C_filter;
    Matrix<2,2>								D_filter;

    int            nA; // matrix A has dimension nA by nA
    int            nB; // number of columns in matrix B
    int            nC; // number of rows in matrix C

};



/*  class LoopShaping_HinfCtrlOuterLoop
 *  creates the state-space model of the H-infinity loop-shaping controller
 *  for the outer loop.
 */
class LoopShaping_HinfCtrlOuterLoop
{
public:

    LoopShaping_HinfCtrlOuterLoop()
    {
    	this->reset();
    	if ( !this->initialize( HINFOuterFILENAME , PreGainOuterFILENAME ) )
    	{
    		printf("\n problem with initializing the H-infinity Controller\n");
    		//masterFlag = false;
    	}
    };

    void reset()
    {
        this->state_ctrl.fill();
    };

    bool initialize( char * HinfOuterLoopFileName, char *PreGainOuterLoopFileName ) ;

	// Update the feedback values that goes into the inner loop controller
	void feedback(double u , double v)
    {
         // note that the feedbacks are in fact the controller input.
        this->input_ctrl[0] = u;
        this->input_ctrl[1] = v;
    }
    // Run the controller
    void step();

    void limit_out(double min, double max)
    {
        this->output_ctrl[0] = min;
        this->output_ctrl[1] = max;
    };

    double dt ; // sampling time for the controller
    Vector<nStateCtrl_outer>     state_ctrl; // controller internal state vector
    Vector<2>              	     output_ctrl; // controller output vector
    Vector<2>               	 input_ctrl; // controller input vector
    Matrix<2,2> 				 PreGain_outer; // pre-Gain used in H-inf loop shaping controller


private:

    char HinfOuterLoopFileName[60];// name of the file which contains the controller matrices
    char PreGainOuterLoopFileName[60]; // name of the file which contains the outer loop pre-gain
    Matrix<nStateCtrl_outer,nStateCtrl_outer> A_ctrl; // controller matrices: x_dot = A x + Bu
    Matrix<nStateCtrl_outer,2>		   		  B_ctrl; //                        y   = C x + Du
    Matrix<2,nStateCtrl_outer>				  C_ctrl;
    Matrix<2,2>								  D_ctrl;

    int            nA; // matrix A has dimension nA by nA
    int            nB; // number of columns in matrix B
    int            nC; // number of rows in matrix C


};

/* class PreCompensator_InnerLoop
 * creates the filter for the pre-compensator (W1) in the H-inifinity loop shaping scheme.
 *
*/

class PreCompensator_OuterLoop
{
public:

    PreCompensator_OuterLoop()
    {
    	this->reset();
    	if ( ! this->initialize(PreCompOuterFILENAME) )
    	{
    		printf("\n problem with initializing the pre-compensator of H-infinity Controller\n");
    		//masterFlag = false;
    	}

    };

    void reset()
    {
        this->state_filter.fill();
    };

    bool initialize( char * PreCompensatorFileName ) ;

   // Run the filter
    void step( Vector<2> in);

    double dt ; // sampling time for the filter
    Vector<nStateW1_outer>      state_filter; // filter internal state vector
    Vector<2>                   output_filter; // filter output vector

private:

    char PreCompensatorFileName[60];// name of the file which contains the controller matrices
    Matrix<nStateW1_outer,nStateW1_outer>	A_filter; // controller matrices: x_dot = A x + Bu
    Matrix<nStateW1_outer,2>			    B_filter; //                        y   = C x + Du
    Matrix<2,nStateW1_outer>		     	C_filter;
    Matrix<2,2>								D_filter;

    int            nA; // matrix A has dimension nA by nA
    int            nB; // number of columns in matrix B
    int            nC; // number of rows in matrix C

};



/* class Desired_Innerloop
 * generates the desired dynamics for the inner-loop dynamic invesion controller.
 *  i.e., it produces the desired dy/dt.
 */

class Desired_cmdRate
{
public:
	Desired_cmdRate(double Kf, double Kb, double fc, double fi, double cuttoff, double dt)
    {
        this->reset();
        this->initialize( Kf,  Kb,  fc,  fi, cuttoff,  dt);
    }

	Desired_cmdRate()
    {
        this->reset();
    }

    void initialize(double Kf, double Kb, double fc, double fi,  double cuttoff, double dt)
    {
    	this->Kf = Kf;
    	this->Kb = Kb;
    	this->fc = fc;
    	this->fi = fi;
    	this->cuttoff = cuttoff;
    	this->dt = dt;

    	this->tau = 1; // time constant of the derivative filter
    }

    ~Desired_cmdRate() {};

    // Run the filter
    double step(double meas, double cmd); // inputs are the measurement and command value
    void reset()
    {
        this->state1_filter	= 0.0;
        this->state2_filter	= 0.0;
        this->in_old = 0.0 ;
        this->out_old = 0.0 ;

    };

    double state1_filter; // filter internal state vector
    double state2_filter;
    double cmd_deriv;

    double dydt_desired_nofilter;
private:

    /* Filter Structure:
     *  dy_dt_desired = Kf*dy_dy + Kb*(fc*yc - y + z)
     *  dz_dt = fi*Kb*(yc-y)
     */

    double Kf, Kb, fc, fi, cuttoff,  dt;
    double tau; // low pass filter time constant.
    double in_old, out_old;

};

void resetControllers (void);



/* autoLand
 *
 * Automatically lands the helicopter by decreasing the throttle slowly
 */
void *autoLand(void *runFlag);

/* operateInSemiPIDMode
 *
 * This method is to operate using the PID controller for
 * altitude and heading control and Joystick for lateral
 * and longitudinal control
 *
 * @runFlag - boolean value to stop the loop and exit this method
 */
void *operateInSemiPIDMode(void *runFlag);

/* circleTestRef
 *
 * Basically this method calculates the reference values to run
 * the helicopter in a circle. calculate the u_ref and v_ref
 * according to the circle equation, read the alt_ref and psi_ref
 * from the joystick
 *
 * @runFlag - boolean value to stop the loop and exit this method
 */
void *circleTestRef(void *runFlag);
void *DoubletTestRef(void *runFlag);


//adds addAmt% of the var to var (passed by refer)
void updateGains(double &var, double addAmt);
void DynInvsInit(Matrix<2,2> Inv_CB, Matrix<2,4> CA);
double normalize(double value, double minI, double maxI, double minO, double maxO);


#endif
