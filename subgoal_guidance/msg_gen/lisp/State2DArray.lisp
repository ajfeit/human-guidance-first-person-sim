; Auto-generated. Do not edit!


(cl:in-package subgoal_guidance-msg)


;//! \htmlinclude State2DArray.msg.html

(cl:defclass <State2DArray> (roslisp-msg-protocol:ros-message)
  ((states
    :reader states
    :initarg :states
    :type (cl:vector subgoal_guidance-msg:State2D)
   :initform (cl:make-array 0 :element-type 'subgoal_guidance-msg:State2D :initial-element (cl:make-instance 'subgoal_guidance-msg:State2D))))
)

(cl:defclass State2DArray (<State2DArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <State2DArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'State2DArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name subgoal_guidance-msg:<State2DArray> is deprecated: use subgoal_guidance-msg:State2DArray instead.")))

(cl:ensure-generic-function 'states-val :lambda-list '(m))
(cl:defmethod states-val ((m <State2DArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-msg:states-val is deprecated.  Use subgoal_guidance-msg:states instead.")
  (states m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <State2DArray>) ostream)
  "Serializes a message object of type '<State2DArray>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'states))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'states))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <State2DArray>) istream)
  "Deserializes a message object of type '<State2DArray>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'states) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'states)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'subgoal_guidance-msg:State2D))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<State2DArray>)))
  "Returns string type for a message object of type '<State2DArray>"
  "subgoal_guidance/State2DArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'State2DArray)))
  "Returns string type for a message object of type 'State2DArray"
  "subgoal_guidance/State2DArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<State2DArray>)))
  "Returns md5sum for a message object of type '<State2DArray>"
  "1b8e25c819ca5841d14cc94133874483")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'State2DArray)))
  "Returns md5sum for a message object of type 'State2DArray"
  "1b8e25c819ca5841d14cc94133874483")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<State2DArray>)))
  "Returns full string definition for message of type '<State2DArray>"
  (cl:format cl:nil "State2D[] states~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'State2DArray)))
  "Returns full string definition for message of type 'State2DArray"
  (cl:format cl:nil "State2D[] states~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <State2DArray>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'states) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <State2DArray>))
  "Converts a ROS message object to a list"
  (cl:list 'State2DArray
    (cl:cons ':states (states msg))
))
