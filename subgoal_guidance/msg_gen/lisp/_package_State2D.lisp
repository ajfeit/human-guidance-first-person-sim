(cl:in-package subgoal_guidance-msg)
(cl:export '(AGENT-VAL
          AGENT
          X-VAL
          X
          Y-VAL
          Y
          VX-VAL
          VX
          VY-VAL
          VY
          AX-VAL
          AX
          AY-VAL
          AY
))