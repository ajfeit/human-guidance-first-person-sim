
(cl:in-package :asdf)

(defsystem "subgoal_guidance-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "State2D" :depends-on ("_package_State2D"))
    (:file "_package_State2D" :depends-on ("_package"))
    (:file "Control2D" :depends-on ("_package_Control2D"))
    (:file "_package_Control2D" :depends-on ("_package"))
    (:file "ControlSequence" :depends-on ("_package_ControlSequence"))
    (:file "_package_ControlSequence" :depends-on ("_package"))
    (:file "State2DArray" :depends-on ("_package_State2DArray"))
    (:file "_package_State2DArray" :depends-on ("_package"))
  ))