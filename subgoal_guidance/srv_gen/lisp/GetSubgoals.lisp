; Auto-generated. Do not edit!


(cl:in-package subgoal_guidance-srv)


;//! \htmlinclude GetSubgoals-request.msg.html

(cl:defclass <GetSubgoals-request> (roslisp-msg-protocol:ros-message)
  ((filename
    :reader filename
    :initarg :filename
    :type cl:string
    :initform ""))
)

(cl:defclass GetSubgoals-request (<GetSubgoals-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GetSubgoals-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GetSubgoals-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name subgoal_guidance-srv:<GetSubgoals-request> is deprecated: use subgoal_guidance-srv:GetSubgoals-request instead.")))

(cl:ensure-generic-function 'filename-val :lambda-list '(m))
(cl:defmethod filename-val ((m <GetSubgoals-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:filename-val is deprecated.  Use subgoal_guidance-srv:filename instead.")
  (filename m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GetSubgoals-request>) ostream)
  "Serializes a message object of type '<GetSubgoals-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'filename))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'filename))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GetSubgoals-request>) istream)
  "Deserializes a message object of type '<GetSubgoals-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'filename) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'filename) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GetSubgoals-request>)))
  "Returns string type for a service object of type '<GetSubgoals-request>"
  "subgoal_guidance/GetSubgoalsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetSubgoals-request)))
  "Returns string type for a service object of type 'GetSubgoals-request"
  "subgoal_guidance/GetSubgoalsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GetSubgoals-request>)))
  "Returns md5sum for a message object of type '<GetSubgoals-request>"
  "73b19d608968731b970f08623a9b6cf7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GetSubgoals-request)))
  "Returns md5sum for a message object of type 'GetSubgoals-request"
  "73b19d608968731b970f08623a9b6cf7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GetSubgoals-request>)))
  "Returns full string definition for message of type '<GetSubgoals-request>"
  (cl:format cl:nil "string filename~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GetSubgoals-request)))
  "Returns full string definition for message of type 'GetSubgoals-request"
  (cl:format cl:nil "string filename~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GetSubgoals-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'filename))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GetSubgoals-request>))
  "Converts a ROS message object to a list"
  (cl:list 'GetSubgoals-request
    (cl:cons ':filename (filename msg))
))
;//! \htmlinclude GetSubgoals-response.msg.html

(cl:defclass <GetSubgoals-response> (roslisp-msg-protocol:ros-message)
  ((subgoals
    :reader subgoals
    :initarg :subgoals
    :type subgoal_guidance-msg:State2DArray
    :initform (cl:make-instance 'subgoal_guidance-msg:State2DArray)))
)

(cl:defclass GetSubgoals-response (<GetSubgoals-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GetSubgoals-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GetSubgoals-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name subgoal_guidance-srv:<GetSubgoals-response> is deprecated: use subgoal_guidance-srv:GetSubgoals-response instead.")))

(cl:ensure-generic-function 'subgoals-val :lambda-list '(m))
(cl:defmethod subgoals-val ((m <GetSubgoals-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:subgoals-val is deprecated.  Use subgoal_guidance-srv:subgoals instead.")
  (subgoals m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GetSubgoals-response>) ostream)
  "Serializes a message object of type '<GetSubgoals-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'subgoals) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GetSubgoals-response>) istream)
  "Deserializes a message object of type '<GetSubgoals-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'subgoals) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GetSubgoals-response>)))
  "Returns string type for a service object of type '<GetSubgoals-response>"
  "subgoal_guidance/GetSubgoalsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetSubgoals-response)))
  "Returns string type for a service object of type 'GetSubgoals-response"
  "subgoal_guidance/GetSubgoalsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GetSubgoals-response>)))
  "Returns md5sum for a message object of type '<GetSubgoals-response>"
  "73b19d608968731b970f08623a9b6cf7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GetSubgoals-response)))
  "Returns md5sum for a message object of type 'GetSubgoals-response"
  "73b19d608968731b970f08623a9b6cf7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GetSubgoals-response>)))
  "Returns full string definition for message of type '<GetSubgoals-response>"
  (cl:format cl:nil "State2DArray subgoals~%~%~%================================================================================~%MSG: subgoal_guidance/State2DArray~%State2D[] states~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GetSubgoals-response)))
  "Returns full string definition for message of type 'GetSubgoals-response"
  (cl:format cl:nil "State2DArray subgoals~%~%~%================================================================================~%MSG: subgoal_guidance/State2DArray~%State2D[] states~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GetSubgoals-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'subgoals))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GetSubgoals-response>))
  "Converts a ROS message object to a list"
  (cl:list 'GetSubgoals-response
    (cl:cons ':subgoals (subgoals msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'GetSubgoals)))
  'GetSubgoals-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'GetSubgoals)))
  'GetSubgoals-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GetSubgoals)))
  "Returns string type for a service object of type '<GetSubgoals>"
  "subgoal_guidance/GetSubgoals")