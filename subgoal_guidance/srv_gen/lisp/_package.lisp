(cl:defpackage subgoal_guidance-srv
  (:use )
  (:export
   "GETSUBGOALS"
   "<GETSUBGOALS-REQUEST>"
   "GETSUBGOALS-REQUEST"
   "<GETSUBGOALS-RESPONSE>"
   "GETSUBGOALS-RESPONSE"
   "COMPUTECONTROLSEQUENCE"
   "<COMPUTECONTROLSEQUENCE-REQUEST>"
   "COMPUTECONTROLSEQUENCE-REQUEST"
   "<COMPUTECONTROLSEQUENCE-RESPONSE>"
   "COMPUTECONTROLSEQUENCE-RESPONSE"
   "COMPUTEOPTIMALVELOCITY"
   "<COMPUTEOPTIMALVELOCITY-REQUEST>"
   "COMPUTEOPTIMALVELOCITY-REQUEST"
   "<COMPUTEOPTIMALVELOCITY-RESPONSE>"
   "COMPUTEOPTIMALVELOCITY-RESPONSE"
  ))

