; Auto-generated. Do not edit!


(cl:in-package subgoal_guidance-srv)


;//! \htmlinclude ComputeControlSequence-request.msg.html

(cl:defclass <ComputeControlSequence-request> (roslisp-msg-protocol:ros-message)
  ((xstart
    :reader xstart
    :initarg :xstart
    :type subgoal_guidance-msg:State2D
    :initform (cl:make-instance 'subgoal_guidance-msg:State2D))
   (xgoal
    :reader xgoal
    :initarg :xgoal
    :type subgoal_guidance-msg:State2D
    :initform (cl:make-instance 'subgoal_guidance-msg:State2D)))
)

(cl:defclass ComputeControlSequence-request (<ComputeControlSequence-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ComputeControlSequence-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ComputeControlSequence-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name subgoal_guidance-srv:<ComputeControlSequence-request> is deprecated: use subgoal_guidance-srv:ComputeControlSequence-request instead.")))

(cl:ensure-generic-function 'xstart-val :lambda-list '(m))
(cl:defmethod xstart-val ((m <ComputeControlSequence-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:xstart-val is deprecated.  Use subgoal_guidance-srv:xstart instead.")
  (xstart m))

(cl:ensure-generic-function 'xgoal-val :lambda-list '(m))
(cl:defmethod xgoal-val ((m <ComputeControlSequence-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:xgoal-val is deprecated.  Use subgoal_guidance-srv:xgoal instead.")
  (xgoal m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ComputeControlSequence-request>) ostream)
  "Serializes a message object of type '<ComputeControlSequence-request>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'xstart) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'xgoal) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ComputeControlSequence-request>) istream)
  "Deserializes a message object of type '<ComputeControlSequence-request>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'xstart) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'xgoal) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ComputeControlSequence-request>)))
  "Returns string type for a service object of type '<ComputeControlSequence-request>"
  "subgoal_guidance/ComputeControlSequenceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ComputeControlSequence-request)))
  "Returns string type for a service object of type 'ComputeControlSequence-request"
  "subgoal_guidance/ComputeControlSequenceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ComputeControlSequence-request>)))
  "Returns md5sum for a message object of type '<ComputeControlSequence-request>"
  "f6760870d9a92d80d602844c9672645f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ComputeControlSequence-request)))
  "Returns md5sum for a message object of type 'ComputeControlSequence-request"
  "f6760870d9a92d80d602844c9672645f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ComputeControlSequence-request>)))
  "Returns full string definition for message of type '<ComputeControlSequence-request>"
  (cl:format cl:nil "State2D xstart~%State2D xgoal~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ComputeControlSequence-request)))
  "Returns full string definition for message of type 'ComputeControlSequence-request"
  (cl:format cl:nil "State2D xstart~%State2D xgoal~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ComputeControlSequence-request>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'xstart))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'xgoal))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ComputeControlSequence-request>))
  "Converts a ROS message object to a list"
  (cl:list 'ComputeControlSequence-request
    (cl:cons ':xstart (xstart msg))
    (cl:cons ':xgoal (xgoal msg))
))
;//! \htmlinclude ComputeControlSequence-response.msg.html

(cl:defclass <ComputeControlSequence-response> (roslisp-msg-protocol:ros-message)
  ((uopt
    :reader uopt
    :initarg :uopt
    :type subgoal_guidance-msg:ControlSequence
    :initform (cl:make-instance 'subgoal_guidance-msg:ControlSequence))
   (topt
    :reader topt
    :initarg :topt
    :type cl:integer
    :initform 0)
   (jopt
    :reader jopt
    :initarg :jopt
    :type cl:float
    :initform 0.0))
)

(cl:defclass ComputeControlSequence-response (<ComputeControlSequence-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ComputeControlSequence-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ComputeControlSequence-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name subgoal_guidance-srv:<ComputeControlSequence-response> is deprecated: use subgoal_guidance-srv:ComputeControlSequence-response instead.")))

(cl:ensure-generic-function 'uopt-val :lambda-list '(m))
(cl:defmethod uopt-val ((m <ComputeControlSequence-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:uopt-val is deprecated.  Use subgoal_guidance-srv:uopt instead.")
  (uopt m))

(cl:ensure-generic-function 'topt-val :lambda-list '(m))
(cl:defmethod topt-val ((m <ComputeControlSequence-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:topt-val is deprecated.  Use subgoal_guidance-srv:topt instead.")
  (topt m))

(cl:ensure-generic-function 'jopt-val :lambda-list '(m))
(cl:defmethod jopt-val ((m <ComputeControlSequence-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:jopt-val is deprecated.  Use subgoal_guidance-srv:jopt instead.")
  (jopt m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ComputeControlSequence-response>) ostream)
  "Serializes a message object of type '<ComputeControlSequence-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'uopt) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'topt)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'topt)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'topt)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'topt)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'jopt))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ComputeControlSequence-response>) istream)
  "Deserializes a message object of type '<ComputeControlSequence-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'uopt) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'topt)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'topt)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'topt)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'topt)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'jopt) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ComputeControlSequence-response>)))
  "Returns string type for a service object of type '<ComputeControlSequence-response>"
  "subgoal_guidance/ComputeControlSequenceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ComputeControlSequence-response)))
  "Returns string type for a service object of type 'ComputeControlSequence-response"
  "subgoal_guidance/ComputeControlSequenceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ComputeControlSequence-response>)))
  "Returns md5sum for a message object of type '<ComputeControlSequence-response>"
  "f6760870d9a92d80d602844c9672645f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ComputeControlSequence-response)))
  "Returns md5sum for a message object of type 'ComputeControlSequence-response"
  "f6760870d9a92d80d602844c9672645f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ComputeControlSequence-response>)))
  "Returns full string definition for message of type '<ComputeControlSequence-response>"
  (cl:format cl:nil "ControlSequence uopt~%uint32 topt~%float32 jopt~%~%~%================================================================================~%MSG: subgoal_guidance/ControlSequence~%Control2D[] controls~%~%================================================================================~%MSG: subgoal_guidance/Control2D~%float32 x~%float32 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ComputeControlSequence-response)))
  "Returns full string definition for message of type 'ComputeControlSequence-response"
  (cl:format cl:nil "ControlSequence uopt~%uint32 topt~%float32 jopt~%~%~%================================================================================~%MSG: subgoal_guidance/ControlSequence~%Control2D[] controls~%~%================================================================================~%MSG: subgoal_guidance/Control2D~%float32 x~%float32 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ComputeControlSequence-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'uopt))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ComputeControlSequence-response>))
  "Converts a ROS message object to a list"
  (cl:list 'ComputeControlSequence-response
    (cl:cons ':uopt (uopt msg))
    (cl:cons ':topt (topt msg))
    (cl:cons ':jopt (jopt msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'ComputeControlSequence)))
  'ComputeControlSequence-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'ComputeControlSequence)))
  'ComputeControlSequence-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ComputeControlSequence)))
  "Returns string type for a service object of type '<ComputeControlSequence>"
  "subgoal_guidance/ComputeControlSequence")