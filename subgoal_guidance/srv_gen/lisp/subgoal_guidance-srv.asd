
(cl:in-package :asdf)

(defsystem "subgoal_guidance-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :subgoal_guidance-msg
)
  :components ((:file "_package")
    (:file "GetSubgoals" :depends-on ("_package_GetSubgoals"))
    (:file "_package_GetSubgoals" :depends-on ("_package"))
    (:file "ComputeControlSequence" :depends-on ("_package_ComputeControlSequence"))
    (:file "_package_ComputeControlSequence" :depends-on ("_package"))
    (:file "ComputeOptimalVelocity" :depends-on ("_package_ComputeOptimalVelocity"))
    (:file "_package_ComputeOptimalVelocity" :depends-on ("_package"))
  ))