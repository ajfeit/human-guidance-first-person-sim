; Auto-generated. Do not edit!


(cl:in-package subgoal_guidance-srv)


;//! \htmlinclude ComputeOptimalVelocity-request.msg.html

(cl:defclass <ComputeOptimalVelocity-request> (roslisp-msg-protocol:ros-message)
  ((goalstate
    :reader goalstate
    :initarg :goalstate
    :type subgoal_guidance-msg:State2D
    :initform (cl:make-instance 'subgoal_guidance-msg:State2D))
   (startpos
    :reader startpos
    :initarg :startpos
    :type geometry_msgs-msg:Vector3
    :initform (cl:make-instance 'geometry_msgs-msg:Vector3)))
)

(cl:defclass ComputeOptimalVelocity-request (<ComputeOptimalVelocity-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ComputeOptimalVelocity-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ComputeOptimalVelocity-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name subgoal_guidance-srv:<ComputeOptimalVelocity-request> is deprecated: use subgoal_guidance-srv:ComputeOptimalVelocity-request instead.")))

(cl:ensure-generic-function 'goalstate-val :lambda-list '(m))
(cl:defmethod goalstate-val ((m <ComputeOptimalVelocity-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:goalstate-val is deprecated.  Use subgoal_guidance-srv:goalstate instead.")
  (goalstate m))

(cl:ensure-generic-function 'startpos-val :lambda-list '(m))
(cl:defmethod startpos-val ((m <ComputeOptimalVelocity-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:startpos-val is deprecated.  Use subgoal_guidance-srv:startpos instead.")
  (startpos m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ComputeOptimalVelocity-request>) ostream)
  "Serializes a message object of type '<ComputeOptimalVelocity-request>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'goalstate) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'startpos) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ComputeOptimalVelocity-request>) istream)
  "Deserializes a message object of type '<ComputeOptimalVelocity-request>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'goalstate) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'startpos) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ComputeOptimalVelocity-request>)))
  "Returns string type for a service object of type '<ComputeOptimalVelocity-request>"
  "subgoal_guidance/ComputeOptimalVelocityRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ComputeOptimalVelocity-request)))
  "Returns string type for a service object of type 'ComputeOptimalVelocity-request"
  "subgoal_guidance/ComputeOptimalVelocityRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ComputeOptimalVelocity-request>)))
  "Returns md5sum for a message object of type '<ComputeOptimalVelocity-request>"
  "187864a750528184a2b7940b619c82dc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ComputeOptimalVelocity-request)))
  "Returns md5sum for a message object of type 'ComputeOptimalVelocity-request"
  "187864a750528184a2b7940b619c82dc")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ComputeOptimalVelocity-request>)))
  "Returns full string definition for message of type '<ComputeOptimalVelocity-request>"
  (cl:format cl:nil "State2D goalstate~%geometry_msgs/Vector3 startpos~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ComputeOptimalVelocity-request)))
  "Returns full string definition for message of type 'ComputeOptimalVelocity-request"
  (cl:format cl:nil "State2D goalstate~%geometry_msgs/Vector3 startpos~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ComputeOptimalVelocity-request>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'goalstate))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'startpos))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ComputeOptimalVelocity-request>))
  "Converts a ROS message object to a list"
  (cl:list 'ComputeOptimalVelocity-request
    (cl:cons ':goalstate (goalstate msg))
    (cl:cons ':startpos (startpos msg))
))
;//! \htmlinclude ComputeOptimalVelocity-response.msg.html

(cl:defclass <ComputeOptimalVelocity-response> (roslisp-msg-protocol:ros-message)
  ((startstate
    :reader startstate
    :initarg :startstate
    :type subgoal_guidance-msg:State2D
    :initform (cl:make-instance 'subgoal_guidance-msg:State2D))
   (jopt
    :reader jopt
    :initarg :jopt
    :type cl:float
    :initform 0.0))
)

(cl:defclass ComputeOptimalVelocity-response (<ComputeOptimalVelocity-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ComputeOptimalVelocity-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ComputeOptimalVelocity-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name subgoal_guidance-srv:<ComputeOptimalVelocity-response> is deprecated: use subgoal_guidance-srv:ComputeOptimalVelocity-response instead.")))

(cl:ensure-generic-function 'startstate-val :lambda-list '(m))
(cl:defmethod startstate-val ((m <ComputeOptimalVelocity-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:startstate-val is deprecated.  Use subgoal_guidance-srv:startstate instead.")
  (startstate m))

(cl:ensure-generic-function 'jopt-val :lambda-list '(m))
(cl:defmethod jopt-val ((m <ComputeOptimalVelocity-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader subgoal_guidance-srv:jopt-val is deprecated.  Use subgoal_guidance-srv:jopt instead.")
  (jopt m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ComputeOptimalVelocity-response>) ostream)
  "Serializes a message object of type '<ComputeOptimalVelocity-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'startstate) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'jopt))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ComputeOptimalVelocity-response>) istream)
  "Deserializes a message object of type '<ComputeOptimalVelocity-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'startstate) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'jopt) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ComputeOptimalVelocity-response>)))
  "Returns string type for a service object of type '<ComputeOptimalVelocity-response>"
  "subgoal_guidance/ComputeOptimalVelocityResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ComputeOptimalVelocity-response)))
  "Returns string type for a service object of type 'ComputeOptimalVelocity-response"
  "subgoal_guidance/ComputeOptimalVelocityResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ComputeOptimalVelocity-response>)))
  "Returns md5sum for a message object of type '<ComputeOptimalVelocity-response>"
  "187864a750528184a2b7940b619c82dc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ComputeOptimalVelocity-response)))
  "Returns md5sum for a message object of type 'ComputeOptimalVelocity-response"
  "187864a750528184a2b7940b619c82dc")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ComputeOptimalVelocity-response>)))
  "Returns full string definition for message of type '<ComputeOptimalVelocity-response>"
  (cl:format cl:nil "State2D startstate~%float64 jopt~%~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ComputeOptimalVelocity-response)))
  "Returns full string definition for message of type 'ComputeOptimalVelocity-response"
  (cl:format cl:nil "State2D startstate~%float64 jopt~%~%~%================================================================================~%MSG: subgoal_guidance/State2D~%string agent~%float64 x~%float64 y~%float64 vx~%float64 vy~%float64 ax~%float64 ay~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ComputeOptimalVelocity-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'startstate))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ComputeOptimalVelocity-response>))
  "Converts a ROS message object to a list"
  (cl:list 'ComputeOptimalVelocity-response
    (cl:cons ':startstate (startstate msg))
    (cl:cons ':jopt (jopt msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'ComputeOptimalVelocity)))
  'ComputeOptimalVelocity-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'ComputeOptimalVelocity)))
  'ComputeOptimalVelocity-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ComputeOptimalVelocity)))
  "Returns string type for a service object of type '<ComputeOptimalVelocity>"
  "subgoal_guidance/ComputeOptimalVelocity")